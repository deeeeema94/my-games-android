package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Message extends BaseEntity implements Serializable {

     @SerializedName("title")
    private String Title;

    @SerializedName("script")
    private String Script;

    @SerializedName("groupId")
    private int GroupId;

    @SerializedName("isForAll")
    private boolean IsForAll;

    @SerializedName("group")
    private Group Group;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getScript() {
        return Script;
    }

    public void setScript(String script) {
        Script = script;
    }

    public int getGroupId() {
        return GroupId;
    }

    public void setGroupId(int groupId) {
        GroupId = groupId;
    }

    public boolean isForAll() {
        return IsForAll;
    }

    public void setForAll(boolean forAll) {
        IsForAll = forAll;
    }

    public com.u.mygamesapp.models.Group getGroup() {
        return Group;
    }

    public void setGroup(com.u.mygamesapp.models.Group group) {
        Group = group;
    }
}
