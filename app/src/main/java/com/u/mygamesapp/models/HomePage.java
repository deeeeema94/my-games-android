package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class HomePage implements Serializable {

    @SerializedName("tasks")
    private List<AppTask> Tasks;

    @SerializedName("finishedTasks")
    private List<AppTask> finishedTasks;

    @SerializedName("pendingTasks")
    private List<AppTask> PendingTasks;

    @SerializedName("slides")
    private List<Content> Slides;

    @SerializedName("luckyWheelValid")
    private boolean LuckyWheelValid;

    @SerializedName("dailyBonusValid")
    private boolean DailyBonusValid;

    @SerializedName("newMessages")
    private int newMessages;

//    @SerializedName("taskTypes")
//    private List<TaskType> TaskTypes;


    public List<AppTask> getTasks() {
        return Tasks;
    }

    public void setTasks(List<AppTask> tasks) {
        Tasks = tasks;
    }

    public List<Content> getSlides() {
        return Slides;
    }

    public void setSlides(List<Content> slides) {
        Slides = slides;
    }

    public boolean isLuckyWheelValid() {
        return LuckyWheelValid;
    }

    public void setLuckyWheelValid(boolean luckyWheelValid) {
        LuckyWheelValid = luckyWheelValid;
    }

    public List<AppTask> getPendingTasks() {
        return PendingTasks;
    }

    public void setPendingTasks(List<AppTask> pendingTasks) {
        PendingTasks = pendingTasks;
    }

    public List<AppTask> getFinishedTasks() {
        return finishedTasks;
    }

    public void setFinishedTasks(List<AppTask> finishedTasks) {
        this.finishedTasks = finishedTasks;
    }

    public int getNewMessages() {
        return newMessages;
    }

    public void setNewMessages(int newMessages) {
        this.newMessages = newMessages;
    }

    public boolean isDailyBonusValid() {
        return DailyBonusValid;
    }

    public void setDailyBonusValid(boolean dailyBonusValid) {
        DailyBonusValid = dailyBonusValid;
    }
}
