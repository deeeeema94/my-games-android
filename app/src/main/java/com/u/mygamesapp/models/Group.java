package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Group extends BaseEntity implements Serializable {

    @SerializedName("color")
    private String Color;

    @SerializedName("name")
    private String Name;

    @SerializedName("displayName")
    private String DisplayName;

    @SerializedName("itemOrder")
    private int ItemOrder;

    @SerializedName("points")
    private int Points;

    public Group(int id) {
        this.setId(id);
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public int getPoints() {
        return Points;
    }

    public void setPoints(int points) {
        Points = points;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public int getItemOrder() {
        return ItemOrder;
    }

    public void setItemOrder(int itemOrder) {
        ItemOrder = itemOrder;
    }
}
