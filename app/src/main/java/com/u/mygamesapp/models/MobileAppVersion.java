package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MobileAppVersion extends BaseEntity implements Serializable {

    @SerializedName("isCurrent")
    private boolean IsCurrent;

    @SerializedName("isSupported")
    private boolean IsSupported;

    @SerializedName("versionNumber")
    private String VersionNumber;

    @SerializedName("versionCode")
    private String VersionCode;

    @SerializedName("storeUrl")
    private String StoreUrl;

    @SerializedName("syriaStoreUrl")
    private String SyriaStoreUrl;

    @SerializedName("platformType")
    private String PlatformType;

    @SerializedName("qrcode")
    private String Qrcode;

    @SerializedName("updateMsg")
    private String UpdateMsg;

    @SerializedName("numberOfDownloads")
    private int NumberOfDownloads;

    @SerializedName("note")
    private String Note;

    public boolean isCurrent() {
        return IsCurrent;
    }

    public void setCurrent(boolean current) {
        IsCurrent = current;
    }

    public boolean isSupported() {
        return IsSupported;
    }

    public void setSupported(boolean supported) {
        IsSupported = supported;
    }

    public String getVersionNumber() {
        return VersionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        VersionNumber = versionNumber;
    }

    public String getVersionCode() {
        return VersionCode;
    }

    public void setVersionCode(String versionCode) {
        VersionCode = versionCode;
    }

    public String getStoreUrl() {
        return StoreUrl;
    }

    public void setStoreUrl(String storeUrl) {
        StoreUrl = storeUrl;
    }

    public String getSyriaStoreUrl() {
        return SyriaStoreUrl;
    }

    public void setSyriaStoreUrl(String syriaStoreUrl) {
        SyriaStoreUrl = syriaStoreUrl;
    }

    public String getPlatformType() {
        return PlatformType;
    }

    public void setPlatformType(String platformType) {
        PlatformType = platformType;
    }

    public String getQrcode() {
        return Qrcode;
    }

    public void setQrcode(String qrcode) {
        Qrcode = qrcode;
    }

    public String getUpdateMsg() {
        return UpdateMsg;
    }

    public void setUpdateMsg(String updateMsg) {
        UpdateMsg = updateMsg;
    }

    public int getNumberOfDownloads() {
        return NumberOfDownloads;
    }

    public void setNumberOfDownloads(int numberOfDownloads) {
        NumberOfDownloads = numberOfDownloads;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }
}
