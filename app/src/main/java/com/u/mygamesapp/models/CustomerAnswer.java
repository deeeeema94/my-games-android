package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CustomerAnswer extends BaseEntity implements Serializable {

    @SerializedName("customerId")
    private int CustomerId;

    @SerializedName("questionOptionId")
    private int QuestionOptionId;

    @SerializedName("questionId")
    private int QuestionId;

    @SerializedName("answer")
    private String Answer;

    @SerializedName("question")
    private Question Question;

    @SerializedName("questionOption")
    private QuestionOption QuestionOption;

    public CustomerAnswer(int questionOptionId, int questionId) {
        QuestionOptionId = questionOptionId;
        QuestionId = questionId;
    }

    public CustomerAnswer(int questionId, String answer) {
        QuestionId = questionId;
        Answer = answer;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int customerId) {
        CustomerId = customerId;
    }

    public int getQuestionOptionId() {
        return QuestionOptionId;
    }

    public void setQuestionOptionId(int questionOptionId) {
        QuestionOptionId = questionOptionId;
    }

    public int getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(int questionId) {
        QuestionId = questionId;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String answer) {
        Answer = answer;
    }

    public com.u.mygamesapp.models.Question getQuestion() {
        return Question;
    }

    public void setQuestion(com.u.mygamesapp.models.Question question) {
        Question = question;
    }

    public com.u.mygamesapp.models.QuestionOption getQuestionOption() {
        return QuestionOption;
    }

    public void setQuestionOption(com.u.mygamesapp.models.QuestionOption questionOption) {
        QuestionOption = questionOption;
    }
}
