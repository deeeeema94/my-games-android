package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CustomerTask extends BaseEntity implements Serializable {

    @SerializedName("customerId")
    private int CustomerId;

    @SerializedName("appTaskId")
    private int AppTaskId;

    @SerializedName("isDone")
    private boolean IsDone;

    @SerializedName("doneDate")
    private String DoneDate;

    @SerializedName("startDate")
    private String StartDate;

    @SerializedName("earnedPoints")
    private int EarnedPoints;

    @SerializedName("appTask")
    private AppTask AppTask;

    @SerializedName("sportMatch")
    private SportMatch SportMatch;

    public CustomerTask(int appTaskId) {
        AppTaskId = appTaskId;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int customerId) {
        CustomerId = customerId;
    }

    public int getAppTaskId() {
        return AppTaskId;
    }

    public void setAppTaskId(int appTaskId) {
        AppTaskId = appTaskId;
    }

    public boolean isDone() {
        return IsDone;
    }

    public void setDone(boolean done) {
        IsDone = done;
    }

    public String getDoneDate() {
        return DoneDate;
    }

    public void setDoneDate(String doneDate) {
        DoneDate = doneDate;
    }

    public com.u.mygamesapp.models.AppTask getAppTask() {
        return AppTask;
    }

    public void setAppTask(com.u.mygamesapp.models.AppTask appTask) {
        AppTask = appTask;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public com.u.mygamesapp.models.SportMatch getSportMatch() {
        return SportMatch;
    }

    public void setSportMatch(com.u.mygamesapp.models.SportMatch sportMatch) {
        SportMatch = sportMatch;
    }

    public int getEarnedPoints() {
        return EarnedPoints;
    }

    public void setEarnedPoints(int earnedPoints) {
        EarnedPoints = earnedPoints;
    }
}
