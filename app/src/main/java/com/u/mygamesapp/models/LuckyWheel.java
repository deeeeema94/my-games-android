package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

public class LuckyWheel {

    @SerializedName("groupId")
    private int GroupId;
    
    @SerializedName("partsCount")
    private int PartsCount;

    @SerializedName("isDoneToday")
    private boolean IsDoneToday;

    @SerializedName("group")
    private Group Group;

    @SerializedName("prizes")
    private Prize[] Prizes;

    public int getGroupId() {
        return GroupId;
    }

    public void setGroupId(int groupId) {
        GroupId = groupId;
    }

    public int getPartsCount() {
        return PartsCount;
    }

    public void setPartsCount(int partsCount) {
        PartsCount = partsCount;
    }

    public boolean isDoneToday() {
        return IsDoneToday;
    }

    public void setDoneToday(boolean doneToday) {
        IsDoneToday = doneToday;
    }

    public com.u.mygamesapp.models.Group getGroup() {
        return Group;
    }

    public void setGroup(com.u.mygamesapp.models.Group group) {
        Group = group;
    }

    public Prize[] getPrizes() {
        return Prizes;
    }

    public void setPrizes(Prize[] prizes) {
        Prizes = prizes;
    }
}
