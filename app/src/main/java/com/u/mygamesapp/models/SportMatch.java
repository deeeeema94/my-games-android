package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SportMatch extends BaseEntity implements Serializable {

    @SerializedName("firstTeamName")
    private String FirstTeamName;

    @SerializedName("secondTeamName")
    private String SecondTeamName;

    @SerializedName("firstTeamFlag")
    private String FirstTeamFlag;

    @SerializedName("secondTeamFlag")
    private String SecondTeamFlag;

    @SerializedName("type")
    private String Type;

    @SerializedName("league")
    private String League;

    @SerializedName("matchTime")
    private String MatchTime;

    @SerializedName("firstTeamScore")
    private int FirstTeamScore;

    @SerializedName("secondTeamScore")
    private int SecondTeamScore;

    @SerializedName("appTaskId")
    private int AppTaskId;

    @SerializedName("appTask")
    private AppTask AppTask;

    public SportMatch() {
    }

    public SportMatch(int appTaskId) {
        AppTaskId = appTaskId;
    }

    public SportMatch(String firstTeamName, String secondTeamName, String firstTeamFlag, String secondTeamFlag, String type, String league, String matchTime, int firstTeamScore, int secondTeamScore, int appTaskId, com.u.mygamesapp.models.AppTask appTask) {
        FirstTeamName = firstTeamName;
        SecondTeamName = secondTeamName;
        FirstTeamFlag = firstTeamFlag;
        SecondTeamFlag = secondTeamFlag;
        Type = type;
        League = league;
        MatchTime = matchTime;
        FirstTeamScore = firstTeamScore;
        SecondTeamScore = secondTeamScore;
        AppTaskId = appTaskId;
        AppTask = appTask;
    }

    public String getFirstTeamName() {
        return FirstTeamName;
    }

    public void setFirstTeamName(String firstTeamName) {
        FirstTeamName = firstTeamName;
    }

    public String getSecondTeamName() {
        return SecondTeamName;
    }

    public void setSecondTeamName(String secondTeamName) {
        SecondTeamName = secondTeamName;
    }

    public String getFirstTeamFlag() {
        return FirstTeamFlag;
    }

    public void setFirstTeamFlag(String firstTeamFlag) {
        FirstTeamFlag = firstTeamFlag;
    }

    public String getSecondTeamFlag() {
        return SecondTeamFlag;
    }

    public void setSecondTeamFlag(String secondTeamFlag) {
        SecondTeamFlag = secondTeamFlag;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getLeague() {
        return League;
    }

    public void setLeague(String league) {
        League = league;
    }

    public String getMatchTime() {
        return MatchTime;
    }

    public void setMatchTime(String matchTime) {
        MatchTime = matchTime;
    }

    public int getFirstTeamScore() {
        return FirstTeamScore;
    }

    public void setFirstTeamScore(int firstTeamScore) {
        FirstTeamScore = firstTeamScore;
    }

    public int getSecondTeamScore() {
        return SecondTeamScore;
    }

    public void setSecondTeamScore(int secondTeamScore) {
        SecondTeamScore = secondTeamScore;
    }

    public int getAppTaskId() {
        return AppTaskId;
    }

    public void setAppTaskId(int appTaskId) {
        AppTaskId = appTaskId;
    }

    public com.u.mygamesapp.models.AppTask getAppTask() {
        return AppTask;
    }

    public void setAppTask(com.u.mygamesapp.models.AppTask appTask) {
        AppTask = appTask;
    }
}
