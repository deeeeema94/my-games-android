package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Question extends BaseEntity implements Serializable {

    @SerializedName("script")
    private String Script;

    @SerializedName("description")
    private String Description;

    @SerializedName("type")
    private boolean Type;

     @SerializedName("itemOrder")
    private int ItemOrder;

    @SerializedName("appTaskId")
    private int AppTaskId;

    @SerializedName("appTask")
    private AppTask AppTask;

    @SerializedName("isLast")
    private boolean IsLast;

    @SerializedName("isAllRight")
    private boolean IsAllRight;

    @SerializedName("totalQuestionsCount")
    private int TotalQuestionsCount;

    @SerializedName("questionOptions")
    private List<QuestionOption> QuestionOptions;

    @SerializedName("customerAnswers")
    private List<CustomerAnswer> CustomerAnswers;

    public Question(int itemOrder, int appTaskId) {
        ItemOrder = itemOrder;
        AppTaskId = appTaskId;
    }

    public String getScript() {
        return Script;
    }

    public void setScript(String script) {
        Script = script;
    }

    public boolean isType() {
        return Type;
    }

    public void setType(boolean type) {
        Type = type;
    }

    public int getItemOrder() {
        return ItemOrder;
    }

    public void setItemOrder(int itemOrder) {
        ItemOrder = itemOrder;
    }

    public int getAppTaskId() {
        return AppTaskId;
    }

    public void setAppTaskId(int appTaskId) {
        AppTaskId = appTaskId;
    }

    public com.u.mygamesapp.models.AppTask getAppTask() {
        return AppTask;
    }

    public void setAppTask(com.u.mygamesapp.models.AppTask appTask) {
        AppTask = appTask;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public List<QuestionOption> getQuestionOptions() {
        return QuestionOptions;
    }

    public void setQuestionOptions(List<QuestionOption> questionOptions) {
        QuestionOptions = questionOptions;
    }

    public List<CustomerAnswer> getCustomerAnswers() {
        return CustomerAnswers;
    }

    public void setCustomerAnswers(List<CustomerAnswer> customerAnswers) {
        CustomerAnswers = customerAnswers;
    }

    public boolean isLast() {
        return IsLast;
    }

    public void setLast(boolean last) {
        IsLast = last;
    }

    public int getTotalQuestionsCount() {
        return TotalQuestionsCount;
    }

    public void setTotalQuestionsCount(int totalQuestionsCount) {
        TotalQuestionsCount = totalQuestionsCount;
    }

    public boolean isAllRight() {
        return IsAllRight;
    }

    public void setAllRight(boolean allRight) {
        IsAllRight = allRight;
    }
}
