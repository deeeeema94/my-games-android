package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

public class DailyBonus {

    @SerializedName("partsCount")
    private int PartsCount;

    @SerializedName("isDoneToday")
    private boolean IsDoneToday;

    @SerializedName("isDoneYesterday")
    private boolean IsDoneYesterday;

    @SerializedName("prizes")
    private Prize[] Prizes;


    public int getPartsCount() {
        return PartsCount;
    }

    public void setPartsCount(int partsCount) {
        PartsCount = partsCount;
    }

    public boolean isDoneToday() {
        return IsDoneToday;
    }

    public void setDoneToday(boolean doneToday) {
        IsDoneToday = doneToday;
    }

    public Prize[] getPrizes() {
        return Prizes;
    }

    public void setPrizes(Prize[] prizes) {
        Prizes = prizes;
    }

    public boolean isDoneYesterday() {
        return IsDoneYesterday;
    }

    public void setDoneYesterday(boolean doneYesterday) {
        IsDoneYesterday = doneYesterday;
    }
}
