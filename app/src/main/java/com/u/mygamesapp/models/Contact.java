package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Contact extends BaseEntity  implements Serializable {

    @SerializedName("icon")
    private String Icon;

    @SerializedName("name")
    private String Name;

    @SerializedName("displayName")
    private String DisplayName;

    @SerializedName("value")
    private String Value;

    public String getIcon() {
        return Icon;
    }

    public void setIcon(String icon) {
        Icon = icon;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
