package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AppTask extends BaseEntity implements Serializable {

    @SerializedName("imageUrl")
    private String ImageUrl;
    
    @SerializedName("displayName")
    private String DisplayName;
    
    @SerializedName("description")
    private String Description;
    
    @SerializedName("link")
    private String Link;

    @SerializedName("startDate")
    private String StartDate;

    @SerializedName("endDate")
    private String EndDate;

    @SerializedName("points")
    private int Points;

    @SerializedName("limit")
    private int Limit;

    @SerializedName("stared")
    private boolean Stared;

    @SerializedName("taskTypeId")
    private int TaskTypeId;
    
    @SerializedName("isDone")
    private boolean IsDone;

    @SerializedName("isReachLimit")
    private boolean IsReachLimit;

    @SerializedName("taskType")
    private TaskType TaskType;

    @SerializedName("packageName")
    private String PackageName;

    @SerializedName("vedioId")
    private String vedioId;

    @SerializedName("vedioDuration")
    private int VedioDuration;

    @SerializedName("appTaskGroups")
    private List<AppTaskGroup> AppTaskGroups;

    public AppTask(int appTaskId) {
        this.setId(appTaskId);
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public int getPoints() {
        return Points;
    }

    public void setPoints(int points) {
        Points = points;
    }

    public int getLimit() {
        return Limit;
    }

    public void setLimit(int limit) {
        Limit = limit;
    }

    public boolean isStared() {
        return Stared;
    }

    public void setStared(boolean stared) {
        Stared = stared;
    }

    public int getTaskTypeId() {
        return TaskTypeId;
    }

    public void setTaskTypeId(int taskTypeId) {
        TaskTypeId = taskTypeId;
    }

    public boolean isDone() {
        return IsDone;
    }

    public void setDone(boolean done) {
        IsDone = done;
    }

    public com.u.mygamesapp.models.TaskType getTaskType() {
        return TaskType;
    }

    public void setTaskType(com.u.mygamesapp.models.TaskType taskType) {
        TaskType = taskType;
    }

    public List<AppTaskGroup> getAppTaskGroups() {
        return AppTaskGroups;
    }

    public void setAppTaskGroups(List<AppTaskGroup> appTaskGroups) {
        AppTaskGroups = appTaskGroups;
    }

    public boolean isReachLimit() {
        return IsReachLimit;
    }

    public void setReachLimit(boolean reachLimit) {
        IsReachLimit = reachLimit;
    }

    public String getPackageName() {
        return PackageName;
    }

    public void setPackageName(String packageName) {
        PackageName = packageName;
    }

    public String getVedioId() {
        return vedioId;
    }

    public void setVedioId(String vedioId) {
        this.vedioId = vedioId;
    }

    public int getVedioDuration() {
        return VedioDuration;
    }

    public void setVedioDuration(int vedioDuration) {
        VedioDuration = vedioDuration;
    }
}
