package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AppTaskGroup extends BaseEntity  implements Serializable {

    @SerializedName("appTaskId")
    private int AppTaskId;

    @SerializedName("groupId")
    private int GroupId;

    @SerializedName("group")
    private Group Group;

    public int getAppTaskId() {
        return AppTaskId;
    }

    public void setAppTaskId(int appTaskId) {
        AppTaskId = appTaskId;
    }

    public int getGroupId() {
        return GroupId;
    }

    public void setGroupId(int groupId) {
        GroupId = groupId;
    }

    public com.u.mygamesapp.models.Group getGroup() {
        return Group;
    }

    public void setGroup(com.u.mygamesapp.models.Group group) {
        Group = group;
    }
}
