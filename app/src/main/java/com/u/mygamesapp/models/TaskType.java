package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TaskType extends BaseEntity implements Serializable {

    @SerializedName("name")
    private String Name;

    @SerializedName("displayName")
    private String DisplayName;

    @SerializedName("imageUrl")
    private String ImageUrl;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }
}
