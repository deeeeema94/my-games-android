package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ContactUs extends BaseEntity implements Parent<String>, Serializable {

    private List<String> answers;

    @SerializedName("featuredSubject")
    private String FeaturedSubject;

    @SerializedName("featuredReply")
    private String FeaturedReply;

    public ContactUs() {
    }

    public ContactUs(String featuredSubject, String featuredReply) {
        FeaturedSubject = featuredSubject;
        FeaturedReply = featuredReply;
    }

    public String getFeaturedSubject() {
        return FeaturedSubject;
    }

    public void setFeaturedSubject(String featuredSubject) {
        FeaturedSubject = featuredSubject;
    }

    public String getFeaturedReply() {
        return FeaturedReply;
    }

    public void setFeaturedReply(String featuredReply) {
        FeaturedReply = featuredReply;
    }


    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }

    @Override
    public List<String> getChildList() {
        return this.getAnswers();
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}