package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CustomerMessage extends BaseEntity implements Serializable {

    @SerializedName("customerId")
    private int CustomerId;

    @SerializedName("messageId")
    private int MessageId;

    @SerializedName("isRead")
    private boolean IsRead;

    @SerializedName("sendDate")
    private String SendDate;

    @SerializedName("message")
    private Message Message;

    public CustomerMessage(int id) {
        this.setId(id);
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int customerId) {
        CustomerId = customerId;
    }

    public int getMessageId() {
        return MessageId;
    }

    public void setMessageId(int messageId) {
        MessageId = messageId;
    }

    public boolean isRead() {
        return IsRead;
    }

    public void setRead(boolean read) {
        IsRead = read;
    }

    public String getSendDate() {
        return SendDate;
    }

    public void setSendDate(String sendDate) {
        SendDate = sendDate;
    }

    public com.u.mygamesapp.models.Message getMessage() {
        return Message;
    }

    public void setMessage(com.u.mygamesapp.models.Message message) {
        Message = message;
    }
}
