package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class QuestionOption extends BaseEntity implements Serializable {

    @SerializedName("script")
    private String Script;

    @SerializedName("questionId")
    private int QuestionId;

    @SerializedName("itemOrder")
    private int ItemOrder;

    @SerializedName("isRightOption")
    private boolean IsRightOption;

    @SerializedName("question")
    private Question Question;

    public String getScript() {
        return Script;
    }

    public void setScript(String script) {
        Script = script;
    }

    public int getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(int questionId) {
        QuestionId = questionId;
    }

    public int getItemOrder() {
        return ItemOrder;
    }

    public void setItemOrder(int itemOrder) {
        ItemOrder = itemOrder;
    }

    public com.u.mygamesapp.models.Question getQuestion() {
        return Question;
    }

    public void setQuestion(com.u.mygamesapp.models.Question question) {
        Question = question;
    }

    public boolean isRightOption() {
        return IsRightOption;
    }

    public void setRightOption(boolean rightOption) {
        IsRightOption = rightOption;
    }
}
