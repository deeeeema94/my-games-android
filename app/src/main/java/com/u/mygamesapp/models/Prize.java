package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Prize extends BaseEntity implements Serializable {

    @SerializedName("imageUrl")
    private String ImageUrl;

    @SerializedName("name")
    private String Name;

    @SerializedName("displayName")
    private String DisplayName;

    @SerializedName("description")
    private String Description;

    @SerializedName("points")
    private int Points;

    @SerializedName("prizeTypeId")
    private int PrizeTypeId;

    @SerializedName("prizeType")
    private PrizeType PrizeType;

    @SerializedName("isValid")
    private boolean IsValid;

    @SerializedName("itemOrder")
    private int ItemOrder;

    public Prize(int id){
        this.setId(id);
    }
    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getPoints() {
        return Points;
    }

    public void setPoints(int points) {
        Points = points;
    }

    public int getPrizeTypeId() {
        return PrizeTypeId;
    }

    public void setPrizeTypeId(int prizeTypeId) {
        PrizeTypeId = prizeTypeId;
    }

    public com.u.mygamesapp.models.PrizeType getPrizeType() {
        return PrizeType;
    }

    public void setPrizeType(com.u.mygamesapp.models.PrizeType prizeType) {
        PrizeType = prizeType;
    }

    public boolean isValid() {
        return IsValid;
    }

    public void setValid(boolean valid) {
        IsValid = valid;
    }

    public int getItemOrder() {
        return ItemOrder;
    }

    public void setItemOrder(int itemOrder) {
        ItemOrder = itemOrder;
    }
}
