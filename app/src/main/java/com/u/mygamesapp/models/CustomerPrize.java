package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CustomerPrize extends BaseEntity implements Serializable {

    @SerializedName("customerId")
    private int CustomerId;

    @SerializedName("prizeId")
    private int PrizeId;

    @SerializedName("earnDate")
    private String EarnDate;

    @SerializedName("requestDate")
    private String RequestDate;

    @SerializedName("prizeStatusId")
    private int PrizeStatusId;

    @SerializedName("prize")
    private Prize Prize;

    @SerializedName("prizeStatus")
    private PrizeStatus PrizeStatus;

    public CustomerPrize(int prizeId) {
        PrizeId = prizeId;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int customerId) {
        CustomerId = customerId;
    }

    public int getPrizeId() {
        return PrizeId;
    }

    public void setPrizeId(int prizeId) {
        PrizeId = prizeId;
    }

    public String getEarnDate() {
        return EarnDate;
    }

    public void setEarnDate(String earnDate) {
        EarnDate = earnDate;
    }

    public String getRequestDate() {
        return RequestDate;
    }

    public void setRequestDate(String requestDate) {
        RequestDate = requestDate;
    }

    public int getPrizeStatusId() {
        return PrizeStatusId;
    }

    public void setPrizeStatusId(int prizeStatusId) {
        PrizeStatusId = prizeStatusId;
    }

    public com.u.mygamesapp.models.Prize getPrize() {
        return Prize;
    }

    public void setPrize(com.u.mygamesapp.models.Prize prize) {
        Prize = prize;
    }

    public com.u.mygamesapp.models.PrizeStatus getPrizeStatus() {
        return PrizeStatus;
    }

    public void setPrizeStatus(com.u.mygamesapp.models.PrizeStatus prizeStatus) {
        PrizeStatus = prizeStatus;
    }
}
