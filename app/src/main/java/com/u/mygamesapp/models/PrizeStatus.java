package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PrizeStatus extends BaseEntity implements Serializable {
    
    @SerializedName("name")
    private String Name;

    @SerializedName("displayName")
    private String DisplayName;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }
}
