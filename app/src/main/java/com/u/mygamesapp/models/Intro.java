package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Intro implements Serializable {

    @SerializedName("introImages")
    private List<Content> introImages;

    @SerializedName("introVideo")
    private Content introVideo;

    public List<Content> getIntroImages() {
        return introImages;
    }

    public void setIntroImages(List<Content> introImages) {
        this.introImages = introImages;
    }

    public Content getIntroVideo() {
        return introVideo;
    }

    public void setIntroVideo(Content introVideo) {
        this.introVideo = introVideo;
    }
}
