package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Content extends BaseEntity implements Serializable {

    @SerializedName("name")
    private String Name;

     @SerializedName("title")
    private String Title;

    @SerializedName("script")
    private String Script;

     @SerializedName("itemOrder")
    private int ItemOrder;

     @SerializedName("imageUrl")
     private String ImageUrl;

    public Content(String title, String script, String imageUrl) {
        Title = title;
        Script = script;
        ImageUrl = imageUrl;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getScript() {
        return Script;
    }

    public void setScript(String script) {
        Script = script;
    }

    public int getItemOrder() {
        return ItemOrder;
    }

    public void setItemOrder(int itemOrder) {
        ItemOrder = itemOrder;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }
}
