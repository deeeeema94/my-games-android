package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MessageGroup extends BaseEntity implements Serializable {

    @SerializedName("messageId")
    private int MessageId;

    @SerializedName("groupId")
    private int GroupId;

    @SerializedName("group")
    private Group Group;

    public int getMessageId() {
        return MessageId;
    }

    public void setMessageId(int messageId) {
        MessageId = messageId;
    }

    public int getGroupId() {
        return GroupId;
    }

    public void setGroupId(int groupId) {
        GroupId = groupId;
    }

    public com.u.mygamesapp.models.Group getGroup() {
        return Group;
    }

    public void setGroup(com.u.mygamesapp.models.Group group) {
        Group = group;
    }
}
