package com.u.mygamesapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Customer extends BaseEntity implements Serializable {

    @SerializedName("firstName")
    private String FirstName;

    @SerializedName("lastName")
    private String LastName;

    @SerializedName("imageUrl")
    private String ImageUrl;

    @SerializedName("termsAndConditions")
    private boolean TermsAndConditions;

    @SerializedName("gender")
    private boolean Gender;

    @SerializedName("currentPoints")
    private int CurrentPoints;

    @SerializedName("totalPoints")
    private int TotalPoints;

    @SerializedName("luckyWheelLastSpinDate")
    private String LuckyWheelLastSpinDate;

    @SerializedName("cityId")
    private int CityId;

    @SerializedName("userId")
    private String UserId;

    @SerializedName("lockoutEnabled")
    private boolean LockoutEnabled;

    @SerializedName("groupId")
    private int GroupId;

    @SerializedName("fCMToken")
    private String FCMToken;

    @SerializedName("token")
    private String Token;

    @SerializedName("tokenExpiration")
    private String TokenExpiration;

    @SerializedName("group")
    private Group Group;

    @SerializedName("user")
    private ApplicationUser User;

    @SerializedName("city")
    private City City;

    @SerializedName("nextGroupPoints")
    private int NextGroupPoints;

    @SerializedName("shareCode")
    private String ShareCode;

    @SerializedName("installedFrom")
    private String InstalledFrom;

    @SerializedName("dailyBonusLevel")
    private int dailyBonusLevel;

    @SerializedName("dailyBonusLastUseDate")
    private String dailyBonusLastUseDate;

    public Customer(String firstName, String lastName, ApplicationUser user) {
        FirstName = firstName;
        LastName = lastName;
        User = user;
    }

    public Customer() {

    }

    public Customer(String FCMToken) {
        this.FCMToken = FCMToken;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public boolean isTermsAndConditions() {
        return TermsAndConditions;
    }

    public void setTermsAndConditions(boolean termsAndConditions) {
        TermsAndConditions = termsAndConditions;
    }

    public boolean isGender() {
        return Gender;
    }

    public void setGender(boolean gender) {
        Gender = gender;
    }

    public int getCurrentPoints() {
        return CurrentPoints;
    }

    public void setCurrentPoints(int currentPoints) {
        CurrentPoints = currentPoints;
    }

    public int getTotalPoints() {
        return TotalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        TotalPoints = totalPoints;
    }

    public String getLuckyWheelLastSpinDate() {
        return LuckyWheelLastSpinDate;
    }

    public void setLuckyWheelLastSpinDate(String luckyWheelLastSpinDate) {
        LuckyWheelLastSpinDate = luckyWheelLastSpinDate;
    }

    public int getCityId() {
        return CityId;
    }

    public void setCityId(int cityId) {
        CityId = cityId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public boolean isLockoutEnabled() {
        return LockoutEnabled;
    }

    public void setLockoutEnabled(boolean lockoutEnabled) {
        LockoutEnabled = lockoutEnabled;
    }

    public int getGroupId() {
        return GroupId;
    }

    public void setGroupId(int groupId) {
        GroupId = groupId;
    }

    public String getFCMToken() {
        return FCMToken;
    }

    public void setFCMToken(String FCMToken) {
        this.FCMToken = FCMToken;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getTokenExpiration() {
        return TokenExpiration;
    }

    public void setTokenExpiration(String tokenExpiration) {
        TokenExpiration = tokenExpiration;
    }

    public com.u.mygamesapp.models.Group getGroup() {
        return Group;
    }

    public void setGroup(com.u.mygamesapp.models.Group group) {
        Group = group;
    }

    public ApplicationUser getUser() {
        return User;
    }

    public void setUser(ApplicationUser user) {
        User = user;
    }

    public com.u.mygamesapp.models.City getCity() {
        return City;
    }

    public void setCity(com.u.mygamesapp.models.City city) {
        City = city;
    }

    public int getNextGroupPoints() {
        return NextGroupPoints;
    }

    public void setNextGroupPoints(int nextGroupPoints) {
        NextGroupPoints = nextGroupPoints;
    }

    public String getShareCode() {
        return ShareCode;
    }

    public void setShareCode(String shareCode) {
        ShareCode = shareCode;
    }

    public String getInstalledFrom() {
        return InstalledFrom;
    }

    public void setInstalledFrom(String installedFrom) {
        InstalledFrom = installedFrom;
    }

    public int getDailyBonusLevel() {
        return dailyBonusLevel;
    }

    public void setDailyBonusLevel(int dailyBonusLevel) {
        this.dailyBonusLevel = dailyBonusLevel;
    }

    public String getDailyBonusLastUseDate() {
        return dailyBonusLastUseDate;
    }

    public void setDailyBonusLastUseDate(String dailyBonusLastUseDate) {
        this.dailyBonusLastUseDate = dailyBonusLastUseDate;
    }
}
