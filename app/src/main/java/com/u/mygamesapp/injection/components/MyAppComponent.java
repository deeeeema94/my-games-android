package com.u.mygamesapp.injection.components;
import com.u.mygamesapp.activities.authentication.UpdateProfileActivity;
import com.u.mygamesapp.injection.modules.ApiInterfaceModule;
import com.u.mygamesapp.injection.scopes.PerActivityScope;

import dagger.Component;


@PerActivityScope
@Component(modules = {ApiInterfaceModule.class})
public interface MyAppComponent {
    void injectUploadImageActivity(UpdateProfileActivity updateProfileActivity);
}
