package com.u.mygamesapp.injection.modules;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.u.mygamesapp.api.ApiInterface;
import com.u.mygamesapp.injection.scopes.PerActivityScope;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.u.mygamesapp.api.APIConstants.API_IMAGES_URL;

@Module(includes = NetworkModule.class)
public class ApiInterfaceModule {
    public static Retrofit retrofit = null;

    @Provides
    @PerActivityScope
    public ApiInterface apiInterface(Retrofit retrofit) {
        return retrofit.create(ApiInterface.class);
    }

    @Provides
    @PerActivityScope
    public Retrofit retrofit(OkHttpClient okHttpClient) {

        if (retrofit == null) {

            retrofit = new Retrofit.Builder()
                    .baseUrl(API_IMAGES_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;

    }
}