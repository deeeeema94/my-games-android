package com.u.mygamesapp;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.u.mygamesapp.injection.components.DaggerMyAppComponent;
import com.u.mygamesapp.injection.components.MyAppComponent;

public class MyGamesApp extends Application {

    public static MyAppComponent myAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        initMyAppComponent();

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }

    private void initMyAppComponent() {
        if (myAppComponent == null) {
            myAppComponent = DaggerMyAppComponent.builder()
                    .build();
        }
    }
}
