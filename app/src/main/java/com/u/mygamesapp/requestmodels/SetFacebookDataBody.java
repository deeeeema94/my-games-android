package com.u.mygamesapp.requestmodels;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SetFacebookDataBody implements Serializable {

    @SerializedName("FacbookAccessToken")
    private String FacbookAccessToken;

    @SerializedName("FacbookEmail")
    private String FacbookEmail;

    @SerializedName("FacbookFirstName")
    private String FacbookFirstName;

    @SerializedName("FacbookLastName")
    private String FacbookLastName;

    @SerializedName("FacbookId")
    private String FacbookId;

    public SetFacebookDataBody(String facbookAccessToken, String facbookEmail, String facbookFirstName, String facbookLastName, String facbookId) {
        FacbookAccessToken = facbookAccessToken;
        FacbookEmail = facbookEmail;
        FacbookFirstName = facbookFirstName;
        FacbookLastName = facbookLastName;
        FacbookId = facbookId;
    }

    public String getFacbookAccessToken() {
        return FacbookAccessToken;
    }

    public void setFacbookAccessToken(String facbookAccessToken) {
        FacbookAccessToken = facbookAccessToken;
    }

    public String getFacbookEmail() {
        return FacbookEmail;
    }

    public void setFacbookEmail(String facbookEmail) {
        FacbookEmail = facbookEmail;
    }

    public String getFacbookFirstName() {
        return FacbookFirstName;
    }

    public void setFacbookFirstName(String facbookFirstName) {
        FacbookFirstName = facbookFirstName;
    }

    public String getFacbookLastName() {
        return FacbookLastName;
    }

    public void setFacbookLastName(String facbookLastName) {
        FacbookLastName = facbookLastName;
    }

    public String getFacbookId() {
        return FacbookId;
    }

    public void setFacbookId(String facbookId) {
        FacbookId = facbookId;
    }
}
