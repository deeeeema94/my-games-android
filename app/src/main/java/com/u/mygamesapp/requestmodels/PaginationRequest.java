package com.u.mygamesapp.requestmodels;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PaginationRequest implements Serializable {

    @SerializedName("page")
    private int page;

    @SerializedName("limit")
    private int limit;

    @SerializedName("taskTypeId")
    private int TaskTypeId;

    public PaginationRequest(int page, int limit) {
        this.page = page;
        this.limit = limit;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getTaskTypeId() {
        return TaskTypeId;
    }

    public void setTaskTypeId(int taskTypeId) {
        TaskTypeId = taskTypeId;
    }
}
