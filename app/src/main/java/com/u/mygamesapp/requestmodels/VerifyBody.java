package com.u.mygamesapp.requestmodels;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VerifyBody implements Serializable {

    @SerializedName("Id")
    private int Id;

    @SerializedName("Code")
    private String Code;

    public VerifyBody() {
    }

    public VerifyBody(int id, String code) {
        Id = id;
        Code = code;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }
}
