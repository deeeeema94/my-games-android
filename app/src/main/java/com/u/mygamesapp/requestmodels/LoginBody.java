package com.u.mygamesapp.requestmodels;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginBody implements Serializable {

    @SerializedName("Username")
    private String Username;

    @SerializedName("Password")
    private String Password;

    @SerializedName("FCMToken")
    private String FCMToken;

    public LoginBody() {
    }

    public LoginBody(String username, String password, String FCMToken) {
        Username = username;
        Password = password;
        this.FCMToken = FCMToken;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getFCMToken() {
        return FCMToken;
    }

    public void setFCMToken(String FCMToken) {
        this.FCMToken = FCMToken;
    }
}
