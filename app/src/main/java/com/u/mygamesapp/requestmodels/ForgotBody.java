package com.u.mygamesapp.requestmodels;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ForgotBody implements Serializable {

    @SerializedName("Username")
    private String Username;

    public ForgotBody() {
    }

    public ForgotBody(String username) {
        Username = username;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }
}
