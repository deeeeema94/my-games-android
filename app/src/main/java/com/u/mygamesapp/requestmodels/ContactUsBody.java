package com.u.mygamesapp.requestmodels;

import java.io.Serializable;

public class ContactUsBody implements Serializable {

    private String Subject;

    private String Message;

    public String PhoneNumber;

    public String FirstName;

    public String LastName;
    
    public ContactUsBody() {
    }

    public ContactUsBody(String subject, String message) {
        Subject = subject;
        Message = message;
    }

    public ContactUsBody(String subject, String message, String phoneNumber, String firstName, String lastName) {
        Subject = subject;
        Message = message;
        PhoneNumber = phoneNumber;
        FirstName = firstName;
        LastName = lastName;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}