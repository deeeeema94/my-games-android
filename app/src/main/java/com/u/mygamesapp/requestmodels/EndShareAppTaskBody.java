package com.u.mygamesapp.requestmodels;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EndShareAppTaskBody implements Serializable {

    @SerializedName("shareCode")
    private String ShareCode;

    public EndShareAppTaskBody(String shareCode) {
        ShareCode = shareCode;
    }
}
