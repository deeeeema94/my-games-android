package com.u.mygamesapp.requestmodels;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PredictBody implements Serializable {

    @SerializedName("SportMatchId")
    private int SportMatchId;

    @SerializedName("FirstTeamScore")
    private int FirstTeamScore;

    @SerializedName("SecondTeamScore")
    private int SecondTeamScore;

    public PredictBody() {
    }

    public PredictBody(int sportMatchId, int firstTeamScore, int secondTeamScore) {
        SportMatchId = sportMatchId;
        FirstTeamScore = firstTeamScore;
        SecondTeamScore = secondTeamScore;
    }

    public int getSportMatchId() {
        return SportMatchId;
    }

    public void setSportMatchId(int sportMatchId) {
        SportMatchId = sportMatchId;
    }

    public int getFirstTeamScore() {
        return FirstTeamScore;
    }

    public void setFirstTeamScore(int firstTeamScore) {
        FirstTeamScore = firstTeamScore;
    }

    public int getSecondTeamScore() {
        return SecondTeamScore;
    }

    public void setSecondTeamScore(int secondTeamScore) {
        SecondTeamScore = secondTeamScore;
    }
}
