package com.u.mygamesapp.requestmodels;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UpdateProfileBody implements Serializable {

    @SerializedName("FirstName")
    private String FirstName;

    @SerializedName("LastName")
    private String LastName;

    @SerializedName("Gender")
    private boolean Gender;

    @SerializedName("CityId")
    private int CityId;

    @SerializedName("ImageUrl")
    private String ImageUrl;

    public UpdateProfileBody() {
    }

    public UpdateProfileBody(String firstName, String lastName, boolean gender, int cityId) {
        FirstName = firstName;
        LastName = lastName;
        Gender = gender;
        CityId = cityId;
    }

    public UpdateProfileBody(String firstName, String lastName, boolean gender, int cityId, String imageUrl) {
        FirstName = firstName;
        LastName = lastName;
        Gender = gender;
        CityId = cityId;
        ImageUrl = imageUrl;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public boolean isGender() {
        return Gender;
    }

    public void setGender(boolean gender) {
        Gender = gender;
    }

    public int getCityId() {
        return CityId;
    }

    public void setCityId(int cityId) {
        CityId = cityId;
    }
}
