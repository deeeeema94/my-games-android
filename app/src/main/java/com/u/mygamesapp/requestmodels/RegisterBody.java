package com.u.mygamesapp.requestmodels;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RegisterBody implements Serializable {
    
    @SerializedName("Phone")
    private String Phone;

    @SerializedName("Password")
    private String Password;

    @SerializedName("FirstName")
    private String FirstName;

    @SerializedName("LastName")
    private String LastName;

    @SerializedName("TermsAndConditions")
    private boolean TermsAndConditions;

    @SerializedName("Gender")
    private boolean Gender;

    @SerializedName("CityId")
    private int CityId;

    @SerializedName("FCMToken")
    private String FCMToken;

    public RegisterBody() {
    }

    public RegisterBody(String phone, String password, String firstName, String lastName, boolean termsAndConditions, boolean gender, int cityId, String FCMToken) {
        Phone = phone;
        Password = password;
        FirstName = firstName;
        LastName = lastName;
        TermsAndConditions = termsAndConditions;
        Gender = gender;
        CityId = cityId;
        this.FCMToken = FCMToken;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public boolean isTermsAndConditions() {
        return TermsAndConditions;
    }

    public void setTermsAndConditions(boolean termsAndConditions) {
        TermsAndConditions = termsAndConditions;
    }

    public boolean isGender() {
        return Gender;
    }

    public void setGender(boolean gender) {
        Gender = gender;
    }

    public int getCityId() {
        return CityId;
    }

    public void setCityId(int cityId) {
        CityId = cityId;
    }

    public String getFCMToken() {
        return FCMToken;
    }

    public void setFCMToken(String FCMToken) {
        this.FCMToken = FCMToken;
    }
}
