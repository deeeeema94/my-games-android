package com.u.mygamesapp.requestmodels;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ChangePasswordBody implements Serializable {
    
    @SerializedName("OldPassword")
    private String OldPassword;

    @SerializedName("NewPassword")
    private String NewPassword;

    @SerializedName("ConfirmPassword")
    private String ConfirmPassword;

    public ChangePasswordBody() {
    }

    public ChangePasswordBody(String oldPassword, String newPassword, String confirmPassword) {
        OldPassword = oldPassword;
        NewPassword = newPassword;
        ConfirmPassword = confirmPassword;
    }

    public String getOldPassword() {
        return OldPassword;
    }

    public void setOldPassword(String oldPassword) {
        OldPassword = oldPassword;
    }

    public String getNewPassword() {
        return NewPassword;
    }

    public void setNewPassword(String newPassword) {
        NewPassword = newPassword;
    }

    public String getConfirmPassword() {
        return ConfirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        ConfirmPassword = confirmPassword;
    }
}
