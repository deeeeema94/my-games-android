package com.u.mygamesapp.requestmodels;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ResetBody implements Serializable {

    @SerializedName("Username")
    private String Username;

    @SerializedName("Password")
    private String Password;

    @SerializedName("Code")
    private String Code;

    public ResetBody() {
    }

    public ResetBody(String username, String password, String code) {
        Username = username;
        Password = password;
        Code = code;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }
}
