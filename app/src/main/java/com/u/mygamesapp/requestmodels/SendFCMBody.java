package com.u.mygamesapp.requestmodels;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SendFCMBody implements Serializable {

    @SerializedName("FCMToken")
    private String FCMToken;

    public SendFCMBody(String FCMToken) {
        this.FCMToken = FCMToken;
    }

    public String getFCMToken() {
        return FCMToken;
    }

    public void setFCMToken(String FCMToken) {
        this.FCMToken = FCMToken;
    }
}
