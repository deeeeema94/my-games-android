package com.u.mygamesapp.firebase;

import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.google.gson.Gson;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.models.Customer;
import com.u.mygamesapp.requestmodels.SendFCMBody;
import com.u.mygamesapp.responsemodels.APIResponse;

import static com.u.mygamesapp.api.APIConstants.SET_FCM_TOKEN;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;

public class MyFirebaseSendTokenToServer
{
    private String FCM_Token = "";
    private Context myContext;

    public MyFirebaseSendTokenToServer(Context context, String fCM_Token)
    {
        FCM_Token = fCM_Token;
        myContext = context;
    }

    public void Execute()
    {
        SendFCMBody inModel = new SendFCMBody(FCM_Token);
        Gson gson = new Gson();
        String json = gson.toJson(inModel);
        new SendGetJsonApi(myContext, SET_FCM_TOKEN, json,
                new CallBackListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<Customer> userInfoAPIResponse = getResponse(result, Customer.class);
                        }
                        catch ( Exception e)
                        {
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ).Execute();
    }
}