package com.u.mygamesapp.firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;

import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_FCM_TOKEN;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_LANG;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_TOKEN;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private final static AtomicInteger c = new AtomicInteger(0);

    public String Token = "", FCM_Token = "";
    /**
     * Called when message is received.
     *
     * param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]

    @Override
    public void onNewToken(@NonNull String refreshedToken) {
        super.onNewToken(refreshedToken);

        FirebaseMessaging.getInstance().subscribeToTopic("all-users");
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.


        if (!refreshedToken.equals("")) {
            // save new token
            writeSharedPreferenceFCMString(refreshedToken);
            // send to server
            sendRegistrationToServer(refreshedToken);
        }
    }

    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
        Token = readSharedPreferenceString();

        FCM_Token= token;

        if(!Token.equals("")) {
            new MyFirebaseSendTokenToServer(this, FCM_Token).Execute();
        }
    }


    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            sendNotification(remoteMessage.getData().get("body"),remoteMessage.getData().get("bodyAr"), remoteMessage.getData().get("action"));
        }

        if (remoteMessage.getNotification() != null) {
            sendNotification(remoteMessage.getNotification().getBody(),null,"");
        }
    }

    private String readSharedPreferenceString(){
        SharedPreferences sharedPreferences = getSharedPreferences(DEFAULT_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(PREF_TOKEN,"");
    }

    //write shared preferences in String
    private void writeSharedPreferenceFCMString(String value){

        SharedPreferences sharedPrefereSt = getSharedPreferences(DEFAULT_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefereSt.edit();
        editor.putString(PREF_FCM_TOKEN, value);
        editor.apply();
    }

    private void sendNotification(String messageBody , String messageBodyAr, String action) {

        if (messageBody==null) messageBody="";
        if (messageBodyAr==null) messageBodyAr = messageBody;


        SharedPreferences sharedPreferences = getSharedPreferences(DEFAULT_FILE_NAME + PREF_LANG, Context.MODE_PRIVATE);
        String lang =  sharedPreferences.getString("lang","");
        String sys_lang = Locale.getDefault().getLanguage();
        if (lang != null && (lang.equals("ar") || sys_lang.equals("ar")))
            messageBody = messageBodyAr;

        Intent intent;
        intent = new Intent(this, BaseActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                0);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager == null) return;

        String CHANNEL_ID  = "fcm_default_channel";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            CharSequence name = "my_channel";
            String Description = "This is my channel";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mChannel.setShowBadge(false);
            notificationManager.createNotificationChannel(mChannel);
        }

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this,CHANNEL_ID)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setContentText(messageBody)
                        .setSound(defaultSoundUri)
                        .setAutoCancel(false)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                        .setColorized(true)
                        .setContentIntent(pendingIntent);

        notificationManager.notify(c.incrementAndGet(), notificationBuilder.build());
    }


}