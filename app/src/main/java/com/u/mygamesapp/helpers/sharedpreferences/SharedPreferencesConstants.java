package com.u.mygamesapp.helpers.sharedpreferences;

public class SharedPreferencesConstants {

    public static final String DEFAULT_FILE_NAME = "my.basic.file.name";
    public static final String FIRST_INSTALL_FILE_NAME = "my.first.install.file.name";

    public static final String PREF_LANG = "com.ms.my.basic.template.lang";

    public static final String PREF_IS_AUTHENTICATED = "com.ms.my.basic.template.is.authenticated";
    public static final String PREF_USER_ID = "com.ms.my.basic.template.user_id";
    public static final String PREF_IS_ACTIVE = "com.ms.my.basic.template.is_active";
    public static final String PREF_USERNAME = "com.ms.my.basic.template.user.name";
    public static final String PREF_FIRST_NAME = "com.ms.my.basic.template.user.first";
    public static final String PREF_LAST_NAME = "com.ms.my.basic.template.user.last";
    public static final String PREF_FCM_TOKEN = "com.ms.my.basic.template.firebase.token";
    public static final String PREF_IS_FCM_TOKEN_SENT = "com.ms.my.basic.template.firebase.token.sent";
    public static final String PREF_TOKEN = "com.ms.my.basic.template.token";
    public static final String PREF_IS_LUCKY_VALID = "com.ms.my.basic.template.lucky.wheel";
    public static final String PREF_IS_BONUS_VALID = "com.ms.my.basic.template.bonus.wheel";
    public static final String PREF_LUCKY_CLOSED = "com.ms.my.basic.template.lucky.wheel.closed";
    public static final String PREF_IMAGE = "com.ms.my.basic.template.image";
    public static final String PREF_TOTAL_POINTS = "com.ms.my.basic.template.total.points";
    public static final String PREF_CURRENT_POINTS = "com.ms.my.basic.template.current.points";
    public static final String PREF_GROUP_ID = "com.ms.my.basic.template.group.id";
    public static final String PREF_GROUP_NAME = "com.ms.my.basic.template.group.name";
    public static final String PREF_GROUP_COLOR = "com.ms.my.basic.template.group.color";
    public static final String PREF_NEXT_GROUP_POINTS = "com.ms.my.basic.template.next.group.points";
    public static final String PREF_SHARE_CODE = "com.ms.my.basic.template.share.code";
    public static final String PREF_INSTALLED_FROM = "com.ms.my.basic.template.installed.from";
    public static final String PREF_DAILY_BONUS_LEVEL = "com.ms.my.basic.template.daily.bonus.level";

    public static final String PREF_FACEBOOK_TOKEN = "com.ms.my.basic.template.facebook.token";
    public static final String PREF_FACEBOOK_ID = "com.ms.my.basic.template.facebook.id";
    public static final String PREF_FACEBOOK_EMAIL = "com.ms.my.basic.template.facebook.email";
    public static final String PREF_FACEBOOK_FIRST_NAME = "com.ms.my.basic.template.facebook.first.name";
    public static final String PREF_FACEBOOK_LAST_NAME = "com.ms.my.basic.template.facebook.last.name";
    public static final String PREF_FACEBOOK_IMAGE = "com.ms.my.basic.template.facebook.image";

    public static final String PREF_IS_FIRST_INSTALL = "com.ms.my.basic.template.app.first.install";
    public static final String PREF_LOCAL_INTRO = "com.ms.my.basic.template.local.intro";
    public static final String PREF_SLIDER_FLIP_DURATION = "com.ms.my.basic.template.flip.duration";

    public static final String PREF_SHOW_MESSAGES_SNACK_BAR = "com.ms.my.basic.template.show.messages.snack.bar";
    public static final String PREF_MESSAGES_COUNT = "com.ms.my.basic.template.messages.count";

    public static final String PREF_INTRO_VIDEO_URL = "com.ms.my.basic.template.intro.video.url";

}
