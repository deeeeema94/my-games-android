package com.u.mygamesapp.helpers;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.airbnb.lottie.LottieAnimationView;
import com.u.mygamesapp.R;

import static com.u.mygamesapp.helpers.Constants.DIALOG_TYPE_LOGOUT;
import static com.u.mygamesapp.helpers.Constants.DIALOG_TYPE_MESSAGE_SENT;
import static com.u.mygamesapp.helpers.Constants.DIALOG_TYPE_PREDICT;
import static com.u.mygamesapp.helpers.Constants.DIALOG_TYPE_PRIZE;
import static com.u.mygamesapp.helpers.Constants.DIALOG_TYPE_SPINNER;

public class CustomDialog extends Dialog implements
        View.OnClickListener {

    private String mType;
    public Button yes, no, ok;
    public TextView prizeName, password, res, groupName, total, team1Name, team2Name;
    public EditText team1Score, team2Score;
    public ImageView prizeImg, team1Falg, team2Flag;
    public CardView cardView;
    public LottieAnimationView iconSad, icon;

    public CustomDialog(AppCompatActivity a, String type) {
        super(a);
        this.mType = type;
    }

    public CustomDialog(Context a, String type) {
        super(a);
        this.mType = type;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        if (DIALOG_TYPE_LOGOUT.equals(mType)) {
            setContentView(R.layout.dialog_custom_logout);
            yes = findViewById(R.id.yes_btn);
            no = findViewById(R.id.no_btn);
            yes.setOnClickListener(this);
            no.setOnClickListener(this);
        }

        else if (DIALOG_TYPE_PRIZE.equals(mType)) {
            setContentView(R.layout.dialog_custom_prize);
            icon = findViewById(R.id.icon_happy);
            iconSad = findViewById(R.id.icon_sad);
            ok = findViewById(R.id.ok_btn);
            ok.setOnClickListener(this);
            prizeImg = findViewById(R.id.prize_img);
            prizeName = findViewById(R.id.prize_name);
        }

        else if(DIALOG_TYPE_SPINNER.equals(mType)){
            setContentView(R.layout.dialog_custom_spinner);
        }
        else if(DIALOG_TYPE_PREDICT.equals(mType)){
            setContentView(R.layout.dialog_custom_predict);
            team1Name = findViewById(R.id.team1_name);
            team2Name = findViewById(R.id.team2_name);
            team1Falg = findViewById(R.id.team1_flag);
            team2Flag = findViewById(R.id.team2_flag);
            team1Score = findViewById(R.id.team1_score);
            team2Score = findViewById(R.id.team2_score);
            yes = findViewById(R.id.yes_btn);
            no = findViewById(R.id.no_btn);
            yes.setOnClickListener(this);
            no.setOnClickListener(this);
        }
        else if(DIALOG_TYPE_MESSAGE_SENT.equals(mType)){
            setContentView(R.layout.dialog_custom_spinner_sending);
        }
    }
    @Override
    public void onClick(View v) { }
}
