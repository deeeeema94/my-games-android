package com.u.mygamesapp.helpers;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.u.mygamesapp.R;

import java.text.ParseException;

public class FAQAnswerViewHolder extends ChildViewHolder implements View.OnClickListener{

    private TextView answerTextView;
    private Context mContext;

    public FAQAnswerViewHolder(Context context, @NonNull View itemView) {
        super(context, itemView);
        mContext = context;
        answerTextView = itemView.findViewById(R.id.answer);
    }

    public void bind(@NonNull String answer) throws ParseException {
        answerTextView.setText(answer);
    }
}
