package com.u.mygamesapp.helpers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.u.mygamesapp.responsemodels.APIResponse;
import com.u.mygamesapp.responsemodels.PagedAPIResponse;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class JsonParser<T> {

    private static Type getType(final Class<?> rawClass, final Class<?> parameterClass) {
        return new ParameterizedType() {
            @Override
            public Type[] getActualTypeArguments() {
                return new Type[]{parameterClass};
            }

            @Override
            public Type getRawType() {
                return rawClass;
            }

            @Override
            public Type getOwnerType() {
                return null;
            }

        };
    }

    public static <T> APIResponse<T> getResponse(String response, final Class<T> dataClass) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        try {
            return gson.fromJson(response, getType(APIResponse.class, dataClass));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static <T> PagedAPIResponse<T> getPagedResponse(String response, final Class<T> dataClass) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        try {
            return gson.fromJson(response, getType(PagedAPIResponse.class, dataClass));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}