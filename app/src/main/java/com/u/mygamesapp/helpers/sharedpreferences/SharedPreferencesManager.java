package com.u.mygamesapp.helpers.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;

public class SharedPreferencesManager {

    public static void saveToPreferences(Context context, String prefFileName, String preferenceName, String preferenceValue) {
        if (prefFileName == null) {
            prefFileName = DEFAULT_FILE_NAME;
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenceName, preferenceValue);
        editor.apply();
    }

    public static String readFromPreferences(Context context, String prefFileName, String preferenceName, String defaultValue) {
        if (prefFileName == null) {
            prefFileName = DEFAULT_FILE_NAME;
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName, defaultValue);
    }

    public static void saveIntegerToPreferences(Context context, String prefFileName, String preferenceName, int preferenceValue) {
        if (prefFileName == null) {
            prefFileName = DEFAULT_FILE_NAME;
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(preferenceName);
        editor.apply();
        editor.putInt(preferenceName, preferenceValue);
        editor.apply();
    }

    public static int readIntegerFromPreferences(Context context, String prefFileName, String preferenceName, int defaultValue) {
        if (prefFileName == null) {
            prefFileName = DEFAULT_FILE_NAME;
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(preferenceName, defaultValue);
    }

    public static void clearUserPreferences(Context context, String prefFileName) {
        if (prefFileName == null) {
            prefFileName = DEFAULT_FILE_NAME;
        }
        SharedPreferences prefs = context.getSharedPreferences(
                prefFileName, Context.MODE_PRIVATE);
        prefs.edit().clear().apply();
    }
}
