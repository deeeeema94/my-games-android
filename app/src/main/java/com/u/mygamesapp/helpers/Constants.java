package com.u.mygamesapp.helpers;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.u.mygamesapp.R;

public class Constants {

    public static final String ENGLISH_LANG = "en";
    public static final String ARABIC_LANG = "ar";

    public static final String DIALOG_TYPE_LOGOUT = "logout";
    public static final String DIALOG_TYPE_PRIZE = "prize";
    public static final String DIALOG_TYPE_UPGRADE_GROUP = "upgrade_group";
    public static final String DIALOG_TYPE_SPINNER = "spinner";
    public static final String DIALOG_TYPE_MESSAGE_SENT = "sent";
    public static final String DIALOG_TYPE_PREDICT = "predict";

    public static final String TASK_TYPE_SURVEY = "survey";
    public static final String TASK_TYPE_QUIZ = "quiz";
    public static final String TASK_TYPE_INSTALL_APP = "install_app";
    public static final String TASK_TYPE_YOUTUBE_SUBSCRIBE = "youtube_subscribe";
    public static final String TASK_TYPE_MATCH = "sport_match";
    public static final String TASK_TYPE_WATCH_LOCAL_AD = "watch_ad";
    public static final String TASK_TYPE_SHARE_THIS_APP = "share_games_app";
    public static final String TASK_TYPE_GENERAL = "general";

    public static final int LUCKY_WHEEL_REQUEST_CODE = 1;
    public static final int INSTALL_APP_REQUEST_CODE = 2;
    public static final int YOUTUBE_SUBSCRIBE_REQUEST_CODE = 3;
    public static final int WATCH_LOCAL_AD_REQUEST_CODE = 4;
    public static final int OPEN_LINK_REQUEST_CODE = 5;

    public static final int REQUEST_ACCOUNT_PICKER = 1000;
    public static final int REQUEST_AUTHORIZATION = 1001;
    public static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    public static final int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;


    public static final String ACCEPTED_STATUS = "accepted";
    public static final String REJECTED_STATUS = "rejected";
    public static final String PENDING_STATUS = "requested";


    public static void showGreenToast(Context context, String msg){
        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
        View view = toast.getView();
        view.getBackground().setColorFilter(context.getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        TextView text = view.findViewById(android.R.id.message);
        text.setTextColor(context.getResources().getColor(R.color.white));
        toast.show();
    }

    public static void showRedToast(Context context, String msg){
        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
        View view = toast.getView();
        view.getBackground().setColorFilter(Color.DKGRAY, PorterDuff.Mode.SRC_IN);
        TextView text = view.findViewById(android.R.id.message);
        text.setTextColor(context.getResources().getColor(R.color.white));
        toast.show();
    }
}
