package com.u.mygamesapp.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.ramijemli.percentagechartview.PercentageChartView;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.authentication.LoginActivity;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.models.Content;
import com.u.mygamesapp.requestmodels.PaginationRequest;
import com.u.mygamesapp.responsemodels.APIResponse;

import static com.u.mygamesapp.api.APIConstants.API_IMAGES_URL;
import static com.u.mygamesapp.api.APIConstants.INTRO_VIDEO;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class FullScreenVideo extends AppCompatActivity {

    VideoView videoView;
    MediaController controller;
    Uri uri;
    int pos;
    String videoUrl;
    private Window window;
    private int currentApiVersion;
    ProgressBar progDailog;
    protected PercentageChartView mChart;
    private static boolean isWatch = false;
    public static Gson gson = new Gson();

    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar
            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };
    private CountDownTimer countDownTimer;

    @Override
    @SuppressLint("NewApi")
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_full_screen_video);
        currentApiVersion = Build.VERSION.SDK_INT;

        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

        // This work only for android 4.4+
        if(currentApiVersion >= Build.VERSION_CODES.KITKAT)
        {

            getWindow().getDecorView().setSystemUiVisibility(flags);

            // Code below is to handle presses of Volume up or Volume down.
            // Without this, after pressing volume buttons, the navigation bar will
            // show up and won't hide
            final View decorView = getWindow().getDecorView();
            decorView
                    .setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
                    {

                        @Override
                        public void onSystemUiVisibilityChange(int visibility)
                        {
                            if((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0)
                            {
                                decorView.setSystemUiVisibility(flags);
                            }
                        }
                    });
        }

        window = this.getWindow();

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(Color.BLACK);
        }
        mVisible = true;
        mContentView = findViewById(R.id.fullscreen_content);

        // Set up the user interaction to manually show or hide the system UI.
        mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });


        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        // findViewById(R.id.dummy_button).setOnTouchListener(mDelayHideTouchListener);

        videoView = findViewById(R.id.fullscreen_content);
        controller = new MediaController(this);
        controller.setPadding(0, 0, 0, 120);
        controller.setAnchorView(videoView);
        mChart = findViewById(R.id.view_id);

        Intent intent = getIntent();
        videoUrl = intent.getStringExtra("videoUrl");
        isWatch = intent.getBooleanExtra("isWatch", false);

        if(videoUrl == null || videoUrl.equals("")){
            PaginationRequest request = new PaginationRequest(1, 200);
            String json = gson.toJson(request);
            new SendGetJsonApi(this, INTRO_VIDEO, json,
                    new CallBackListener() {
                        @Override
                        public void onFinish(String results) {
                            try {
                                APIResponse<Content> contentAPIResponse = getResponse(results, Content.class);
                                if (contentAPIResponse != null && contentAPIResponse.getResult().equals("success")) {
                                    Content video = contentAPIResponse.getContent();
                                    if(!video.getImageUrl().startsWith("http"))
                                        videoUrl = API_IMAGES_URL + video.getImageUrl();
                                    else
                                        videoUrl = video.getImageUrl();
                                    OnSuccess();
                                }
                                else {
                                    startActivity(new Intent(FullScreenVideo.this, LoginActivity.class));
                                }
                            }
                            catch (Exception e) {
                                startActivity(new Intent(FullScreenVideo.this, LoginActivity.class));
                            }
                        }
                        @Override
                        public void onProgress(int process) {
                        }
                    }
                    ,true).Execute();
        }
        else {
            OnSuccess();
        }

        if (savedInstanceState != null) {
            int i1 = savedInstanceState.getInt("pos");
            videoView.seekTo(i1);
        }
    }

    private void OnSuccess() {
        uri = Uri.parse(videoUrl);

        if(isWatch){
            videoView.setMediaController(controller);
            mChart.setVisibility(View.INVISIBLE);
        }
        else {
            videoView.setMediaController(null);
            mChart.setVisibility(View.VISIBLE);
        }
        videoView.setVideoURI(uri);
        videoView.requestFocus();
        videoView.start();
        progDailog = findViewById(R.id.progress);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            public void onPrepared(MediaPlayer mp) {
                progDailog.setVisibility(View.GONE);
                if(!isWatch){
                    final int[] counter = {1};
                    final float[] percentage = new float[1];
                    try {
                        final int videoDuration = getIntent().getIntExtra("videoDuration", 0);
                        countDownTimer = new CountDownTimer(videoDuration*1000, 1000){
                            public void onTick(long millisUntilFinished){
                                percentage[0] = (float) (100*counter[0])/videoDuration;
                                mChart.setProgress(percentage[0], false);
                                counter[0]++;
                            }
                            public  void onFinish(){
                                setResult(RESULT_OK);
                                finish();
                            }
                        }.start();
                    }
                    catch (Exception e){}
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        videoView.pause();
        pos = videoView.getCurrentPosition();
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoView.seekTo(pos);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("pos", videoView.getCurrentPosition());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    @SuppressLint("NewApi")
    @Override
    public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        if(currentApiVersion >= Build.VERSION_CODES.KITKAT && hasFocus)
        {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    @Override
    public void onBackPressed() {
        if(!isWatch){
            try{
                countDownTimer.cancel();
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        super.onBackPressed();
    }
}
