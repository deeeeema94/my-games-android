package com.u.mygamesapp.activities.authentication;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.requestmodels.EndShareAppTaskBody;

import java.util.Locale;

import static com.u.mygamesapp.activities.GodFatherActivity.currLang;
import static com.u.mygamesapp.activities.GodFatherActivity.hideKeyboard;
import static com.u.mygamesapp.api.APIConstants.ADD_SHARE_APP_END;
import static com.u.mygamesapp.api.APIConstants.TASKS;
import static com.u.mygamesapp.helpers.Constants.ARABIC_LANG;
import static com.u.mygamesapp.helpers.Constants.ENGLISH_LANG;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.FIRST_INSTALL_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_IS_FIRST_INSTALL;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_LANG;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readFromPreferences;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.saveToPreferences;

public class InstalledFromActivity extends AppCompatActivity {

    private EditText codeEdit;
    private static Gson gson = new Gson();

    @Override
    protected void attachBaseContext(Context context) {
        String lang = readFromPreferences(context, DEFAULT_FILE_NAME, PREF_LANG, "");
        String sys_lang = ARABIC_LANG;
        if ((lang != null && lang.equals(ARABIC_LANG)) || (lang != null && lang.equals(ENGLISH_LANG)))
            currLang = lang;
        else currLang = sys_lang;
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(currLang);
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocale(locale);
            context = context.createConfigurationContext(config);
        } else {
            config.locale = locale;
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        }
        saveToPreferences(context, DEFAULT_FILE_NAME, PREF_LANG, currLang);
        super.attachBaseContext(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_installed_from);
        saveToPreferences(this, FIRST_INSTALL_FILE_NAME, PREF_IS_FIRST_INSTALL, "False");
        initLayout();
    }

    private void initLayout() {
        codeEdit = findViewById(R.id.edit_code);
    }

    public void CloseAction(View view) {
        finish();
    }

    public void SubmitBtnAction(View view) {
        hideKeyboard(this);

        String code = codeEdit.getText().toString().trim();
        if(code.equals("")){
            codeEdit.setError(getString(R.string.code_required));
            codeEdit.setFocusable(true);
            return;
        }

        EndShareAppTaskBody endShareAppTaskBody = new EndShareAppTaskBody(code);

        String json = gson.toJson(endShareAppTaskBody);
        new SendGetJsonApi(this, TASKS + ADD_SHARE_APP_END, json,
                new CallBackListener() {
                    @Override
                    public void onFinish(String result)
                    {
                        startActivity(new Intent(getBaseContext() , BaseActivity.class));
                        finish();
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }, true).Execute();
    }

    public void SkipAction(View view) {
        startActivity(new Intent(getBaseContext() , BaseActivity.class));
        finish();
    }
}