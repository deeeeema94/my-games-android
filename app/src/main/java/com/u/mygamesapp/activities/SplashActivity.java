package com.u.mygamesapp.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.authentication.LoginActivity;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.models.Condition;
import com.u.mygamesapp.models.Content;
import com.u.mygamesapp.models.Intro;
import com.u.mygamesapp.requestmodels.PaginationRequest;
import com.u.mygamesapp.responsemodels.APIResponse;
import com.u.mygamesapp.responsemodels.PagedAPIResponse;

import java.util.List;

import static com.u.mygamesapp.activities.IntroActivity.screensBriefs;
import static com.u.mygamesapp.activities.IntroActivity.screensImages;
import static com.u.mygamesapp.activities.IntroActivity.screensTitles;
import static com.u.mygamesapp.api.APIConstants.API_IMAGES_URL;
import static com.u.mygamesapp.api.APIConstants.CONDITIONS;
import static com.u.mygamesapp.api.APIConstants.INTRO_IMAGES;
import static com.u.mygamesapp.helpers.JsonParser.getPagedResponse;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_INTRO_VIDEO_URL;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_IS_FCM_TOKEN_SENT;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_IS_FIRST_INSTALL;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_LOCAL_INTRO;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_SLIDER_FLIP_DURATION;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readIntegerFromPreferences;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.saveIntegerToPreferences;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.saveToPreferences;

public class SplashActivity extends GodFatherActivity {

    private int isFirstInstall;
    private int localIntro;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        saveToPreferences(this, DEFAULT_FILE_NAME, PREF_IS_FCM_TOKEN_SENT, "False");

        TextView appVersionTxt = findViewById(R.id.version);
        appVersionTxt.setText(String.format("V %s", appVersion));
//        clearUserPreferences(SplashActivity.this, FIRST_INSTALL_FILE_NAME);
//        clearUserPreferences(SplashActivity.this, DEFAULT_FILE_NAME);
        getConditions();
    }

    private void getConditions() {
        PaginationRequest request = new PaginationRequest(1, 200);
        String json = gson.toJson(request);
        new SendGetJsonApi(this, CONDITIONS, json,
                new CallBackListener() {
                    @Override
                    public void onFinish(String results)
                    {
                        try {
                            PagedAPIResponse<Condition[]> pagedAPIResponse = getPagedResponse(results, Condition[].class);
                            if (pagedAPIResponse != null && pagedAPIResponse.getMessage().equals("success")) {
                                Condition[] appConditions = pagedAPIResponse.getContent().getContent();
                                if (appConditions != null) {
                                    for (int i = 0; i < appConditions.length; i++) {
                                        if (appConditions[i].getName().equals("local_intro")) {
                                            saveIntegerToPreferences(SplashActivity.this, DEFAULT_FILE_NAME, PREF_LOCAL_INTRO,
                                                    Integer.parseInt(appConditions[i].getValue()));
                                        }
                                        if (appConditions[i].getName().equals("slides_duration")) {
                                            saveIntegerToPreferences(SplashActivity.this, DEFAULT_FILE_NAME, PREF_SLIDER_FLIP_DURATION,
                                                    Integer.parseInt(appConditions[i].getValue()));
                                        }
                                    }
                                }
                            }
                            isFirstInstall = readIntegerFromPreferences(SplashActivity.this, DEFAULT_FILE_NAME, PREF_IS_FIRST_INSTALL, 1);
                            localIntro = readIntegerFromPreferences(SplashActivity.this, DEFAULT_FILE_NAME, PREF_LOCAL_INTRO, 1);

                            if (isFirstInstall == 1) {
                                getImages();
                            }

                            else {
                                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                                finish();
                            }
                        }
                        catch (Exception e){
                            startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                        }
                    }
                    @Override
                    public void onProgress(int process) {}
                },false).Execute();
    }

    private void getImages(){
        if(localIntro == 1){
            String pictures0 = "img1";
            String pictures1 = "img2";
            String pictures2 = "img3";

            Content[] contents = new Content[3];
            contents[0] = new Content(getString(R.string.f_screen_title), getString(R.string.f_screen_desc), pictures0);
            contents[1] = new Content(getString(R.string.s_screen_title), getString(R.string.s_screen_desc), pictures1);
            contents[2] = new Content(getString(R.string.t_screen_title), getString(R.string.t_screen_desc), pictures2);

            screensTitles = new String[3];
            screensBriefs = new String[3];
            screensImages = new String[3];
            for (int i = 0; i < 3; i++) {
                screensTitles[i] = contents[i].getTitle();
                screensBriefs[i] = contents[i].getScript();
                screensImages[i] = contents[i].getImageUrl();
            }
            startActivity(new Intent(SplashActivity.this, IntroActivity.class));
            finish();
        }
        else {
            PaginationRequest request = new PaginationRequest(1, 200);
            String json = gson.toJson(request);
            new SendGetJsonApi(this, INTRO_IMAGES, json,
                    new CallBackListener() {
                        @Override
                        public void onFinish(String results) {
                            try {
                                APIResponse<Intro> introAPIResponse = getResponse(results, Intro.class);
                                if (introAPIResponse != null && introAPIResponse.getResult().equals("success")) {
                                    List<Content> images = introAPIResponse.getContent().getIntroImages();
                                    Content video = introAPIResponse.getContent().getIntroVideo();

                                    saveToPreferences(SplashActivity.this, DEFAULT_FILE_NAME, PREF_INTRO_VIDEO_URL, video.getImageUrl());

                                    if (images != null) {
                                        screensTitles = new String[images.size()];
                                        screensBriefs = new String[images.size()];
                                        screensImages = new String[images.size()];
                                        for (int i = 0; i < images.size(); i++) {
                                            screensTitles[i] = images.get(i).getTitle();
                                            screensBriefs[i] = images.get(i).getScript();
                                            screensImages[i] = API_IMAGES_URL + images.get(i).getImageUrl();
                                        }
                                        startActivity(new Intent(SplashActivity.this, IntroActivity.class));
                                        finish();
                                    }
                                }
                                else {
                                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                                }
                            }
                            catch (Exception e) {
                                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                            }
                        }
                        @Override
                        public void onProgress(int process) {
                        }
                    }
                    ,false).Execute();
        }
    }
}
