package com.u.mygamesapp.activities.authentication;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;
import com.u.mygamesapp.activities.GodFatherActivity;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.models.Customer;
import com.u.mygamesapp.requestmodels.ResendCodeBody;
import com.u.mygamesapp.requestmodels.VerifyBody;
import com.u.mygamesapp.responsemodels.APIResponse;

import static com.u.mygamesapp.api.APIConstants.RESEND_CODE;
import static com.u.mygamesapp.api.APIConstants.VERIFY;
import static com.u.mygamesapp.helpers.Constants.showGreenToast;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.FIRST_INSTALL_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_IS_FIRST_INSTALL;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_USERNAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_USER_ID;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readFromPreferences;

public class VerifyActivity extends GodFatherActivity {

    private EditText codeEdit;
    private TextView resultTxt;
    private String code = "";
    private String userName = "";
    private int userId = 0;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activation);
        setTitle(getString(R.string.verify_account));
        initData();
        initLayout();
    }

    private void initData() {
        userName = readFromPreferences(this, DEFAULT_FILE_NAME, PREF_USERNAME, "");
        userId = Integer.parseInt(readFromPreferences(this, DEFAULT_FILE_NAME, PREF_USER_ID, "0"));
    }

    private void initLayout() {
        resultTxt = findViewById(R.id.txt_result);
        codeEdit = findViewById(R.id.edit_code);
        EditText editPhone = findViewById(R.id.edit_phone);
        editPhone.setText(userName);
        codeEdit.setText(code);
    }

    public void ResendActiveCodeAction(View v) {
        hideKeyboard(this);
        resultTxt.setText("");

        ResendCodeBody inModel = new ResendCodeBody(userName);
        String json = gson.toJson(inModel);
        new SendGetJsonApi(this, RESEND_CODE, json,
                new CallBackListener() {
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<Customer> userInfoAPIResponse = getResponse(result, Customer.class);
                            if (userInfoAPIResponse != null && userInfoAPIResponse.getResult().equals("success"))
                            {
                                String msg = getString(R.string.code_sent);
                                showGreenToast(getApplicationContext(), msg);
                            }
                            else
                            {
                                String error_des = userInfoAPIResponse != null ? userInfoAPIResponse.getResult() : null;
                                if((error_des != null && !error_des.equals(""))){
                                    resultTxt.setText(error_des);
                                }else
                                {
                                    resultTxt.setText(getString(R.string.went_wrong));
                                }
                            }
                        }
                        catch ( Exception e)
                        {
                            resultTxt.setText(getString(R.string.went_wrong));
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        , true).Execute();
    }

    public void VerifyAction(View view) {
        hideKeyboard(this);
        resultTxt.setText("");

        code = codeEdit.getText().toString().trim();

        if (code.equals("")) {
            codeEdit.setError(getString(R.string.field_required));
            codeEdit.requestFocus();
            return;
        }
        VerifyBody inModel = new VerifyBody(userId, code);
        String json = gson.toJson(inModel);
        new SendGetJsonApi(this, VERIFY, json,
                new CallBackListener() {
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<Customer> userInfoAPIResponse = getResponse(result, Customer.class);
                            if (userInfoAPIResponse != null && userInfoAPIResponse.getResult().equals("success"))
                            {
                                Customer customer = userInfoAPIResponse.getContent();
                                saveUserData(customer);
                                boolean isFirstInstall = Boolean.parseBoolean(readFromPreferences(VerifyActivity.this, FIRST_INSTALL_FILE_NAME, PREF_IS_FIRST_INSTALL, "True"));
                                if(isFirstInstall && (customer.getInstalledFrom() == null || customer.getInstalledFrom().equals(""))){
                                    startActivity(new Intent(getBaseContext() , InstalledFromActivity.class));
                                }
                                else {
                                    startActivity(new Intent(getBaseContext() , BaseActivity.class));
                                }
                                finish();
                            }
                            else
                            {
                                String error_des = null;
                                if (userInfoAPIResponse != null) {
                                    error_des = userInfoAPIResponse.getResult();
                                }
                                if((error_des != null && !error_des.equals(""))){
                                    resultTxt.setText(error_des);
                                }else
                                {
                                    resultTxt.setText(getString(R.string.went_wrong));
                                }
                            }
                        }
                        catch ( Exception e)
                        {
                            resultTxt.setText(getString(R.string.went_wrong));
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
                , true).Execute();
    }

    public void LoginBtnAction(View view) {
        Intent LoginIntent = new Intent(VerifyActivity.this, LoginActivity.class);
        startActivity(LoginIntent);
        finish();
    }

}


