package com.u.mygamesapp.activities.authentication;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.request.RequestOptions;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.SplashActivity;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.helpers.CustomDialog;
import com.u.mygamesapp.requestmodels.SetFacebookDataBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;

import static com.u.mygamesapp.activities.GodFatherActivity.currLang;
import static com.u.mygamesapp.api.APIConstants.SET_FACEBOOK_DATA;
import static com.u.mygamesapp.helpers.Constants.ARABIC_LANG;
import static com.u.mygamesapp.helpers.Constants.DIALOG_TYPE_LOGOUT;
import static com.u.mygamesapp.helpers.Constants.ENGLISH_LANG;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_FACEBOOK_EMAIL;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_FACEBOOK_FIRST_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_FACEBOOK_ID;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_FACEBOOK_IMAGE;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_FACEBOOK_LAST_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_FACEBOOK_TOKEN;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_LANG;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.clearUserPreferences;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readFromPreferences;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.saveToPreferences;

public class ProfileSettingsActivity extends AppCompatActivity {

    private CallbackManager callbackManager;
    private static final String EMAIL = "email";
    private static Gson gson = new Gson();

    @Override
    protected void attachBaseContext(Context context) {
        String lang = readFromPreferences(context, DEFAULT_FILE_NAME, PREF_LANG, "");
        String sys_lang = ARABIC_LANG;
        if ((lang != null && lang.equals(ARABIC_LANG)) || (lang != null && lang.equals(ENGLISH_LANG)))
            currLang = lang;
        else currLang = sys_lang;
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(currLang);
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocale(locale);
            context = context.createConfigurationContext(config);
        } else {
            config.locale = locale;
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        }
        saveToPreferences(context, DEFAULT_FILE_NAME, PREF_LANG, currLang);
        super.attachBaseContext(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_profile_settings);

        initLayout();
    }

    private void initLayout() {
        TextView changePassTxt = findViewById(R.id.change_pass_txt);
        changePassTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view12) {
                ChangePasswordAction();
            }
        });

        TextView logoutTxt = findViewById(R.id.logout_txt);
        logoutTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view13) {
                LogoutAction();
            }
        });

        TextView edit_info = findViewById(R.id.edit_info);
        edit_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view13) {
                Intent updateProfileIntent = new Intent(ProfileSettingsActivity.this, UpdateProfileActivity.class);
                startActivity(updateProfileIntent);
            }
        });

        checkLoginStatus();

        final LoginButton loginButton = findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList(EMAIL));
//        loginButton.setFragment(this);

        callbackManager = CallbackManager.Factory.create();
        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                loadUserProfile(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                String c = "";
            }

            @Override
            public void onError(FacebookException exception) {
                String c = "";
            }
        });
    }

    public void ChangePasswordAction() {
        Intent intent = new Intent(ProfileSettingsActivity.this, ChangePasswordActivity.class);
        startActivity(intent);
    }

    public void LogoutAction() {
        final CustomDialog cdd = new CustomDialog(ProfileSettingsActivity.this, DIALOG_TYPE_LOGOUT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(cdd.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        cdd.show();
        cdd.yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logOut();
                cdd.dismiss();
            }
        });
        cdd.no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cdd.dismiss();
            }
        });
    }

    private void logOut() {
        clearUserPreferences(ProfileSettingsActivity.this, DEFAULT_FILE_NAME);
        FragmentManager fm = getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
        Intent intent = new Intent(ProfileSettingsActivity.this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        ProfileSettingsActivity.this.finish();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void loadUserProfile(final AccessToken newAccessToken)
    {
        GraphRequest request = GraphRequest.newMeRequest(newAccessToken, new GraphRequest.GraphJSONObjectCallback() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @SuppressLint("CheckResult")
            @Override
            public void onCompleted(JSONObject object, GraphResponse response)
            {
                try {
                    String first_name = object.getString("first_name");
                    String last_name = object.getString("last_name");
                    String email = object.getString("email");
                    String id = object.getString("id");
                    JSONObject pictureInfo = object.getJSONObject("picture");
                    String picture = pictureInfo.getJSONObject("data").getString("url");

                    saveToPreferences(ProfileSettingsActivity.this, DEFAULT_FILE_NAME, PREF_FACEBOOK_FIRST_NAME, first_name);
                    saveToPreferences(ProfileSettingsActivity.this, DEFAULT_FILE_NAME, PREF_FACEBOOK_LAST_NAME, last_name);
                    saveToPreferences(ProfileSettingsActivity.this, DEFAULT_FILE_NAME, PREF_FACEBOOK_EMAIL, email);
                    saveToPreferences(ProfileSettingsActivity.this, DEFAULT_FILE_NAME, PREF_FACEBOOK_ID, id);
                    saveToPreferences(ProfileSettingsActivity.this, DEFAULT_FILE_NAME, PREF_FACEBOOK_IMAGE, picture);
                    saveToPreferences(ProfileSettingsActivity.this, DEFAULT_FILE_NAME, PREF_FACEBOOK_TOKEN, String.valueOf(newAccessToken.getToken()));

                    SetFacebookDataBody dataBody = new SetFacebookDataBody(newAccessToken.getToken() , email, first_name, last_name, id);
                    setFacebookData(dataBody);
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.dontAnimate();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields","first_name,last_name,email,id,picture");
        request.setParameters(parameters);
        request.executeAsync();

    }

    private void setFacebookData(SetFacebookDataBody data){

        String json = gson.toJson(data);
        new SendGetJsonApi(ProfileSettingsActivity.this, SET_FACEBOOK_DATA, json,
                new CallBackListener() {
                    @Override
                    public void onFinish(String result)
                    {
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }, false).Execute();
    }

    private void checkLoginStatus()
    {
        if(AccessToken.getCurrentAccessToken()!=null)
        {
            loadUserProfile(AccessToken.getCurrentAccessToken());
        }
    }

    public void CloseAction(View view) {
        finish();
    }
}