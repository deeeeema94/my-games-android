package com.u.mygamesapp.activities;


import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.authentication.LoginActivity;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.models.Content;
import com.u.mygamesapp.responsemodels.APIResponse;

import static com.u.mygamesapp.api.APIConstants.ABOUT_US;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;

public class AboutUsActivity extends GodFatherActivity implements SwipeRefreshLayout.OnRefreshListener{

    private TextView aboutTxt;
    private String appVersion;
    private SwipeRefreshLayout swipeRefreshLayout;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater != null ? inflater.inflate(R.layout.activity_about_us, null, false) : null;
        contentLayout.addView(contentView, 0);
        setTitle(R.string.about_us);

        aboutTxt = findViewById(R.id.txt_about);
        TextView versionTxt = findViewById(R.id.txt_app_version);

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            appVersion = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        versionTxt.setText(String.format("V %s", appVersion));

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getData();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
        );

        getData();
    }


    @Override
    public void onRefresh() {
        if (swipeRefreshLayout.isRefreshing()) {
            getData();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void getData() {
        new SendGetJsonApi(this, ABOUT_US, "",
                new CallBackListener() {
                    @Override
                    public void onFinish(String results)
                    {
                        try {
                            APIResponse<Content> aboutContent = getResponse(results, Content.class);
                            if (aboutContent != null && aboutContent.getContent() != null && aboutContent.getContent().getScript() != null) {
                                aboutTxt.setText(aboutContent.getContent().getTitle());
                                WebView wv = findViewById(R.id.web_view_about);
                                wv.setVisibility(View.VISIBLE);
                                final String mimeType = "text/html";
                                final String encoding = "UTF-8";
                                String html = aboutContent.getContent().getScript();
                                wv.loadDataWithBaseURL("", html, mimeType, encoding, "");
                            }
                        }
                        catch (Exception e){
                            startActivity(new Intent(AboutUsActivity.this, LoginActivity.class));
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }).Execute();
    }
}
