package com.u.mygamesapp.activities.authentication;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.u.intlphoneinput.IntlPhoneInput;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;
import com.u.mygamesapp.activities.GodFatherActivity;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.models.Customer;
import com.u.mygamesapp.requestmodels.LoginBody;
import com.u.mygamesapp.responsemodels.APIResponse;

import java.util.Objects;

import static com.u.mygamesapp.api.APIConstants.LOGIN;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.FIRST_INSTALL_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_FCM_TOKEN;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_IS_ACTIVE;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_IS_FIRST_INSTALL;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_USER_ID;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readFromPreferences;

public class LoginActivity extends GodFatherActivity {

    public int userId = 0;
    private EditText passwordEdit;
    private IntlPhoneInput phoneEdit;
    private TextView resultTxt;
    private boolean isVerified;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.login));
        Objects.requireNonNull(getSupportActionBar()).setElevation(0);
        initData();

        if (isAuthenticated && isVerified) {
            startActivity(new Intent(this, BaseActivity.class));
            finish();
        } else {
            LayoutInflater inflater = (LayoutInflater) this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") View contentView = inflater != null ? inflater.inflate(R.layout.activity_login, null, false) : null;
            contentLayout.addView(contentView, 0);
            initLayout();
        }
    }

    private void initLayout() {
        resultTxt = findViewById(R.id.txt_result);
        phoneEdit = findViewById(R.id.edit_phone);
        phoneEdit.setEmptyDefault("sy");
        passwordEdit = findViewById(R.id.edit_password);
    }

    private void initData() {
        if (isAuthenticated) {
            userId = Integer.parseInt(readFromPreferences(this, DEFAULT_FILE_NAME, PREF_USER_ID, "0"));
            isVerified = Boolean.parseBoolean(readFromPreferences(this, DEFAULT_FILE_NAME, PREF_IS_ACTIVE, "False"));
        }
    }

    public void LoginBtnAction(View v) {
        hideKeyboard(this);
        resultTxt.setText("");

        String phone = phoneEdit.getText();
        String password = passwordEdit.getText().toString().trim();

        if (phone == null || phone.equals("")) {
            phoneEdit.setError(getString(R.string.field_required));
            phoneEdit.requestFocus();
            return;
        }

        if (password.equals("")) {
            passwordEdit.setError(getString(R.string.field_required));
            passwordEdit.requestFocus();
            return;
        }

        if(phone.length() == 10 && phone.startsWith("0")){
            phone = phone.substring(1);
        }

        String FCM_Token = readFromPreferences(this, DEFAULT_FILE_NAME, PREF_FCM_TOKEN, "");
        LoginBody loginBody = new LoginBody(phone, password, FCM_Token);

        String json = gson.toJson(loginBody);
        new SendGetJsonApi(this, LOGIN, json,
                new CallBackListener() {
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<Customer> userInfoAPIResponse = getResponse(result, Customer.class);
                            if (userInfoAPIResponse != null && userInfoAPIResponse.getResult().equals("success"))
                            {
                                Customer customer = userInfoAPIResponse.getContent();
                                saveUserData(customer);
                                if(customer.getUser().isPhoneNumberConfirmed()){
                                    boolean isFirstInstall = Boolean.parseBoolean(readFromPreferences(LoginActivity.this, FIRST_INSTALL_FILE_NAME, PREF_IS_FIRST_INSTALL, "True"));

                                    if(isFirstInstall && (customer.getInstalledFrom() == null || customer.getInstalledFrom().equals(""))){
                                        startActivity(new Intent(getBaseContext() , InstalledFromActivity.class));
                                    }
                                    else {
                                        startActivity(new Intent(getBaseContext() , BaseActivity.class));
                                    }
                                }
                                else {
                                    startActivity(new Intent(getBaseContext() , VerifyActivity.class));
                                }
                                finish();
                            }
                            else
                            {
                                String error_des = null;
                                if (userInfoAPIResponse != null) {
                                    error_des = userInfoAPIResponse.getResult();
                                }
                                if((error_des != null && !error_des.equals(""))){
                                    resultTxt.setText(error_des);
                                }else
                                {
                                    resultTxt.setText(getString(R.string.went_wrong));
                                }
                            }
                        }
                        catch ( Exception e)
                        {
                            resultTxt.setText(getString(R.string.went_wrong));
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }, true).Execute();
    }




    public void ResetBtnAction(View view) {
        Intent forgotIntent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
        startActivity(forgotIntent);
    }

    public void RegisterBtnAction(View view) {
        Intent RegisterIntent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(RegisterIntent);
        finish();
    }
}
