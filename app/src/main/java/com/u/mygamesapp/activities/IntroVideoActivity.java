package com.u.mygamesapp.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.authentication.LoginActivity;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.models.Content;
import com.u.mygamesapp.requestmodels.PaginationRequest;
import com.u.mygamesapp.responsemodels.APIResponse;

import static com.u.mygamesapp.api.APIConstants.API_IMAGES_URL;
import static com.u.mygamesapp.api.APIConstants.INTRO_VIDEO;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_INTRO_VIDEO_URL;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_IS_FIRST_INSTALL;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.saveIntegerToPreferences;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.saveToPreferences;

public class IntroVideoActivity extends GodFatherActivity {

    VideoView videoView;
    MediaController controller;
    Uri uri;
    int pos;
    String videoUrl;
    ProgressBar progDailog;

    @Override
    @SuppressLint("NewApi")
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_intro_video);

        videoView = findViewById(R.id.fullscreen_content);
        controller = new MediaController(this);
        controller.setPadding(0, 0, 0, 120);
        controller.setBackgroundColor(getColor(R.color.background));
        controller.setAnchorView(videoView);

        Intent intent = getIntent();
        videoUrl = intent.getStringExtra("videoUrl");
        if(videoUrl == null || videoUrl.equals("")){
            PaginationRequest request = new PaginationRequest(1, 200);
            String json = gson.toJson(request);
            new SendGetJsonApi(this, INTRO_VIDEO, json,
                    new CallBackListener() {
                        @Override
                        public void onFinish(String results) {
                            try {
                                APIResponse<Content> contentAPIResponse = getResponse(results, Content.class);
                                if (contentAPIResponse != null && contentAPIResponse.getResult().equals("success")) {
                                    Content video = contentAPIResponse.getContent();
                                    if(!video.getImageUrl().startsWith("http"))
                                        videoUrl = API_IMAGES_URL + video.getImageUrl();
                                    else
                                        videoUrl = video.getImageUrl();
                                    saveToPreferences(IntroVideoActivity.this, DEFAULT_FILE_NAME, PREF_INTRO_VIDEO_URL, videoUrl);
                                    uri = Uri.parse(videoUrl);
                                    videoView.setMediaController(controller);
                                    videoView.setVideoURI(uri);
                                    videoView.requestFocus();
                                    videoView.start();
                                    progDailog = findViewById(R.id.progress);
                                    videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                        public void onPrepared(MediaPlayer mp) {
                                            progDailog.setVisibility(View.GONE);
                                        }
                                    });

                                    if (savedInstanceState != null) {
                                        int i1 = savedInstanceState.getInt("pos");
                                        videoView.seekTo(i1);
                                    }
                                }
                                else {
                                    startActivity(new Intent(IntroVideoActivity.this, LoginActivity.class));
                                }
                            }
                            catch (Exception e) {
                                startActivity(new Intent(IntroVideoActivity.this, LoginActivity.class));
                            }
                        }
                        @Override
                        public void onProgress(int process) {
                        }
                    }
                    ,true).Execute();
        }
        else {
            uri = Uri.parse(videoUrl);
            videoView.setMediaController(controller);
            videoView.setVideoURI(uri);
            videoView.requestFocus();
            videoView.start();
            progDailog = findViewById(R.id.progress);
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                public void onPrepared(MediaPlayer mp) {
                    progDailog.setVisibility(View.GONE);
                }
            });

            if (savedInstanceState != null) {
                int i1 = savedInstanceState.getInt("pos");
                videoView.seekTo(i1);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        videoView.pause();
        pos = videoView.getCurrentPosition();
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoView.seekTo(pos);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("pos", videoView.getCurrentPosition());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void SkipAction(View view) {
        launchMain();
    }

    private void launchMain() {
        saveIntegerToPreferences(this, DEFAULT_FILE_NAME, PREF_IS_FIRST_INSTALL, 0);
        startActivity(new Intent(IntroVideoActivity.this, LoginActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
