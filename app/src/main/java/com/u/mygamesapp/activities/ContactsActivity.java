package com.u.mygamesapp.activities;


import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.u.mygamesapp.R;
import com.u.mygamesapp.adapters.FollowItemsAdapter;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.models.Contact;
import com.u.mygamesapp.requestmodels.PaginationRequest;
import com.u.mygamesapp.responsemodels.PagedAPIResponse;

import static com.u.mygamesapp.api.APIConstants.CONTACT;
import static com.u.mygamesapp.api.APIConstants.GET_ALL;
import static com.u.mygamesapp.helpers.JsonParser.getPagedResponse;

public class ContactsActivity extends GodFatherActivity implements FollowItemsAdapter.OnItemClickListener {

    private RecyclerView recyclerView;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater != null ? inflater.inflate(R.layout.activity_follow, null, false) : null;
        contentLayout.addView(contentView, 0);
        setTitle(getString(R.string.contact_us));

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        getData();
    }

    private void getData() {
        PaginationRequest paginationRequest = new PaginationRequest(1, 100);
        String json = gson.toJson(paginationRequest);
        new SendGetJsonApi(this, CONTACT + GET_ALL, json,
                new CallBackListener() {
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            PagedAPIResponse<Contact[]> pagedAPIResponse = getPagedResponse(result, Contact[].class);
                            if (pagedAPIResponse != null && pagedAPIResponse.getMessage().equals("success"))
                            {
                                Contact[] contacts = pagedAPIResponse.getContent().getContent();
                                if(contacts != null && contacts.length != 0){
                                    FollowItemsAdapter followItemsAdapter = new FollowItemsAdapter(ContactsActivity.this, contacts);
                                    recyclerView.setAdapter(followItemsAdapter);
                                    followItemsAdapter.setOnItemClickListener(ContactsActivity.this);
                                }
                            }
                            else {
                                String error_des = pagedAPIResponse != null ? pagedAPIResponse.getMessage() : null;
                                if((error_des != null && !error_des.equals(""))){
                                    Toast.makeText(ContactsActivity.this, error_des, Toast.LENGTH_LONG).show();
                                }else
                                {
                                    Toast.makeText(ContactsActivity.this, getString(R.string.went_wrong), Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                        catch (Exception e){
                            Toast.makeText(ContactsActivity.this, getString(R.string.went_wrong), Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }).Execute();
    }

    @Override
    public void onItemClick(int position) {

    }
}
