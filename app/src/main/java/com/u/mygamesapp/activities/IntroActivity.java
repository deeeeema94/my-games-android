package com.u.mygamesapp.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.u.mygamesapp.R;

import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_INTRO_VIDEO_URL;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_LOCAL_INTRO;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readFromPreferences;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readIntegerFromPreferences;

public class IntroActivity extends GodFatherActivity {

    private LinearLayout Layout_bars;
    private int[] screens, screensColors, screensTxt1, screensTxt2, screensImgIds;
    public static String[] screensTitles, screensBriefs, screensImages;
    private ViewPager vp;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        screens = new int[]{
                R.layout.intro_screen1,
                R.layout.intro_screen2,
                R.layout.intro_screen3
        };
        screensTxt1 = new int[]{
                R.id.title_txt1,
                R.id.title_txt2,
                R.id.title_txt3
        };
        screensTxt2 = new int[]{
                R.id.brief_txt1,
                R.id.brief_txt2,
                R.id.brief_txt3
        };
        screensImgIds = new int[]{
                R.id.img1,
                R.id.img2,
                R.id.img3
        };
        screensColors = new int[]{
                R.color.colorPrimary,
                R.color.colorAccent,
                R.color.colorAccent
        };

        vp = findViewById(R.id.view_pager);
        Layout_bars = findViewById(R.id.layoutBars);

        MyViewPagerAdapter myvpAdapter = new MyViewPagerAdapter();
        vp.setAdapter(myvpAdapter);
        vp.addOnPageChangeListener(viewPagerPageChangeListener);
        ColoredBars(0);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void ColoredBars(int thisScreen) {
        Drawable colorsInactive = getResources().getDrawable(R.drawable.slider_dot_gray);
        Drawable colorsActive = getResources().getDrawable(R.drawable.slider_dot_black);
        ImageView[] bottomBars = new ImageView[screens.length];

        Layout_bars.removeAllViews();
        for (int i = 0; i < bottomBars.length; i++) {
            bottomBars[i] = new ImageView(this);
            Layout_bars.addView(bottomBars[i]);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(25, 25);
            lp.setMargins(20, 0, 20, 0);
            bottomBars[i].setLayoutParams(lp);
            bottomBars[i].setImageDrawable(colorsInactive);
        }
        if (bottomBars.length > 0) {
            bottomBars[thisScreen].setImageDrawable(colorsActive);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(screensColors[thisScreen]));
                window.setNavigationBarColor(getResources().getColor(screensColors[thisScreen]));
            }
        }
    }

    private int getItem(int i) {
        return vp.getCurrentItem() + i;
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onPageSelected(int position) {
            ColoredBars(position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    public void SkipAction(View view) {
        launchMain();
    }

    private void launchMain() {
        String videoUrl= readFromPreferences(IntroActivity.this, DEFAULT_FILE_NAME, PREF_INTRO_VIDEO_URL, "");

        Intent introIntent = new Intent(IntroActivity.this, IntroVideoActivity.class);
        introIntent.putExtra("videoUrl", videoUrl);
        startActivity(introIntent);
        finish();
//        saveIntegerToPreferences(this, DEFAULT_FILE_NAME, PREF_IS_FIRST_INSTALL, 0);
//
//        startActivity(new Intent(IntroActivity.this, LoginActivity.class));
//        finish();
    }

    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater inflater;

        private MyViewPagerAdapter() {}

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(screens[position], container, false);
            TextView titleTxt = view.findViewById(screensTxt1[position]);
            titleTxt.setText(screensTitles[position]);
            TextView briefTxt = view.findViewById(screensTxt2[position]);
            briefTxt.setText(screensBriefs[position]);
            ImageView img = view.findViewById(screensImgIds[position]);

            int localIntro = readIntegerFromPreferences(IntroActivity.this, DEFAULT_FILE_NAME, PREF_LOCAL_INTRO, 1);
            if(localIntro == 1){
                Glide.with(img.getContext()).load(getImage(screensImages[position])).apply(
                        new RequestOptions()
                                .placeholder(R.drawable.place_holder)
                                .error(R.drawable.place_holder)
                ).into(img);
            }
            else {
                Glide.with(img.getContext()).load(screensImages[position]).apply(
                        new RequestOptions()
                                .placeholder(R.drawable.place_holder)
                                .error(R.drawable.place_holder)
                ).into(img);
            }

            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return screens.length;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            View v = (View) object;
            container.removeView(v);
        }

        @Override
        public boolean isViewFromObject(@NonNull View v, @NonNull Object object) {
            return v == object;
        }
    }

    private int getImage(String imageName) {
        return getResources().getIdentifier(imageName, "drawable", getPackageName());
    }
}
