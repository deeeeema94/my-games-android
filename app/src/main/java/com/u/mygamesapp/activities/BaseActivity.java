package com.u.mygamesapp.activities;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ramijemli.percentagechartview.PercentageChartView;
import com.u.mygamesapp.R;
import com.u.mygamesapp.fragments.GroupInfoFragment;
import com.u.mygamesapp.fragments.HomeFragment;
import com.u.mygamesapp.fragments.MiniGamesFragment;
import com.u.mygamesapp.fragments.MoreFragment;
import com.u.mygamesapp.fragments.MyActivityFragment;
import com.u.mygamesapp.fragments.NoInternetFragment;
import com.u.mygamesapp.fragments.PrizesFragment;

import java.util.List;
import java.util.Locale;
import java.util.Objects;

import pub.devrel.easypermissions.EasyPermissions;

import static com.u.mygamesapp.activities.GodFatherActivity.currLang;
import static com.u.mygamesapp.helpers.Constants.ARABIC_LANG;
import static com.u.mygamesapp.helpers.Constants.ENGLISH_LANG;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_CURRENT_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_COLOR;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_IS_AUTHENTICATED;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_LANG;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_NEXT_GROUP_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_SHOW_MESSAGES_SNACK_BAR;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_TOTAL_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readFromPreferences;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.saveToPreferences;

public class BaseActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks{

    public static TextView groupName;
    public static TextView pointsCount;
    public static PercentageChartView mChart;
    public static RelativeLayout pointsLayout;

    @Override
    protected void attachBaseContext(Context context) {
        String lang = readFromPreferences(context, DEFAULT_FILE_NAME, PREF_LANG, "");
        String sys_lang = ARABIC_LANG;
        if ((lang != null && lang.equals(ARABIC_LANG)) || (lang != null && lang.equals(ENGLISH_LANG)))
            currLang = lang;
        else currLang = sys_lang;
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(currLang);
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocale(locale);
            context = context.createConfigurationContext(config);
        } else {
            config.locale = locale;
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        }
        saveToPreferences(context, DEFAULT_FILE_NAME, PREF_LANG, currLang);
        super.attachBaseContext(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        saveToPreferences(this, DEFAULT_FILE_NAME, PREF_SHOW_MESSAGES_SNACK_BAR, "True");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
            Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(true);
        }

        GodFatherActivity.isAuthenticated = Boolean.parseBoolean(readFromPreferences(this, DEFAULT_FILE_NAME, PREF_IS_AUTHENTICATED, "False"));
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if (netInfo == null) {
            loadFragment(new NoInternetFragment());
        } else {
            loadFragment(new HomeFragment());
        }
        initLayout();

        MobileAds.initialize(this);
        AdView adView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .setRequestAgent("android_studio:ad_template").build();
        adView.loadAd(adRequest);
    }

    private void initLayout() {
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navView.setItemIconTintList(null);
        groupName = findViewById(R.id.group_name);
        pointsCount = findViewById(R.id.points);
        mChart = findViewById(R.id.view_id);
        pointsLayout = findViewById(R.id.points_layout);
        pointsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFragment(new GroupInfoFragment());
            }
        });

        String group = readFromPreferences(this, DEFAULT_FILE_NAME, PREF_GROUP_NAME, "");
        String points = readFromPreferences(this, DEFAULT_FILE_NAME, PREF_CURRENT_POINTS, "");
        String totalPoints = readFromPreferences(this, DEFAULT_FILE_NAME, PREF_TOTAL_POINTS, "");
        String color = readFromPreferences(this, DEFAULT_FILE_NAME, PREF_GROUP_COLOR, "");
        int nextGroupPoints = Integer.parseInt(readFromPreferences(this, DEFAULT_FILE_NAME, PREF_NEXT_GROUP_POINTS, "0"));
        setUserGroupInfo(group, points, totalPoints, color, nextGroupPoints);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment = null;

            ConnectivityManager conMgr = (ConnectivityManager) BaseActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
            if (netInfo == null) {
                fragment = new NoInternetFragment();
            } else {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        fragment = new HomeFragment();
                        break;
                    case R.id.navigation_prizes:
                        fragment = new PrizesFragment();
                        break;
                    case R.id.navigation_account:
                        fragment = new MyActivityFragment();
                        break;
                    case R.id.navigation_wheel:
                        fragment = new MiniGamesFragment();
                        break;
                    case R.id.navigation_more:
                        fragment = new MoreFragment();
                        break;
                }
            }
            return loadFragment(fragment);
        }
    };

    public boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .addToBackStack(null)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        FragmentManager.BackStackEntry backEntry = getSupportFragmentManager().getBackStackEntryAt(count - 1);
        String tag = backEntry.getName();
        if (count <= 1) {
            finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == android.R.id.home)
        {
            onBackPressed();
            return true;
        }
        else
        {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    public static void setUserGroupInfo(String group, String points, String totalPoints, String color, int nextGroupPoints){
        float percentage;
        if(nextGroupPoints != 0){
            percentage = (float) (100*Integer.parseInt(totalPoints))/nextGroupPoints;
        }
        else {
            percentage = 100;
        }
        if(percentage > 100)
            percentage = 100;
        mChart.setProgress(percentage, true);
        mChart.setProgressColor(Color.parseColor(color));
        groupName.setText(group);
        groupName.setTextColor(Color.parseColor(color));
        pointsCount.setText(points);
    }
}
