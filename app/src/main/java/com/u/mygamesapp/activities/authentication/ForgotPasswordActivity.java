package com.u.mygamesapp.activities.authentication;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.u.intlphoneinput.IntlPhoneInput;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.GodFatherActivity;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.requestmodels.ForgotBody;
import com.u.mygamesapp.responsemodels.APIResponse;

import java.util.Objects;

import static com.u.mygamesapp.api.APIConstants.FORGET_PASSWORD;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;

public class ForgotPasswordActivity extends GodFatherActivity {

    private IntlPhoneInput phoneEdit;
    private TextView resultTxt;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.forget_password));
        Objects.requireNonNull(getSupportActionBar()).setElevation(0);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View contentView = inflater != null ? inflater.inflate(R.layout.activity_forgot_password, null, false) : null;
        contentLayout.addView(contentView, 0);
        initLayout();
    }

    private void initLayout() {
        resultTxt = findViewById(R.id.txt_result);
        phoneEdit = findViewById(R.id.edit_phone);
        phoneEdit.setEmptyDefault("sy");
    }

    public void ForgotBtnAction(View v) {
        hideKeyboard(this);
        resultTxt.setText("");

        String phone = phoneEdit.getText().toString().trim();

        if (phone.equals("")) {
            phoneEdit.setError(getString(R.string.field_required));
            phoneEdit.requestFocus();
            return;
        }

        ForgotBody forgotBody = new ForgotBody(phone);

        String json = gson.toJson(forgotBody);
        final String finalPhone = phone;
        new SendGetJsonApi(this, FORGET_PASSWORD, json,
                new CallBackListener() {
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<String> userInfoAPIResponse = getResponse(result, String.class);
                            if (userInfoAPIResponse != null && userInfoAPIResponse.getResult().equals("success"))
                            {
                                String msg = userInfoAPIResponse.getContent();
                                Intent resetIntent = new Intent(ForgotPasswordActivity.this, ResetPasswordActivity.class);
                                resetIntent.putExtra("msg", msg);
                                resetIntent.putExtra("phone", finalPhone);
                                startActivity(resetIntent);
                                finish();
                            }
                            else
                            {
                                String error_des = null;
                                if (userInfoAPIResponse != null) {
                                    error_des = userInfoAPIResponse.getResult();
                                }
                                if((error_des != null && !error_des.equals(""))){
                                    resultTxt.setText(error_des);
                                }else
                                {
                                    resultTxt.setText(getString(R.string.went_wrong));
                                }
                            }
                        }
                        catch ( Exception e)
                        {
                            resultTxt.setText(getString(R.string.went_wrong));
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }, true).Execute();
    }

    public void LoginBtnAction(View view) {
        Intent LoginIntent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
        startActivity(LoginIntent);
        finish();
    }
}
