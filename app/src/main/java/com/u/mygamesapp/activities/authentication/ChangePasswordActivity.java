package com.u.mygamesapp.activities.authentication;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;
import com.u.mygamesapp.activities.GodFatherActivity;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.models.ApplicationUser;
import com.u.mygamesapp.requestmodels.ChangePasswordBody;
import com.u.mygamesapp.responsemodels.APIResponse;

import java.util.Objects;

import static com.u.mygamesapp.api.APIConstants.CHANGE_PASSWORD;
import static com.u.mygamesapp.helpers.Constants.showGreenToast;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;

public class ChangePasswordActivity extends GodFatherActivity {

    private EditText oldPasswordEdit, newPasswordEdit, passwordConfirmEdit;
    private TextView resultTxt;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_change_pass, null, false);
        contentLayout.addView(contentView, 0);
        Objects.requireNonNull(getSupportActionBar()).setElevation(0);
        setTitle(getString(R.string.change_password));
        initLayout();
    }

    private void initLayout() {
        resultTxt = findViewById(R.id.txt_result);
        oldPasswordEdit =  findViewById(R.id.edit_old_pass);
        newPasswordEdit =  findViewById(R.id.edit_new_password);
        passwordConfirmEdit =  findViewById(R.id.edit_password_confirm);
    }

    public void ChangePasswordAction(View v)
    {
        hideKeyboard(this);
        String newPassword = newPasswordEdit.getText().toString().trim();
        String confirmPassword = passwordConfirmEdit.getText().toString().trim();
        String oldPassword = oldPasswordEdit.getText().toString().trim();

        if (oldPassword.equals("")) {
            oldPasswordEdit.setError(getString(R.string.field_required));oldPasswordEdit.requestFocus();  return;
        }
        if (newPassword.equals("")) {
            newPasswordEdit.setError(getString(R.string.field_required));newPasswordEdit.requestFocus();  return;
        }
        if (confirmPassword.equals("")) {
            passwordConfirmEdit.setError(getString(R.string.field_required));passwordConfirmEdit.requestFocus();  return;
        }
        if (!newPassword.equals(confirmPassword)) {
            passwordConfirmEdit.setError(getString(R.string.password_confirm_not_equal_password));newPasswordEdit.requestFocus();  return;
        }


        ChangePasswordBody inModel = new ChangePasswordBody(oldPassword, newPassword, newPassword);

        String json = gson.toJson(inModel);
        new SendGetJsonApi(this, CHANGE_PASSWORD, json,
                new CallBackListener() {
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<ApplicationUser> userInfoAPIResponse = getResponse(result, ApplicationUser.class);
                            if (userInfoAPIResponse != null && userInfoAPIResponse.getResult().equals("success"))
                            {
                                String msg = getString(R.string.password_changed);
                                showGreenToast(getApplicationContext(), msg);
                                startActivity(new Intent(getBaseContext() , BaseActivity.class));
                                finish();
                            }
                            else
                            {
                                String error_des = userInfoAPIResponse != null ? userInfoAPIResponse.getResult() : null;
                                if((error_des != null && !error_des.equals(""))){
                                    resultTxt.setText(error_des);
                                }else
                                {
                                    resultTxt.setText(getString(R.string.went_wrong));
                                }
                            }
                        }
                        catch ( Exception e)
                        {
                            resultTxt.setText(getString(R.string.went_wrong));
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ,true).Execute();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
