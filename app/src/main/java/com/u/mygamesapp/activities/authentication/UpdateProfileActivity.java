package com.u.mygamesapp.activities.authentication;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.u.mygamesapp.MyGamesApp;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.GodFatherActivity;
import com.u.mygamesapp.api.ApiInterface;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.helpers.CustomDialog;
import com.u.mygamesapp.helpers.JsonParser;
import com.u.mygamesapp.injection.components.MyAppComponent;
import com.u.mygamesapp.models.City;
import com.u.mygamesapp.models.Customer;
import com.u.mygamesapp.requestmodels.PaginationRequest;
import com.u.mygamesapp.requestmodels.UpdateProfileBody;
import com.u.mygamesapp.responsemodels.APIResponse;
import com.u.mygamesapp.responsemodels.PagedAPIResponse;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.u.mygamesapp.api.APIConstants.API_IMAGES_URL;
import static com.u.mygamesapp.api.APIConstants.CITIES;
import static com.u.mygamesapp.api.APIConstants.GET_ALL;
import static com.u.mygamesapp.api.APIConstants.GET_PROFILE;
import static com.u.mygamesapp.api.APIConstants.UPDATE_PROFILE;
import static com.u.mygamesapp.api.APIConstants.UPLOAD_IMAGE;
import static com.u.mygamesapp.helpers.Constants.DIALOG_TYPE_SPINNER;
import static com.u.mygamesapp.helpers.Constants.showGreenToast;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;

public class UpdateProfileActivity extends GodFatherActivity implements AdapterView.OnItemSelectedListener,
        SwipeRefreshLayout.OnRefreshListener{

    private EditText usernameEdit, firstNameEdit, lastNameEdit;
    private ImageView userImg;
    private TextView resultTxt;
    private Spinner citiesSpinner;
    private City[] citiesArray;
    private int cityId = 0;
    private static ArrayAdapter<String> citiesAdapter;
    private RadioGroup genderRadioGroup;
    private RadioButton male, female;
    private Customer customer;
    private SwipeRefreshLayout swipeRefreshLayout;
    private final int REQUEST_CODE_FOR_SELECT_IMAGE = 102;
    private Uri mUri;
    private String mUriPath;

    private String filename;
    private String uploadedDocument = "";

    @Inject
    ApiInterface apiInterface;

    private MyAppComponent myAppComponent;


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.edit_profile));
        Objects.requireNonNull(getSupportActionBar()).setElevation(0);

        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View contentView = inflater != null ? inflater.inflate(R.layout.activity_update_profile, null, false) : null;
        contentLayout.addView(contentView, 0);

        initLayout();

        myAppComponent = MyGamesApp.myAppComponent;
        myAppComponent.injectUploadImageActivity(this);

        getCities();
    }

    private void initLayout() {
        resultTxt = findViewById(R.id.txt_result);
        userImg = findViewById(R.id.user_img);
        userImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");
                intent.addCategory(Intent.CATEGORY_OPENABLE);

                try {
                    startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), REQUEST_CODE_FOR_SELECT_IMAGE);
                } catch (Exception ex) {
                    System.out.println("browseClick :" + ex);//android.content.ActivityNotFoundException ex
                }
            }
        });

        usernameEdit = findViewById(R.id.edit_phone);
        firstNameEdit = findViewById(R.id.edit_first_name);
        lastNameEdit = findViewById(R.id.edit_last_name);
        genderRadioGroup = findViewById(R.id.radio_gender);
        male = findViewById(R.id.male_btn);
        female = findViewById(R.id.female_btn);
        citiesSpinner = findViewById(R.id.cities);
        List<String> names = new ArrayList<>();
        names.add(getString(R.string.loading));
        citiesAdapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_item,names);
        citiesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        citiesSpinner.setAdapter(citiesAdapter);

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        UpdateProfileActivity.this.getCities();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
        );
    }

    @Override
    public void onRefresh() {
        if (swipeRefreshLayout.isRefreshing()) {
            getCities();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void getCities(){
        PaginationRequest paginationRequest = new PaginationRequest(1, 100);
        String json = gson.toJson(paginationRequest);
        new SendGetJsonApi(this, CITIES + GET_ALL, json,
                new CallBackListener() {
                    @Override
                    public void onFinish(String result)
                    {
                        PagedAPIResponse<City[]> apiResponse = JsonParser.getPagedResponse(result, City[].class);
                        try {
                            if (apiResponse != null && apiResponse.getMessage().equals("success")) {
                                citiesArray = apiResponse.getContent().getContent();
                                List<String> names = new ArrayList<>();
                                names.add("Select City");

                                for (City city : citiesArray) {
                                    names.add(city.getDisplayName());
                                }
                                citiesSpinner.setOnItemSelectedListener(UpdateProfileActivity.this);
                                citiesAdapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_item,names);
                                citiesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                citiesSpinner.setAdapter(citiesAdapter);
                                citiesSpinner.setSelection(names.size() - 1);
                                citiesSpinner.setPadding(10, 0, 0, 10);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                    citiesSpinner.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                                }
                                getProfile();
                            }
                            else
                            {
                                String error_des = apiResponse != null ? apiResponse.getMessage() : null;
                                if ((error_des != null && !error_des.equals(""))) {
                                    resultTxt.setText(error_des);
                                } else {
                                    resultTxt.setText(getString(R.string.went_wrong));
                                }
                            }
                        }
                        catch ( Exception e)
                        {
                            resultTxt.setText(getString(R.string.went_wrong));
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        , true).Execute();
    }

    private void getProfile(){
        PaginationRequest paginationRequest = new PaginationRequest(1, 100);
        String json = gson.toJson(paginationRequest);
        new SendGetJsonApi(this, GET_PROFILE, json,
                new CallBackListener() {
                    @Override
                    public void onFinish(String result)
                    {
                        APIResponse<Customer> apiResponse = JsonParser.getResponse(result, Customer.class);
                        try {
                            if (apiResponse != null && apiResponse.getResult().equals("success")) {
                                customer = apiResponse.getContent();
                                BindDataToUI();
                            }
                            else
                            {
                                String error_des = apiResponse != null ? apiResponse.getResult() : null;
                                if ((error_des != null && !error_des.equals(""))) {
                                    resultTxt.setText(error_des);
                                } else {
                                    resultTxt.setText(getString(R.string.went_wrong));
                                }
                            }
                        }
                        catch ( Exception e)
                        {
                            resultTxt.setText(getString(R.string.went_wrong));
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
                , true).Execute();
    }

    private void BindDataToUI() {
        firstNameEdit.setText(customer.getFirstName());
        lastNameEdit.setText(customer.getLastName());
        usernameEdit.setText(customer.getUser().getUserName());
        if(customer.isGender()){
            male.setChecked(false);
            female.setChecked(true);
        }
        else {
            male.setChecked(true);
            female.setChecked(false);
        }
        for (int i = 0; i < citiesArray.length; i++){
            if(citiesArray[i].getId() == customer.getCityId()){
                citiesSpinner.setSelection(i + 1);
                break;
            }
        }

        Glide.with(getApplicationContext())
                .load(API_IMAGES_URL + customer.getImageUrl())
                .circleCrop()
                .placeholder(R.drawable.icon)
                .into(userImg);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Spinner spin = (Spinner) adapterView;
        if (spin.getId() == R.id.cities) {
            cityId = citiesArray[i-1].getId();
        }
        resultTxt.setText("");
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    public void UpdateBtnAction(View view) {
        hideKeyboard(this);
        resultTxt.setText("");

        String firstName = firstNameEdit.getText().toString().trim();
        String lastName = lastNameEdit.getText().toString().trim();

        if (firstName.equals("")) {
            firstNameEdit.setError(getString(R.string.field_required));firstNameEdit.requestFocus(); return;
        }
        if (lastName.equals("")) {
            lastNameEdit.setError(getString(R.string.field_required));lastNameEdit.requestFocus(); return;
        }

        if(cityId == 0){
            resultTxt.setText(R.string.city_required);return;
        }
        else {
            resultTxt.setText("");
        }

        final int gender_id = genderRadioGroup.getCheckedRadioButtonId();
        boolean gender = false;
        RadioButton genderRadioBtn = findViewById(gender_id);
        if(genderRadioBtn != null) {
            gender = genderRadioBtn.getText().equals(getString(R.string.female));
        }
        else {
            resultTxt.setText(R.string.gender_required);return;
        }

        UpdateProfileBody registerBody;
        if(!uploadedDocument.equals("")){
            registerBody = new UpdateProfileBody(firstName, lastName, gender, cityId, uploadedDocument);
        }
        else {
            registerBody = new UpdateProfileBody(firstName, lastName, gender, cityId);
        }

        String json = gson.toJson(registerBody);
        new SendGetJsonApi(this, UPDATE_PROFILE, json,
                new CallBackListener() {
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<Customer> userInfoAPIResponse = getResponse(result, Customer.class);
                            if (userInfoAPIResponse != null && userInfoAPIResponse.getResult().equals("success"))
                            {
                                Customer customer = userInfoAPIResponse.getContent();
                                saveUserData(customer);
                                String msg = getString(R.string.updated_successfully);
                                showGreenToast(getApplicationContext(), msg);
                            }
                            else
                            {
                                String error_des = null;
                                if (userInfoAPIResponse != null) {
                                    error_des = userInfoAPIResponse.getResult();
                                }
                                if((error_des != null && !error_des.equals(""))){
                                    resultTxt.setText(error_des);
                                }else
                                {
                                    resultTxt.setText(getString(R.string.went_wrong));
                                }
                            }
                        }
                        catch ( Exception e)
                        {
                            resultTxt.setText(getString(R.string.went_wrong));
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }, true).Execute();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_FOR_SELECT_IMAGE) {
            if (resultCode == RESULT_OK) {
                try {
                    Uri uri = data.getData();
                    mUri = uri;
                    Log.d("opeth", "uri path : " + uri.getPath());

                    String mimeType = getContentResolver().getType(uri);
                    Log.d("opeth", "mimeType : " + mimeType.toString());
                    if (mimeType == null) {
                        String path = getPath(this, uri);
                        File file = new File(path);
                        filename = file.getName();
                        Log.d("opeth", "filename in mimeType null : " + filename);

                    } else {
                        Uri returnUri = data.getData();
                        Cursor returnCursor = getContentResolver().query(returnUri, null, null, null, null);
                        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                        returnCursor.moveToFirst();
                        filename = returnCursor.getString(nameIndex);
                        Log.d("opeth", "filename in mimeType not null : " + filename);

                        String size = Long.toString(returnCursor.getLong(sizeIndex));
                        Log.d("opeth", "size : " + size);
                    }

                    askForPermission(Manifest.permission.READ_EXTERNAL_STORAGE, 1);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getPath(Context context, Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
            }
        } else {
            String uriPath = getPath(this, mUri);
            uploadDocumentToServer(uriPath);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {

                case 1:
                    String uriPath = getPath(this, mUri);
                    uploadDocumentToServer(uriPath);
                    break;
            }

        } else {
            Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadDocumentToServer(String uriPath) {
        final CustomDialog cdd = new CustomDialog(UpdateProfileActivity.this, DIALOG_TYPE_SPINNER);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(cdd.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        cdd.setCancelable(false);
        cdd.show();

        mUriPath = uriPath;
        File file = new File(uriPath);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("files", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));

        Call<APIResponse<Customer>> call = apiInterface.uploadImage(API_IMAGES_URL + UPLOAD_IMAGE, filePart);

        call.enqueue(new Callback<APIResponse<Customer>>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<APIResponse<Customer>> call, Response<APIResponse<Customer>> response) {
                cdd.dismiss();
                APIResponse<Customer> res = response.body();
                if (response.code() == 200) {
                    Customer customer = res.getContent();
                    try {
                        uploadedDocument = Objects.requireNonNull(customer).getImageUrl();
                        if (!customer.getImageUrl().isEmpty()) {
                            Glide.with(getApplicationContext())
                                    .load(API_IMAGES_URL + uploadedDocument)
                                    .circleCrop()
                                    .placeholder(R.drawable.icon)
                                    .into(userImg);
                        }
                    } catch (Exception e){
                        uploadedDocument = "";
                        resultTxt.setText(getString(R.string.went_wrong));
                    }
                }
            }

            @Override
            public void onFailure(Call<APIResponse<Customer>> call, Throwable t) {
                cdd.dismiss();
                uploadedDocument = "";
                resultTxt.setText(getString(R.string.went_wrong));
            }
        });
    }
}
