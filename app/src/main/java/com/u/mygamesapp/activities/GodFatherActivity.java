package com.u.mygamesapp.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;
import com.u.mygamesapp.R;
import com.u.mygamesapp.models.Customer;

import java.util.Locale;
import java.util.Objects;
import java.util.UUID;

import static com.u.mygamesapp.helpers.Constants.ARABIC_LANG;
import static com.u.mygamesapp.helpers.Constants.ENGLISH_LANG;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_CURRENT_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_DAILY_BONUS_LEVEL;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_FIRST_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_COLOR;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_ID;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_IMAGE;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_INSTALLED_FROM;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_IS_ACTIVE;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_IS_AUTHENTICATED;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_LANG;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_LAST_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_NEXT_GROUP_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_SHARE_CODE;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_TOKEN;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_TOTAL_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_USERNAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_USER_ID;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readFromPreferences;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.saveToPreferences;

public class GodFatherActivity extends AppCompatActivity {

    protected RelativeLayout contentLayout;
    protected LinearLayout contact_us_layout;

    public static String currLang = ARABIC_LANG;
    public static String appVersion;
    public static String mobileBrand;
    public static String osVersion;
    public static String deviceId;
    public static Gson gson = new Gson();
    public static boolean isAuthenticated = false;

    @Override
    protected void attachBaseContext(Context context) {
        String lang =  readFromPreferences(context, DEFAULT_FILE_NAME, PREF_LANG, "");
        String sys_lang = ARABIC_LANG;
        if ((lang != null && lang.equals(ARABIC_LANG)) || (lang != null && lang.equals(ENGLISH_LANG))) currLang = lang;
        else  currLang = sys_lang;
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(currLang);
        Locale.setDefault(locale);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocale(locale);
            context = context.createConfigurationContext(config);
        } else {
            config.locale = locale;
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        }
        saveToPreferences(context, DEFAULT_FILE_NAME, PREF_LANG, currLang);
        super.attachBaseContext(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            if (currLang.equals("ar"))
            {
                getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            }
            else
            {
                getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }
        }
        setContentView(R.layout.activity_god_father);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
            Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(true);
        }

        isAuthenticated = Boolean.parseBoolean(readFromPreferences(this, DEFAULT_FILE_NAME, PREF_IS_AUTHENTICATED, "False"));

        initLayout();
        mobileBrand = Build.MANUFACTURER + " " + Build.MODEL;
        osVersion = Build.VERSION.RELEASE;
        deviceId = getUniqueDeviceID();
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            appVersion = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            appVersion = "1.0.2";
            e.printStackTrace();
        }
    }

    private void initLayout() {
        contentLayout = findViewById(R.id.content_layout);
        contact_us_layout = findViewById(R.id.contact_us_layout);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == android.R.id.home)
        {
            onBackPressed();
            return true;
        }
        else
        {
            return super.onOptionsItemSelected(item);
        }
    }


    public static void hideKeyboard(AppCompatActivity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void saveUserData(Customer customer){

        saveToPreferences(this, DEFAULT_FILE_NAME, PREF_IS_AUTHENTICATED, String.valueOf(true));

        saveToPreferences(this, DEFAULT_FILE_NAME, PREF_TOKEN, String.valueOf(customer.getToken()));
        saveToPreferences(this, DEFAULT_FILE_NAME, PREF_USER_ID, String.valueOf(customer.getId()));
        saveToPreferences(this, DEFAULT_FILE_NAME, PREF_IS_ACTIVE, String.valueOf(customer.getUser().isPhoneNumberConfirmed()));
        saveToPreferences(this, DEFAULT_FILE_NAME, PREF_USERNAME, String.valueOf(customer.getUser().getUserName()));
        saveToPreferences(this, DEFAULT_FILE_NAME, PREF_IMAGE, String.valueOf(customer.getImageUrl()));
        saveToPreferences(this, DEFAULT_FILE_NAME, PREF_FIRST_NAME, String.valueOf(customer.getFirstName()));
        saveToPreferences(this, DEFAULT_FILE_NAME, PREF_LAST_NAME, String.valueOf(customer.getLastName()));
        saveToPreferences(this, DEFAULT_FILE_NAME, PREF_TOTAL_POINTS, String.valueOf(customer.getTotalPoints()));
        saveToPreferences(this, DEFAULT_FILE_NAME, PREF_CURRENT_POINTS, String.valueOf(customer.getCurrentPoints()));
        saveToPreferences(this, DEFAULT_FILE_NAME, PREF_GROUP_ID, String.valueOf(customer.getGroupId()));
        saveToPreferences(this, DEFAULT_FILE_NAME, PREF_GROUP_NAME, String.valueOf(customer.getGroup().getDisplayName()));
        saveToPreferences(this, DEFAULT_FILE_NAME, PREF_GROUP_COLOR, String.valueOf(customer.getGroup().getColor()));
        saveToPreferences(this, DEFAULT_FILE_NAME, PREF_NEXT_GROUP_POINTS, String.valueOf(customer.getNextGroupPoints()));
        saveToPreferences(this, DEFAULT_FILE_NAME, PREF_SHARE_CODE, String.valueOf(customer.getShareCode()));
        saveToPreferences(this, DEFAULT_FILE_NAME, PREF_INSTALLED_FROM, String.valueOf(customer.getInstalledFrom()));
        saveToPreferences(this, DEFAULT_FILE_NAME, PREF_DAILY_BONUS_LEVEL, String.valueOf(customer.getDailyBonusLevel()));
    }

    private static String getUniqueDeviceID() {
        String m_szDevIDShort = "35" + (Build.BOARD.length() % 10) + (Build.BRAND.length() % 10) +
                (Build.DEVICE.length() % 10) + (Build.MANUFACTURER.length() % 10) +
                (Build.MODEL.length() % 10) + (Build.PRODUCT.length() % 10);
        String serial = null;
        try {
            serial = Build.class.getField("SERIAL").get(null).toString();
            return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
        } catch (Exception exception) {
            serial = Settings.Secure.ANDROID_ID;
        }
        return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
    }

    public void ContactUsOpenAction(View view) {
        Intent contactIntent = new Intent(GodFatherActivity.this, SendContactUsActivity.class);
        startActivity(contactIntent);
    }

    public void CloseAction(View view) {
        finish();
    }
}
