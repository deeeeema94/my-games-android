package com.u.mygamesapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.u.mygamesapp.R;
import com.u.mygamesapp.adapters.DailyPrizesAdapter;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.helpers.JsonParser;
import com.u.mygamesapp.models.CustomerPrize;
import com.u.mygamesapp.models.DailyBonus;
import com.u.mygamesapp.models.Prize;
import com.u.mygamesapp.requestmodels.PaginationRequest;
import com.u.mygamesapp.responsemodels.APIResponse;

import java.util.LinkedList;
import java.util.List;

import static com.u.mygamesapp.api.APIConstants.ADD_DAILY_BONUS;
import static com.u.mygamesapp.api.APIConstants.CUSTOMER_PRIZES;
import static com.u.mygamesapp.api.APIConstants.DAILY_BONUS;
import static com.u.mygamesapp.api.APIConstants.GET_ONE;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_DAILY_BONUS_LEVEL;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_IS_BONUS_VALID;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readFromPreferences;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.saveToPreferences;

public class DailyBonusActivity extends GodFatherActivity {

    DailyBonus dailyBonus = new DailyBonus();
    Prize[] prizes;
    private boolean isBonusValid;
    private Button spinBtn;
    private RecyclerView recycler10View, recycler20View, recycler30View;
    private ImageView calendar10, calendar20, calendar30;
    private LinearLayout daily_layout;
    private static final int SMALL_SIZE = 100, LARGE_SIZE = 200;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_bonus);

        contact_us_layout.setVisibility(View.GONE);

        initLayout();

        getPrizes();
    }

    private void initLayout() {

        recycler10View = findViewById(R.id.prizes_10_recycler_view);
        recycler20View = findViewById(R.id.prizes_20_recycler_view);
        recycler30View = findViewById(R.id.prizes_30_recycler_view);
        GridLayoutManager mLayoutManager10 = new GridLayoutManager(this, 3);
        GridLayoutManager mLayoutManager20 = new GridLayoutManager(this, 3);
        GridLayoutManager mLayoutManager30 = new GridLayoutManager(this, 3);
        recycler10View.setLayoutManager(mLayoutManager10);
        recycler20View.setLayoutManager(mLayoutManager20);
        recycler30View.setLayoutManager(mLayoutManager30);

        calendar10 = findViewById(R.id.calendar_10);
        calendar20 = findViewById(R.id.calendar_20);
        calendar30 = findViewById(R.id.calendar_30);

        spinBtn = findViewById(R.id.play);
        daily_layout = findViewById(R.id.daily_layout);

        isBonusValid = Boolean.parseBoolean(readFromPreferences(this, DEFAULT_FILE_NAME, PREF_IS_BONUS_VALID, "False"));
        TextView comeTxt = findViewById(R.id.come_txt);
        if(!isBonusValid){
            comeTxt.setVisibility(View.VISIBLE);
            spinBtn.setEnabled(false);
            daily_layout.setAlpha(0.5F);
        }
        else{
            comeTxt.setVisibility(View.GONE);
            spinBtn.setEnabled(true);
            daily_layout.setAlpha(1F);
        }
    }

    private void setPrizesToUI(){
        List<Prize> prizes10 = new LinkedList<>();
        List<Prize> prizes20 = new LinkedList<>();
        List<Prize> prizes30 = new LinkedList<>();
        if(prizes.length > 29){
            for (int i = 0; i < 10; i++){
                prizes10.add(prizes[i]);
            }
            for (int i = 10; i < 20; i++){
                prizes20.add(prizes[i]);
            }
            for (int i = 20; i < 30; i++){
                prizes30.add(prizes[i]);
            }
        }
        DailyPrizesAdapter dailyPrizes10Adapter = new DailyPrizesAdapter(this, prizes10);
        recycler10View.setAdapter(dailyPrizes10Adapter);

        DailyPrizesAdapter dailyPrizes20Adapter = new DailyPrizesAdapter(this, prizes20);
        recycler20View.setAdapter(dailyPrizes20Adapter);

        DailyPrizesAdapter dailyPrizes30Adapter = new DailyPrizesAdapter(this, prizes30);
        recycler30View.setAdapter(dailyPrizes30Adapter);
        final int level = Integer.parseInt(readFromPreferences(this, DEFAULT_FILE_NAME, PREF_DAILY_BONUS_LEVEL, "0"));
        if(level <= 10){
            recycler10View.setVisibility(View.VISIBLE);
            calendar10.requestLayout();
            calendar10.getLayoutParams().height = LARGE_SIZE;
            calendar10.getLayoutParams().width = LARGE_SIZE;
        }
        else if(level <= 20){
            recycler20View.setVisibility(View.VISIBLE);
            calendar20.requestLayout();
            calendar20.getLayoutParams().height = LARGE_SIZE;
            calendar20.getLayoutParams().width = LARGE_SIZE;
        }
        else{
            recycler30View.setVisibility(View.VISIBLE);
            calendar30.requestLayout();
            calendar30.getLayoutParams().height = LARGE_SIZE;
            calendar30.getLayoutParams().width = LARGE_SIZE;
        }

        spinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int prizeId = 0;
                for (int i = 0; i < prizes.length; i++){
                    if(prizes[i].getItemOrder() == level){
                        prizeId = prizes[i].getId();
                    }
                }
                GetPrizeBtnAction(prizeId);
            }
        });
    }

    private void getPrizes(){
        PaginationRequest paginationRequest = new PaginationRequest(1, 100);
        String json = gson.toJson(paginationRequest);
        new SendGetJsonApi(this, DAILY_BONUS + GET_ONE, json,
                new CallBackListener() {
                    @Override
                    public void onFinish(String result)
                    {
                        APIResponse<DailyBonus> apiResponse = JsonParser.getResponse(result, DailyBonus.class);
                        try {
                            if (apiResponse != null && apiResponse.getResult().equals("success")) {
                                dailyBonus = apiResponse.getContent();
                                prizes = dailyBonus.getPrizes();
                                setPrizesToUI();
                            }
                            else
                            {
                                String error_des = apiResponse != null ? apiResponse.getResult() : null;
                                if ((error_des != null && !error_des.equals(""))) {
                                    Toast.makeText(DailyBonusActivity.this, error_des, Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(DailyBonusActivity.this, getString(R.string.went_wrong), Toast.LENGTH_LONG).show();
                                }
                                setResult(Activity.RESULT_CANCELED);
                                finish();
                            }
                        }
                        catch ( Exception e)
                        {
                            Toast.makeText(DailyBonusActivity.this, getString(R.string.went_wrong), Toast.LENGTH_LONG).show();
                            setResult(Activity.RESULT_CANCELED);
                            finish();
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }, true).Execute();
    }

    public void CloseAction(View view) {
        finish();
    }

    public void GetPrizeBtnAction(int prizeId)
    {
        CustomerPrize inModel = new CustomerPrize(prizeId);

        String json = gson.toJson(inModel);

        new SendGetJsonApi(this, CUSTOMER_PRIZES + ADD_DAILY_BONUS, json,
                new CallBackListener() {
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            saveToPreferences(DailyBonusActivity.this, DEFAULT_FILE_NAME, PREF_IS_BONUS_VALID, String.valueOf(false));
                            APIResponse<CustomerPrize> customerPrizeAPIResponse = getResponse(result, CustomerPrize.class);
                            if (customerPrizeAPIResponse != null && customerPrizeAPIResponse.getResult().equals("success"))
                            {
                                spinBtn.setEnabled(false);
                                spinBtn.setOnClickListener(null);
                                Intent resultIntent = new Intent();
                                resultIntent.putExtra("prize", customerPrizeAPIResponse.getContent().getPrize());
                                setResult(Activity.RESULT_OK, resultIntent);
                                finish();
                            }
                            else
                            {
                                String error_des = customerPrizeAPIResponse != null ? customerPrizeAPIResponse.getResult() : null;
                                if((error_des != null && !error_des.equals(""))){
                                    Toast.makeText(DailyBonusActivity.this, error_des, Toast.LENGTH_LONG).show();
                                }else
                                {
                                    Toast.makeText(DailyBonusActivity.this, getString(R.string.went_wrong), Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                        catch ( Exception e)
                        {
                            Toast.makeText(DailyBonusActivity.this, getString(R.string.went_wrong), Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }, true).Execute();
    }

    public void Show10Action(View view) {
        recycler10View.setVisibility(View.VISIBLE);
        recycler20View.setVisibility(View.GONE);
        recycler30View.setVisibility(View.GONE);

        calendar10.requestLayout();
        calendar10.getLayoutParams().height = LARGE_SIZE;
        calendar10.getLayoutParams().width = LARGE_SIZE;

        calendar20.requestLayout();
        calendar20.getLayoutParams().height = SMALL_SIZE;
        calendar20.getLayoutParams().width = SMALL_SIZE;

        calendar30.requestLayout();
        calendar30.getLayoutParams().height = SMALL_SIZE;
        calendar30.getLayoutParams().width = SMALL_SIZE;

    }

    public void Show20Action(View view) {
        recycler10View.setVisibility(View.GONE);
        recycler20View.setVisibility(View.VISIBLE);
        recycler30View.setVisibility(View.GONE);

        calendar10.requestLayout();
        calendar10.getLayoutParams().height = SMALL_SIZE;
        calendar10.getLayoutParams().width = SMALL_SIZE;

        calendar20.requestLayout();
        calendar20.getLayoutParams().height = LARGE_SIZE;
        calendar20.getLayoutParams().width = LARGE_SIZE;

        calendar30.requestLayout();
        calendar30.getLayoutParams().height = SMALL_SIZE;
        calendar30.getLayoutParams().width = SMALL_SIZE;
    }

    public void Show30Action(View view) {
        recycler10View.setVisibility(View.GONE);
        recycler20View.setVisibility(View.GONE);
        recycler30View.setVisibility(View.VISIBLE);

        calendar10.requestLayout();
        calendar10.getLayoutParams().height = SMALL_SIZE;
        calendar10.getLayoutParams().width = SMALL_SIZE;

        calendar20.requestLayout();
        calendar20.getLayoutParams().height = SMALL_SIZE;
        calendar20.getLayoutParams().width = SMALL_SIZE;

        calendar30.requestLayout();
        calendar30.getLayoutParams().height = LARGE_SIZE;
        calendar30.getLayoutParams().width = LARGE_SIZE;
    }
}