package com.u.mygamesapp.activities;


import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.u.mygamesapp.R;
import com.u.mygamesapp.adapters.ExpandableFAQsAdapter;
import com.u.mygamesapp.adapters.ExpandableRecyclerAdapter;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.helpers.CustomDialog;
import com.u.mygamesapp.helpers.JsonParser;
import com.u.mygamesapp.models.ContactUs;
import com.u.mygamesapp.requestmodels.PaginationRequest;
import com.u.mygamesapp.responsemodels.PagedAPIResponse;

import java.util.ArrayList;
import java.util.List;

import static com.u.mygamesapp.api.APIConstants.CONTACT_US;
import static com.u.mygamesapp.api.APIConstants.GET_ALL;

public class FAQActivity extends GodFatherActivity {

    private RecyclerView faqRecyclerView;
    private static CustomDialog cdd;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View contentView = inflater != null ? inflater.inflate(R.layout.activity_faq, null, false) : null;
        contentLayout.addView(contentView, 0);
        setTitle(R.string.faqs);

        faqRecyclerView = findViewById(R.id.faq_recycler_view);

        getData();
    }

    private void getData() {
        PaginationRequest paginationRequest = new PaginationRequest(1, 100);
        String json = gson.toJson(paginationRequest);
        new SendGetJsonApi(this, CONTACT_US + GET_ALL, json,
                new CallBackListener() {
                    @Override
                    public void onFinish(String results)
                    {
                        try {
                            PagedAPIResponse<ContactUs[]> pagedAPIResponse = JsonParser.getPagedResponse(results, ContactUs[].class);
                            if (pagedAPIResponse != null && pagedAPIResponse.getMessage().equals("success")) {
                                ContactUs[] faqs = pagedAPIResponse.getContent().getContent();
                                if (faqs != null && faqs.length != 0) {
                                    final ArrayList<ContactUs> faqsArray = new ArrayList<>();
                                    for (ContactUs faq : faqs) {
                                        List<String> temp = new ArrayList<>();
                                        temp.add(faq.getFeaturedReply());
                                        faq.setAnswers(temp);
                                        faqsArray.add(faq);
                                    }
                                    ExpandableFAQsAdapter faQsAdapter = new ExpandableFAQsAdapter(FAQActivity.this, faqsArray);
                                    faQsAdapter.setExpandCollapseListener(new ExpandableRecyclerAdapter.ExpandCollapseListener() {
                                        @UiThread
                                        @Override
                                        public void onParentExpanded(int parentPosition) {
                                            ContactUs expandedFaqItem = faqsArray.get(parentPosition);
                                        }

                                        @UiThread
                                        @Override
                                        public void onParentCollapsed(int parentPosition) {
                                            ContactUs collapsedFaqItem = faqsArray.get(parentPosition);
                                        }
                                    });

                                    faQsAdapter.expandAllParents();

                                    faqRecyclerView.setAdapter(faQsAdapter);
                                    faqRecyclerView.setLayoutManager(new LinearLayoutManager(FAQActivity.this));
                                } else {
                                    String error_des = pagedAPIResponse != null ? pagedAPIResponse.getMessage() : null;
                                    if ((error_des != null && !error_des.equals(""))) {
                                        Toast.makeText(FAQActivity.this, error_des, Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(FAQActivity.this, getString(R.string.went_wrong), Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                                else {
                                    String error_des = pagedAPIResponse != null ? pagedAPIResponse.getMessage() : null;
                                    if((error_des != null && !error_des.equals(""))){
                                        Toast.makeText(FAQActivity.this, error_des, Toast.LENGTH_LONG).show();
                                    }else
                                    {
                                        Toast.makeText(FAQActivity.this, getString(R.string.went_wrong), Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                        catch (Exception e){
                                Toast.makeText(FAQActivity.this, getString(R.string.went_wrong), Toast.LENGTH_LONG).show();
                            }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }).Execute();
    }

//    public void LoginBtnAction(TextView emailEdit, TextView passwordEdit, TextView resultTxt)
//    {
//        hideKeyboard(this);
//        cdd.res.setText("");
//
//        String email = emailEdit.getText().toString().trim();
//        String password = passwordEdit.getText().toString().trim();
//
//        if (email.equals("")) {
//            emailEdit.setError(getString(R.string.email_required));emailEdit.requestFocus();  return;
//        }
//        if (password.equals("")) {
//            passwordEdit.setError(getString(R.string.password_required));passwordEdit.requestFocus();  return;
//        }
//
//        String FCM_Token = readFromPreferences(this, DEFAULT_FILE_NAME, PREF_FCM_TOKEN, "");
//
//        password = PasswordEncryptionHelper.encryptAndEncode(password);
//        saveToPreferences(this, USER_DATA_FILE_NAME, PREF_ENCRYPTED_PASSWORD, password);
//
//        LoginBody loginBody = new LoginBody(email, password, FCM_Token);
//        String json = gson.toJson(loginBody);
//        new SendGetJsonApi(this, LOGIN, json,
//                new CallBackListener() {
//                    @Override
//                    public void onFinish(String result)
//                    {
//                        try {
//                            APIResponse<Customer> userInfoAPIResponse = getResponse(result, Customer.class);
//                            if (userInfoAPIResponse != null && userInfoAPIResponse.getResult().equals("success"))
//                            {
//                                Customer userInfo = userInfoAPIResponse.getContent();
//                                saveUserData(userInfo);
//                                if (userInfo.getAppUser().isEmailConfirmed())
//                                {
//                                    startActivity(new Intent(getBaseContext() , SendContactUsActivity.class));
//                                    cdd.dismiss();
//                                }
//                                else
//                                {
//                                    startActivity(new Intent(getBaseContext() , VerifyActivity.class));
//                                    finish();
//                                }
//                            }
//                            else
//                            {
//                                String error_des = userInfoAPIResponse != null ? userInfoAPIResponse.getResult() : null;
//                                if((error_des != null && !error_des.equals(""))){
//                                    cdd.res.setText(error_des);
//                                }else
//                                {
//                                    cdd.res.setText(getString(R.string.went_wrong));
//                                }
//                            }
//                        }
//                        catch ( Exception e)
//                        {
//                            cdd.res.setText(getString(R.string.went_wrong));
//                        }
//                    }
//                    @Override
//                    public void onProgress(int process) {
//                    }
//                }).Execute();
//    }
}
