package com.u.mygamesapp.activities.authentication;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.GodFatherActivity;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.models.Customer;
import com.u.mygamesapp.requestmodels.ResendCodeBody;
import com.u.mygamesapp.requestmodels.ResetBody;
import com.u.mygamesapp.responsemodels.APIResponse;

import java.util.Objects;

import static com.u.mygamesapp.api.APIConstants.RESEND_CODE;
import static com.u.mygamesapp.api.APIConstants.RESET_PASS;
import static com.u.mygamesapp.helpers.Constants.showGreenToast;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;

public class ResetPasswordActivity extends GodFatherActivity {

    private EditText codeEdit, passwordEdit, passwordConfirmEdit;
    private TextView resultTxt;
    private String phone, msg;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_reset_pass, null, false);
        contentLayout.addView(contentView, 0);
        Objects.requireNonNull(getSupportActionBar()).setElevation(0);
        setTitle(getString(R.string.reset_password));
        initLayout();

        phone = getIntent().getStringExtra("phone");
        msg = getIntent().getStringExtra("msg");
        showGreenToast(getApplicationContext(), msg);
    }

    private void initLayout() {
        resultTxt = findViewById(R.id.txt_result);
        codeEdit =  findViewById(R.id.edit_code);
        passwordEdit =  findViewById(R.id.edit_password);
        passwordConfirmEdit =  findViewById(R.id.edit_password_confirm);
    }

    public void ResetBtnAction(View v)
    {
        resultTxt.setText("");
        hideKeyboard(this);
        String password = passwordEdit.getText().toString().trim();
        String password_confirm = passwordConfirmEdit.getText().toString().trim();
        String code = codeEdit.getText().toString().trim();

        if (code.equals("")) {
            codeEdit.setError(getString(R.string.field_required));
            codeEdit.requestFocus();
            return;
        }
        if (password.equals("")) {
            passwordEdit.setError(getString(R.string.field_required));
            passwordEdit.requestFocus();
            return;
        }
        if (!password.equals(password_confirm)) {
            passwordConfirmEdit.setError(getString(R.string.password_confirm_not_equal_password));
            passwordConfirmEdit.requestFocus();
            return;
        }

        ResetBody inModel = new ResetBody(phone, password, code);

        String json = gson.toJson(inModel);
        new SendGetJsonApi(this, RESET_PASS, json,
                new CallBackListener() {
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<String> userInfoAPIResponse = getResponse(result, String.class);
                            if (userInfoAPIResponse != null && userInfoAPIResponse.getResult().equals("success"))
                            {
                                String msg = userInfoAPIResponse.getContent();
                                showGreenToast(getApplicationContext(), msg);
                                startActivity(new Intent(getBaseContext() , LoginActivity.class));
                                finish();
                            }
                            else
                            {
                                String error_des = userInfoAPIResponse != null ? userInfoAPIResponse.getResult() : null;
                                if((error_des != null && !error_des.equals(""))){
                                    resultTxt.setText(error_des);
                                }else
                                {
                                    resultTxt.setText(getString(R.string.went_wrong));
                                }
                            }
                        }
                        catch ( Exception e)
                        {
                            resultTxt.setText(getString(R.string.went_wrong));
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ).Execute();
    }

    public void ResendBtnAction(View view) {
        hideKeyboard(this);
        resultTxt.setText("");

        ResendCodeBody inModel = new ResendCodeBody(phone);
        String json = gson.toJson(inModel);
        new SendGetJsonApi(this, RESEND_CODE, json,
                new CallBackListener() {
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<Customer> userInfoAPIResponse = getResponse(result, Customer.class);
                            if (userInfoAPIResponse != null && userInfoAPIResponse.getResult().equals("success"))
                            {
                                String msg = getString(R.string.code_sent);
                                showGreenToast(getApplicationContext(), msg);
                            }
                            else
                            {
                                String error_des = userInfoAPIResponse != null ? userInfoAPIResponse.getResult() : null;
                                if((error_des != null && !error_des.equals(""))){
                                    resultTxt.setText(error_des);
                                }else
                                {
                                    resultTxt.setText(getString(R.string.went_wrong));
                                }
                            }
                        }
                        catch ( Exception e)
                        {
                            resultTxt.setText(getString(R.string.went_wrong));
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }, true).Execute();
    }

}
