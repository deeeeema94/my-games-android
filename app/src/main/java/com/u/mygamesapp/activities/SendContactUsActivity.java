package com.u.mygamesapp.activities;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.u.intlphoneinput.IntlPhoneInput;
import com.u.mygamesapp.R;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.helpers.CustomDialog;
import com.u.mygamesapp.models.ContactUs;
import com.u.mygamesapp.requestmodels.ContactUsBody;
import com.u.mygamesapp.responsemodels.APIResponse;

import java.util.Objects;

import static com.u.mygamesapp.api.APIConstants.ADD;
import static com.u.mygamesapp.api.APIConstants.CONTACT_US;
import static com.u.mygamesapp.helpers.Constants.DIALOG_TYPE_MESSAGE_SENT;
import static com.u.mygamesapp.helpers.Constants.showGreenToast;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;

public class SendContactUsActivity extends GodFatherActivity {

    private EditText titleEdit, msgEdit;
    private TextView resultTxt;
    private EditText firstNameEdit, lastNameEdit;
    private IntlPhoneInput phoneEdit;
    private LinearLayout notAuthenticated;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_send_contact_us, null, false);
        contentLayout.addView(contentView, 0);
        setTitle(getString(R.string.contact_us));

        contact_us_layout.setVisibility(View.GONE);
        initLayout();
    }

    private void initLayout() {
        resultTxt = findViewById(R.id.txt_result);
        titleEdit =  findViewById(R.id.edit_subject);
        msgEdit =  findViewById(R.id.edit_message);
        phoneEdit = findViewById(R.id.edit_phone);
        phoneEdit.setEmptyDefault("sy");
        firstNameEdit = findViewById(R.id.edit_first_name);
        lastNameEdit = findViewById(R.id.edit_last_name);
        notAuthenticated = findViewById(R.id.not_authenticated);

        if(!isAuthenticated){
            notAuthenticated.setVisibility(View.VISIBLE);
        }
        else {
            notAuthenticated.setVisibility(View.GONE);
        }
    }

    public void SendBtnAction(View v)
    {
        hideKeyboard(this);

        final CustomDialog cdd = new CustomDialog(SendContactUsActivity.this, DIALOG_TYPE_MESSAGE_SENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(cdd.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        String title = titleEdit.getText().toString().trim();
        String msg = msgEdit.getText().toString().trim();

        if (title.equals("")) {
            titleEdit.setError(getString(R.string.field_required));titleEdit.requestFocus();  return;
        }
        if (msg.equals("")) {
            msgEdit.setError(getString(R.string.field_required));msgEdit.requestFocus();  return;
        }

        ContactUsBody inModel = new ContactUsBody(title, msg);
        if(!isAuthenticated){
            String userName = phoneEdit.getText().trim();
            String firstName = firstNameEdit.getText().toString().trim();
            String lastName = lastNameEdit.getText().toString().trim();

            if (firstName.equals("")) {
                firstNameEdit.setError(getString(R.string.field_required));firstNameEdit.requestFocus(); return;
            }
            if (lastName.equals("")) {
                lastNameEdit.setError(getString(R.string.field_required));lastNameEdit.requestFocus(); return;
            }
            if (userName.equals("")) {
                phoneEdit.setError(getString(R.string.field_required));
                phoneEdit.requestFocus();
                return;
            }
            inModel = new ContactUsBody(title, msg, userName, firstName, lastName);
        }

        String json = gson.toJson(inModel);
        cdd.show();
        new SendGetJsonApi(this, CONTACT_US + ADD, json,
                new CallBackListener() {
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<ContactUs> userInfoAPIResponse = getResponse(result, ContactUs.class);
                            if (userInfoAPIResponse != null && userInfoAPIResponse.getResult().equals("success"))
                            {
                                titleEdit.setText("");
                                msgEdit.setText("");
                                firstNameEdit.setText("");
                                lastNameEdit.setText("");
                                phoneEdit.setNumber("");
                                showGreenToast(SendContactUsActivity.this, getString(R.string.message_sent));
                            }
                            else
                            {
                                String error_des = userInfoAPIResponse != null ? userInfoAPIResponse.getResult() : null;
                                if((error_des != null && !error_des.equals(""))){
                                    resultTxt.setText(error_des);
                                }else
                                {
                                    resultTxt.setText(getString(R.string.went_wrong));
                                }
                            }
                        }
                        catch ( Exception e)
                        {
                            resultTxt.setText(getString(R.string.went_wrong));
                        }
                        new Handler().postDelayed(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        cdd.dismiss();
                                    }
                                },
                                2000
                        );

                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }, false).Execute();
    }
}
