package com.u.mygamesapp.activities.authentication;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.u.intlphoneinput.IntlPhoneInput;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;
import com.u.mygamesapp.activities.GodFatherActivity;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.helpers.JsonParser;
import com.u.mygamesapp.models.City;
import com.u.mygamesapp.models.Customer;
import com.u.mygamesapp.requestmodels.PaginationRequest;
import com.u.mygamesapp.requestmodels.RegisterBody;
import com.u.mygamesapp.responsemodels.APIResponse;
import com.u.mygamesapp.responsemodels.PagedAPIResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.u.mygamesapp.api.APIConstants.CITIES;
import static com.u.mygamesapp.api.APIConstants.GET_ALL;
import static com.u.mygamesapp.api.APIConstants.REGISTER;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_FCM_TOKEN;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readFromPreferences;

public class RegisterActivity extends GodFatherActivity implements AdapterView.OnItemSelectedListener {

    public int userId = 0;
    private EditText passwordEdit, firstNameEdit, lastNameEdit;
    private IntlPhoneInput phoneEdit;
    private TextView resultTxt;
    private Spinner citiesSpinner;
    private City[] citiesArray;
    private int cityId = 0;
    private static ArrayAdapter<String> citiesAdapter;
    private RadioGroup genderRadioGroup;
    private RadioButton male, female;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.sign_up));
        Objects.requireNonNull(getSupportActionBar()).setElevation(0);

        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View contentView = inflater != null ? inflater.inflate(R.layout.activity_register, null, false) : null;
        contentLayout.addView(contentView, 0);

        initLayout();

        getCities();
    }

    private void initLayout() {
        resultTxt = findViewById(R.id.txt_result);
        phoneEdit = findViewById(R.id.edit_phone);
        phoneEdit.setEmptyDefault("sy");
        passwordEdit = findViewById(R.id.edit_password);
        firstNameEdit = findViewById(R.id.edit_first_name);
        lastNameEdit = findViewById(R.id.edit_last_name);
        passwordEdit = findViewById(R.id.edit_password);
        genderRadioGroup = findViewById(R.id.radio_gender);
        male = findViewById(R.id.male_btn);
        female = findViewById(R.id.female_btn);
        citiesSpinner = findViewById(R.id.cities);
        List<String> names = new ArrayList<>();
        names.add(getString(R.string.loading));
        citiesAdapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_item,names);
        citiesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        citiesSpinner.setAdapter(citiesAdapter);
    }

    public void RegisterBtnAction(View v) {
        hideKeyboard(this);
        resultTxt.setText("");

        String userName = phoneEdit.getText().trim();
        String password = passwordEdit.getText().toString().trim();
        String firstName = firstNameEdit.getText().toString().trim();
        String lastName = lastNameEdit.getText().toString().trim();

        if (firstName.equals("")) {
            firstNameEdit.setError(getString(R.string.field_required));firstNameEdit.requestFocus(); return;
        }
        if (lastName.equals("")) {
            lastNameEdit.setError(getString(R.string.field_required));lastNameEdit.requestFocus(); return;
        }
        if (userName.equals("")) {
            phoneEdit.setError(getString(R.string.field_required));
            phoneEdit.requestFocus();
            return;
        }

        if(!isValidPassword(password)){
            passwordEdit.requestFocus();
            return;
        }

        if(cityId == 0){
            resultTxt.setText(R.string.city_required);return;
        }
        else {
            resultTxt.setText("");
        }

        final int gender_id = genderRadioGroup.getCheckedRadioButtonId();
        boolean gender = false;
        RadioButton genderRadioBtn = findViewById(gender_id);
        if(genderRadioBtn != null) {
            gender = genderRadioBtn.getText().equals(getString(R.string.female));
        }
        else {
            resultTxt.setText(R.string.gender_required);return;
        }

        if (password.equals("")) {
            passwordEdit.setError(getString(R.string.field_required));
            passwordEdit.requestFocus();
            return;
        }

        String FCM_Token = readFromPreferences(this, DEFAULT_FILE_NAME, PREF_FCM_TOKEN, "");
        RegisterBody registerBody = new RegisterBody(userName, password, firstName, lastName, true, gender, cityId, FCM_Token);

        String json = gson.toJson(registerBody);
        new SendGetJsonApi(this, REGISTER, json,
                new CallBackListener() {
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<Customer> userInfoAPIResponse = getResponse(result, Customer.class);
                            if (userInfoAPIResponse != null && userInfoAPIResponse.getResult().equals("success"))
                            {
                                Customer customer = userInfoAPIResponse.getContent();
                                saveUserData(customer);
                                if(customer.getUser().isPhoneNumberConfirmed()){
                                    startActivity(new Intent(getBaseContext() , BaseActivity.class));
                                }
                                else {
                                    startActivity(new Intent(getBaseContext() , VerifyActivity.class));
                                }
                                finish();
                            }
                            else
                            {
                                String error_des = null;
                                if (userInfoAPIResponse != null) {
                                    error_des = userInfoAPIResponse.getResult();
                                }
                                if((error_des != null && !error_des.equals(""))){
                                    resultTxt.setText(error_des);
                                }else
                                {
                                    resultTxt.setText(getString(R.string.went_wrong));
                                }
                            }
                        }
                        catch ( Exception e)
                        {
                            resultTxt.setText(getString(R.string.went_wrong));
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }, true).Execute();
    }

    public void LoginBtnAction(View view) {
        Intent LoginIntent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(LoginIntent);
        finish();
    }

    private void getCities(){
        PaginationRequest paginationRequest = new PaginationRequest(1, 100);
        String json = gson.toJson(paginationRequest);
        new SendGetJsonApi(this, CITIES + GET_ALL, json,
                new CallBackListener() {
                    @Override
                    public void onFinish(String result)
                    {
                        PagedAPIResponse<City[]> apiResponse = JsonParser.getPagedResponse(result, City[].class);
                        try {
                            if (apiResponse != null && apiResponse.getMessage().equals("success")) {
                                citiesArray = apiResponse.getContent().getContent();
                                List<String> names = new ArrayList<>();
                                names.add(getString(R.string.select_city));

                                for (City city : citiesArray) {
                                    names.add(city.getDisplayName());
                                }
                                citiesSpinner.setOnItemSelectedListener(RegisterActivity.this);
                                citiesAdapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_item,names);
                                citiesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                citiesSpinner.setAdapter(citiesAdapter);
                                citiesSpinner.setSelection(names.size() - 1);
                                citiesSpinner.setPadding(10, 0, 0, 10);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                    citiesSpinner.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                                }
                            }
                            else
                            {
                                String error_des = apiResponse != null ? apiResponse.getMessage() : null;
                                if ((error_des != null && !error_des.equals(""))) {
                                    resultTxt.setText(error_des);
                                } else {
                                    resultTxt.setText(getString(R.string.went_wrong));
                                }
                            }
                        }
                        catch ( Exception e)
                        {
                            resultTxt.setText(getString(R.string.went_wrong));
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        , false).Execute();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Spinner spin = (Spinner) adapterView;
        if (spin.getId() == R.id.cities && i > 0) {
            cityId = citiesArray[i-1].getId();
            resultTxt.setText("");
        }
        else {
            resultTxt.setText(getString(R.string.city_required));
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public boolean isValidPassword(String password)
    {
        if (password.length() < 6)
        {
            resultTxt.setText(getString(R.string.password_length));
            return false;
        }
        String upperCaseChars = "(.*[A-Z].*)";
        if (!password.matches(upperCaseChars ))
        {
            resultTxt.setText(getString(R.string.password_upper));
            return false;
        }
        String lowerCaseChars = "(.*[a-z].*)";
        if (!password.matches(lowerCaseChars ))
        {
            resultTxt.setText(getString(R.string.password_lower));
            return false;
        }
        String numbers = "(.*[0-9].*)";
        if (!password.matches(numbers ))
        {
            resultTxt.setText(R.string.password_digit);
            return false;
        }
        String specialChars = "(.*[@,#,$,%].*$)";
        if (!password.matches(specialChars ))
        {
            resultTxt.setText(getString(R.string.password_char));
            return false;
        }
        return true;
    }
}
