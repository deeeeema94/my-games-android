package com.u.mygamesapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.u.mygamesapp.R;
import com.u.mygamesapp.models.Group;

import java.util.List;

public class GroupsAdapter extends RecyclerView.Adapter<GroupsAdapter.ItemViewHolder> {
    private Context mContext;
    private List<Group> itemsList;
    private OnItemClickListener mListener;

    public void setOnItemClickListener(FragmentActivity activity) {
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public GroupsAdapter(Context context, List<Group> groups) {
        mContext = context;
        itemsList = groups;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_group, parent, false);
        return new ItemViewHolder(v);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint({"DefaultLocale", "UseCompatLoadingForDrawables"})
    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        final Group currentItem = itemsList.get(position);

        holder.groupLayout.setCardBackgroundColor(Color.parseColor(currentItem.getColor()));
        RelativeLayout.LayoutParams groupLayoutParams = (RelativeLayout.LayoutParams)holder.groupLayout.getLayoutParams();
        RelativeLayout.LayoutParams pointsTxtParams = (RelativeLayout.LayoutParams)holder.pointsLayout.getLayoutParams();

        if(position%2 == 0){
            groupLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            holder.groupLayout.setLayoutParams(groupLayoutParams);
            pointsTxtParams.addRule(RelativeLayout.LEFT_OF, R.id.group_card);
            holder.pointsLayout.setLayoutParams(pointsTxtParams);
        }
        else {
            groupLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            holder.groupLayout.setLayoutParams(groupLayoutParams);
            pointsTxtParams.addRule(RelativeLayout.RIGHT_OF, R.id.group_card);
            holder.pointsLayout.setLayoutParams(pointsTxtParams);
        }

        holder.nameTxt.setText(currentItem.getDisplayName());
        holder.pointsTxt.setText(String.format("%d %s", currentItem.getPoints(), mContext.getString(R.string.points)));
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTxt;
        public TextView pointsTxt;
        public CardView groupLayout;
        public LinearLayout pointsLayout;

        public ItemViewHolder(View v) {
            super(v);
            nameTxt = v.findViewById(R.id.group_name);
            pointsTxt = v.findViewById(R.id.group_points);
            groupLayout = v.findViewById(R.id.group_card);
            pointsLayout = v.findViewById(R.id.points_layout);
        }
    }
}
