package com.u.mygamesapp.adapters;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class PagerAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> fragmentList = new ArrayList<>();

    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setFragmentList(List<Fragment> fragmentList) {
        this.fragmentList = fragmentList;
    }

    private Fragment getFragment(int position) {
        return fragmentList.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        try{
            return getFragment(position);
        }
        catch (Exception e){
            return null;
        }
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }
}
