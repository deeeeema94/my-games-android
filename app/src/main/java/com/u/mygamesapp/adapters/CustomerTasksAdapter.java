package com.u.mygamesapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.u.mygamesapp.R;
import com.u.mygamesapp.models.CustomerTask;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.u.mygamesapp.api.APIConstants.API_IMAGES_URL;
import static com.u.mygamesapp.helpers.Constants.TASK_TYPE_MATCH;

public class CustomerTasksAdapter extends RecyclerView.Adapter<CustomerTasksAdapter.ItemViewHolder> {
    private Context mContext;
    private List<CustomerTask> tasks;
    private OnItemClickListener mListener;

    public void setOnItemClickListener(FragmentActivity activity) {
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public CustomerTasksAdapter(Context context, List<CustomerTask> appTasks) {
        mContext = context;
        tasks = appTasks;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_customer_task, parent, false);
        return new ItemViewHolder(v);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        final CustomerTask currentItem = tasks.get(position);
        Glide.with(mContext)
                .load(API_IMAGES_URL + currentItem.getAppTask().getImageUrl())
                .circleCrop()
                .placeholder(R.drawable.icon)
                .into(holder.img);
        holder.nameTxt.setText(currentItem.getAppTask().getDisplayName());
        holder.pointsTxt.setText(String.format(" %d ", currentItem.getAppTask().getPoints()));

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");

        if (currentItem.isDone()) {
            holder.statusTxt.setBackground(mContext.getResources().getDrawable(R.drawable.badge_accepted));
            holder.statusTxt.setText(R.string.done);
            try {
                Date doneDate = null;
                doneDate = format.parse(currentItem.getDoneDate());
                String done = formatter.format(doneDate.getTime());
                holder.earnDateTxt.setText(String.format("%s", done));
            } catch (Exception e) {
                holder.earnDateTxt.setText(currentItem.getDoneDate());
                e.printStackTrace();
            }
            holder.earnDateTxt.setVisibility(View.VISIBLE);
        } else {
            holder.statusTxt.setBackground(mContext.getResources().getDrawable(R.drawable.badge_pending));
            holder.statusTxt.setText(R.string.started);
            holder.earnDateTxt.setVisibility(View.GONE);
        }

        holder.earned_points.setText(String.format(" %d ", currentItem.getEarnedPoints()));
        if(currentItem.getAppTask().getTaskType().getName().equals(TASK_TYPE_MATCH)){
            holder.match_score.setVisibility(View.VISIBLE);

            String fFlag = API_IMAGES_URL + currentItem.getSportMatch().getFirstTeamFlag();
            String sFlag = API_IMAGES_URL + currentItem.getSportMatch().getSecondTeamFlag();
            Glide.with(mContext)
                    .load(fFlag)
                    .optionalCenterInside()
                    .into(holder.team1Flag);

            Glide.with(mContext)
                    .load(sFlag)
                    .optionalCenterInside()
                    .into(holder.team2Flag);

            holder.team1Name.setText(currentItem.getSportMatch().getFirstTeamName());
            holder.team2Name.setText(currentItem.getSportMatch().getSecondTeamName());
            holder.team1Score.setText(String.format("%d", currentItem.getSportMatch().getFirstTeamScore()));
            holder.team2Score.setText(String.format("%d", currentItem.getSportMatch().getSecondTeamScore()));
        }
        else {
            holder.match_score.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return tasks.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTxt;
        public ImageView img;
        public TextView earnDateTxt;
        public TextView statusTxt;
        public TextView pointsTxt;
        public LinearLayout match_score;
        public ImageView team1Flag, team2Flag;
        public TextView team1Name, team2Name, team1Score, team2Score, earned_points;

        public ItemViewHolder(View v) {
            super(v);
            nameTxt = v.findViewById(R.id.name);
            img = v.findViewById(R.id.img);
            earnDateTxt = v.findViewById(R.id.earn_date);
            statusTxt = v.findViewById(R.id.status);
            pointsTxt = v.findViewById(R.id.points);
            match_score = v.findViewById(R.id.match_score);
            team1Flag = v.findViewById(R.id.team1_flag);
            team2Flag = v.findViewById(R.id.team2_flag);
            team1Name = v.findViewById(R.id.team1_name);
            team2Name = v.findViewById(R.id.team2_name);
            team1Score = v.findViewById(R.id.team1_score);
            team2Score = v.findViewById(R.id.team2_score);
            earned_points = v.findViewById(R.id.earned_points);
        }
    }
}
