package com.u.mygamesapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.UiThread;

import com.u.mygamesapp.R;
import com.u.mygamesapp.helpers.FAQAnswerViewHolder;
import com.u.mygamesapp.helpers.FAQQuestionViewHolder;
import com.u.mygamesapp.models.ContactUs;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class ExpandableFAQsAdapter extends ExpandableRecyclerAdapter<ContactUs, String, FAQQuestionViewHolder, FAQAnswerViewHolder> {

    private static final int PARENT_VEGETARIAN = 0;
    private static final int PARENT_NORMAL = 1;
    private static final int CHILD_NORMAL = 3;
    private Context mContext;
    private LayoutInflater mInflater;
    private List<ContactUs> mFAQItemList;

    public ExpandableFAQsAdapter(Context context, @NonNull ArrayList<ContactUs> faqItemList) {
        super(faqItemList);
        mFAQItemList = faqItemList;
        mInflater = LayoutInflater.from(context);
        mContext = context;
    }

    @UiThread
    @NonNull
    @Override
    public FAQQuestionViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View faqItemView;
        faqItemView = mInflater.inflate(R.layout.item_faq, parentViewGroup, false);
        return new FAQQuestionViewHolder(mContext, faqItemView);
    }

    @UiThread
    @NonNull
    @Override
    public FAQAnswerViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        final View matchView;
        matchView = mInflater.inflate(R.layout.item_faq_answer, childViewGroup, false);
        return new FAQAnswerViewHolder(mContext, matchView);
    }

    @UiThread
    @Override
    public void onBindParentViewHolder(@NonNull FAQQuestionViewHolder faqItemViewHolder, int parentPosition, @NonNull ContactUs faqItem) {
        faqItemViewHolder.bind(faqItem);
    }

    @UiThread
    @Override
    public void onBindChildViewHolder(@NonNull FAQAnswerViewHolder matchViewHolder, int parentPosition, int childPosition, @NonNull String match) {
        try {
            matchViewHolder.bind(match);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getParentViewType(int parentPosition) {
        return PARENT_NORMAL;
    }

    @Override
    public int getChildViewType(int parentPosition, int childPosition) {
        return CHILD_NORMAL;
    }

    @Override
    public boolean isParentViewType(int viewType) {
        return viewType == PARENT_VEGETARIAN || viewType == PARENT_NORMAL;
    }
}

