package com.u.mygamesapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.u.mygamesapp.R;
import com.u.mygamesapp.fragments.MatchTaskDetailsFragment;
import com.u.mygamesapp.fragments.TaskDetailsFragment;
import com.u.mygamesapp.fragments.YoutubeTaskDetailsFragment;
import com.u.mygamesapp.models.AppTask;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.u.mygamesapp.api.APIConstants.API_IMAGES_URL;
import static com.u.mygamesapp.helpers.Constants.TASK_TYPE_MATCH;
import static com.u.mygamesapp.helpers.Constants.TASK_TYPE_YOUTUBE_SUBSCRIBE;

public class TasksAdapter extends RecyclerView.Adapter<TasksAdapter.ItemViewHolder> {
    private Context mContext;
    private List<AppTask> tasks;
    private OnItemClickListener mListener;

    public void setOnItemClickListener(FragmentActivity activity) {
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public TasksAdapter(Context context, List<AppTask> appTasks) {
        mContext = context;
        tasks = appTasks;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_task, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        final AppTask currentItem = tasks.get(position);
        Glide.with(mContext)
                .load(API_IMAGES_URL + currentItem.getImageUrl())
                .circleCrop()
                .placeholder(R.drawable.icon)
                .into(holder.img);
        holder.nameTxt.setText(currentItem.getDisplayName());
        holder.pointsTxt.setText(String.format("%d", currentItem.getPoints()));
        if(currentItem.isStared()){
            holder.special.setVisibility(View.VISIBLE);
        }
        else {
            holder.special.setVisibility(View.GONE);
        }

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");

        Date endDate = null;
        try {
            endDate = format.parse(currentItem.getEndDate());
            String end = formatter.format(endDate.getTime());
            holder.endTxt.setText(String.format("%s: %s", mContext.getString(R.string.ends_on), end));
        } catch (Exception e) {
            holder.endTxt.setText(String.format("%s: %s", mContext.getString(R.string.ends_on), currentItem.getEndDate()));
            e.printStackTrace();
        }

        final AppCompatActivity activity = (AppCompatActivity) holder.endTxt.getContext();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currentItem.getTaskType().getName().equals(TASK_TYPE_YOUTUBE_SUBSCRIBE)){
                    activity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, new YoutubeTaskDetailsFragment(currentItem.getId()))
                            .addToBackStack(null)
                            .commit();
                }
                else if(currentItem.getTaskType().getName().equals(TASK_TYPE_MATCH)){
                    activity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, new MatchTaskDetailsFragment(currentItem.getId()))
                            .addToBackStack(null)
                            .commit();
                }
                else {
                    activity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, new TaskDetailsFragment(currentItem.getId()))
                            .addToBackStack(null)
                            .commit();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return tasks.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTxt;
        public ImageView img;
        public TextView pointsTxt;
        public TextView endTxt;
        public ImageView special;

        public ItemViewHolder(View v) {
            super(v);
            nameTxt = v.findViewById(R.id.name);
            img = v.findViewById(R.id.img);
            pointsTxt = v.findViewById(R.id.points);
            endTxt = v.findViewById(R.id.end_date);
            special = v.findViewById(R.id.special);
        }
    }
}
