package com.u.mygamesapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.u.mygamesapp.R;
import com.u.mygamesapp.models.CustomerPrize;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.u.mygamesapp.api.APIConstants.API_IMAGES_URL;
import static com.u.mygamesapp.helpers.Constants.ACCEPTED_STATUS;
import static com.u.mygamesapp.helpers.Constants.REJECTED_STATUS;

public class CustomerPrizesAdapter extends RecyclerView.Adapter<CustomerPrizesAdapter.ItemViewHolder> {
    private Context mContext;
    private List<CustomerPrize> itemsList;
    private OnItemClickListener mListener;

    public void setOnItemClickListener(FragmentActivity activity) {
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public CustomerPrizesAdapter(Context context, List<CustomerPrize> prizes) {
        mContext = context;
        itemsList = prizes;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_customer_prize, parent, false);
        return new ItemViewHolder(v);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        final CustomerPrize currentItem = itemsList.get(position);
        Glide.with(mContext)
                .load(API_IMAGES_URL + currentItem.getPrize().getImageUrl())
                .fitCenter()
                .placeholder(R.drawable.icon)
                .into(holder.img);
        holder.nameTxt.setText(currentItem.getPrize().getDisplayName());
        holder.statusTxt.setText(String.format("%s", currentItem.getPrizeStatus().getDisplayName()));
        if(currentItem.getPrize().getPrizeType().getName().equals("lucky_wheel")){
            holder.pointsTxt.setVisibility(View.GONE);
            holder.pointsImg.setVisibility(View.GONE);
        }
        else {
            holder.pointsImg.setVisibility(View.VISIBLE);
            holder.pointsTxt.setVisibility(View.VISIBLE);
            holder.pointsTxt.setText(String.format("%d %s", currentItem.getPrize().getPoints(), mContext.getString(R.string.points)));
        }
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");
        Date sendDate = null;
        try {
            sendDate = format.parse(currentItem.getRequestDate());
            String send = formatter.format(sendDate.getTime());
            holder.requestDateTxt.setText(String.format("%s %s", mContext.getString(R.string.requested_on), send));
        } catch (Exception e) {
            holder.requestDateTxt.setText(currentItem.getRequestDate());
            e.printStackTrace();
        }

        switch (currentItem.getPrizeStatus().getName()) {
            case ACCEPTED_STATUS:
                holder.statusTxt.setBackground(mContext.getResources().getDrawable(R.drawable.badge_accepted));
                try {
                    Date earnDate = null;
                    earnDate = format.parse(currentItem.getEarnDate());
                    String earn = formatter.format(earnDate.getTime());
                    holder.earnDateTxt.setText(String.format("%s: %s",mContext.getString(R.string.earned_on) , earn));
                } catch (Exception e) {
                    holder.earnDateTxt.setText(currentItem.getEarnDate());
                    e.printStackTrace();
                }
                holder.earnDateTxt.setVisibility(View.VISIBLE);
                holder.earnImg.setVisibility(View.VISIBLE);
                break;
            case REJECTED_STATUS:
                holder.statusTxt.setBackground(mContext.getResources().getDrawable(R.drawable.badge_rejected));
                try {
                    Date earnDate = null;
                    earnDate = format.parse(currentItem.getEarnDate());
                    String earn = formatter.format(earnDate.getTime());
                    holder.earnDateTxt.setText(String.format("%s: %s",mContext.getString(R.string.rejected_on) , earn));
                } catch (Exception e) {
                    holder.earnDateTxt.setText(currentItem.getEarnDate());
                    e.printStackTrace();
                }
                holder.earnDateTxt.setVisibility(View.VISIBLE);
                holder.earnImg.setVisibility(View.VISIBLE);

                break;
            default:
                holder.statusTxt.setBackground(mContext.getResources().getDrawable(R.drawable.badge_pending));
                holder.earnDateTxt.setVisibility(View.GONE);
                holder.earnImg.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTxt;
        public ImageView img, pointsImg, requestImg, earnImg;
        public TextView requestDateTxt;
        public TextView earnDateTxt;
        public TextView statusTxt;
        public TextView pointsTxt;

        public ItemViewHolder(View v) {
            super(v);
            nameTxt = v.findViewById(R.id.name);
            img = v.findViewById(R.id.img);
            requestDateTxt = v.findViewById(R.id.request_date);
            earnDateTxt = v.findViewById(R.id.earn_date);
            statusTxt = v.findViewById(R.id.status);
            pointsTxt = v.findViewById(R.id.points);
            pointsImg = v.findViewById(R.id.points_img);
            requestImg = v.findViewById(R.id.request_img);
            earnImg = v.findViewById(R.id.earn_img);
        }
    }
}
