package com.u.mygamesapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.u.mygamesapp.R;
import com.u.mygamesapp.models.Contact;

import static com.u.mygamesapp.api.APIConstants.API_IMAGES_URL;

public class FollowItemsAdapter extends RecyclerView.Adapter<FollowItemsAdapter.GameViewHolder> {
    private Context mContext;
    private Contact[] contacts;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public FollowItemsAdapter(Context context, Contact[] Contacts) {
        mContext = context;
        contacts = Contacts;
    }

    @NonNull
    @Override
    public GameViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_follow, parent, false);
        return new GameViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull GameViewHolder holder, int position) {
        Contact currentItem = contacts[position];
        final String posterUrl;

        posterUrl = API_IMAGES_URL + currentItem.getIcon();

        Glide.with(mContext).load(posterUrl).placeholder(mContext.getResources().getDrawable(R.drawable.contact_us))
                .error(mContext.getResources().getDrawable(R.drawable.contact_us)).into(holder.mImage);
        holder.mTxt.setText(String.format(" %s", currentItem.getValue()));
    }

    @Override
    public int getItemCount() {
        return contacts.length;
    }

    public class GameViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImage;
        public TextView mTxt;

        public GameViewHolder(View v) {
            super(v);
            mImage = v.findViewById(R.id.img);
            mTxt = v.findViewById(R.id.text);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v1) {
                    if (mListener != null) {
                        int position = GameViewHolder.this.getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            mListener.onItemClick(position);

                            switch (contacts[position].getName()) {
                                case "email":
                                    try {
                                        Intent intent = new Intent(Intent.ACTION_SEND);
                                        intent.setType("plain/text");
                                        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{contacts[position].getValue()});
                                        intent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
                                        intent.putExtra(Intent.EXTRA_TEXT, "");
                                        mContext.startActivity(Intent.createChooser(intent, ""));
                                    } catch (Exception ignored) {
                                    }
                                    break;
                                case "facebook":
                                    try {
                                        Uri uri = Uri.parse(contacts[position].getValue());
                                        if (!contacts[position].getValue().contains("https://")) {
                                            uri = Uri.parse("https://" + contacts[position].getValue());
                                        }
                                        try {
                                            ApplicationInfo applicationInfo = mContext.getPackageManager().getApplicationInfo("com.facebook.katana", 0);
                                            if (applicationInfo.enabled) {
                                                if (!contacts[position].getValue().contains("https://")) {
                                                    uri = Uri.parse("fb://facewebmodal/f?href=https://" + contacts[position].getValue());
                                                } else {
                                                    uri = Uri.parse("fb://facewebmodal/f?href=" + contacts[position].getValue());
                                                }
                                            }
                                        } catch (PackageManager.NameNotFoundException ignored) {
                                        }
                                        mContext.startActivity(new Intent(Intent.ACTION_VIEW, uri));
                                    } catch (Exception ignored) {
                                    }
                                    break;
                                case "mobile":
                                    try {
                                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", contacts[position].getValue(), null));
                                        mContext.startActivity(intent);
                                    } catch (Exception ignored) {
                                    }
                                    break;
                                default:
                                    try {
                                        Uri uri = Uri.parse(contacts[position].getValue());
                                        mContext.startActivity(new Intent(Intent.ACTION_VIEW, uri));
                                    } catch (Exception ignored) {
                                    }
                                    break;
                            }
                        }
                    }
                }
            });
        }
    }
}
