package com.u.mygamesapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.u.mygamesapp.R;
import com.u.mygamesapp.models.Prize;

import java.util.List;

import static com.u.mygamesapp.api.APIConstants.API_IMAGES_URL;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_DAILY_BONUS_LEVEL;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readFromPreferences;

public class DailyPrizesAdapter extends RecyclerView.Adapter<DailyPrizesAdapter.ItemViewHolder> {
    private Context mContext;
    private List<Prize> itemsList;
    private OnItemClickListener mListener;

    public void setOnItemClickListener(FragmentActivity activity) {
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public DailyPrizesAdapter(Context context, List<Prize> prizes) {
        mContext = context;
        itemsList = prizes;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_daily_prize, parent, false);
        return new ItemViewHolder(v);
    }

    @SuppressLint({"DefaultLocale", "UseCompatLoadingForDrawables"})
    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        final Prize currentItem = itemsList.get(position);
        Glide.with(mContext)
                .load(API_IMAGES_URL + currentItem.getImageUrl())
                .fitCenter()
                .placeholder(R.drawable.icon)
                .into(holder.img);
        holder.nameTxt.setText(currentItem.getDisplayName());
        holder.orderTxt.setText(String.format("%s %d", mContext.getString(R.string.day), currentItem.getItemOrder()));

        int level = Integer.parseInt(readFromPreferences(mContext, DEFAULT_FILE_NAME, PREF_DAILY_BONUS_LEVEL, "1"));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (currentItem.getItemOrder() < level) {
                holder.orderTxt.setBackground(mContext.getDrawable(R.drawable.order_start_background));
            }
            else if (currentItem.getItemOrder() == level){
                holder.orderTxt.setBackground(mContext.getDrawable(R.drawable.order_center_background));
            }
            else{
                holder.orderTxt.setBackground(mContext.getDrawable(R.drawable.order_end_background));
            }
        }

        if(currentItem.getItemOrder() < level)
            holder.done.setVisibility(View.VISIBLE);
        else
            holder.done.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTxt;
        public TextView orderTxt;
        public ImageView img;
        public ImageView done;

        public ItemViewHolder(View v) {
            super(v);
            nameTxt = v.findViewById(R.id.name);
            orderTxt = v.findViewById(R.id.order);
            img = v.findViewById(R.id.img);
            done = v.findViewById(R.id.done);
        }
    }
}
