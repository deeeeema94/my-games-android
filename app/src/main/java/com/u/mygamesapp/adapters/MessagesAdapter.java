package com.u.mygamesapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.u.mygamesapp.R;
import com.u.mygamesapp.fragments.MessageDetailsFragment;
import com.u.mygamesapp.models.CustomerMessage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.u.mygamesapp.fragments.MessageDetailsFragment.MESSAGE_DETAILS_FRAGMENT_TAG;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ItemViewHolder> {
    private Context mContext;
    private List<CustomerMessage> itemsList;
    private OnItemClickListener mListener;

    public void setOnItemClickListener(FragmentActivity activity) {
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public MessagesAdapter(Context context, List<CustomerMessage> messages) {
        mContext = context;
        itemsList = messages;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_message, parent, false);
        return new ItemViewHolder(v);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint({"DefaultLocale", "UseCompatLoadingForDrawables"})
    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        final CustomerMessage currentItem = itemsList.get(position);

        holder.titleTxt.setText(currentItem.getMessage().getTitle());
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");
        Date sendDate = null;
        try {
            sendDate = format.parse(currentItem.getSendDate());
            String send = formatter.format(sendDate.getTime());
            holder.dateTxt.setText(String.format("%s", send));
        } catch (Exception e) {
            holder.dateTxt.setText(currentItem.getSendDate());
            e.printStackTrace();
        }

        if(currentItem.isRead()){
            holder.titleTxt.setTextColor(Color.LTGRAY);
            holder.dateTxt.setTextColor(Color.LTGRAY);
            holder.img.setImageDrawable(mContext.getDrawable(R.drawable.message_grey_8));
        }
        else {
            holder.titleTxt.setTextColor(Color.BLACK);
            holder.dateTxt.setTextColor(Color.BLACK);
            holder.img.setImageDrawable(mContext.getDrawable(R.drawable.message_8));
        }

        final AppCompatActivity activity = (AppCompatActivity) holder.img.getContext();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new MessageDetailsFragment(currentItem))
                        .addToBackStack(MESSAGE_DETAILS_FRAGMENT_TAG)
                        .commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView titleTxt;
        public ImageView img;
        public TextView dateTxt;

        public ItemViewHolder(View v) {
            super(v);
            titleTxt = v.findViewById(R.id.msg_title);
            img = v.findViewById(R.id.msg_img);
            dateTxt = v.findViewById(R.id.msg_date);
        }
    }
}
