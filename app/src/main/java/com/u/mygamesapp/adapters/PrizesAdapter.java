package com.u.mygamesapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.u.mygamesapp.R;
import com.u.mygamesapp.fragments.PrizeDetailsFragment;
import com.u.mygamesapp.models.Prize;

import java.util.List;

import static com.u.mygamesapp.api.APIConstants.API_IMAGES_URL;

public class PrizesAdapter extends RecyclerView.Adapter<PrizesAdapter.ItemViewHolder> {
    private Context mContext;
    private List<Prize> itemsList;
    private OnItemClickListener mListener;

    public void setOnItemClickListener(FragmentActivity activity) {
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public PrizesAdapter(Context context, List<Prize> prizes) {
        mContext = context;
        itemsList = prizes;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_prize, parent, false);
        return new ItemViewHolder(v);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        final Prize currentItem = itemsList.get(position);
        Glide.with(mContext)
                .load(API_IMAGES_URL + currentItem.getImageUrl())
                .fitCenter()
                .placeholder(R.drawable.icon)
                .into(holder.img);
        holder.nameTxt.setText(currentItem.getDisplayName());
        holder.pointsTxt.setText(String.format("%d", currentItem.getPoints()));

        final AppCompatActivity activity = (AppCompatActivity) holder.img.getContext();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new PrizeDetailsFragment(currentItem.getId()))
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTxt;
        public ImageView img;
        public TextView pointsTxt;

        public ItemViewHolder(View v) {
            super(v);
            nameTxt = v.findViewById(R.id.name);
            img = v.findViewById(R.id.img);
            pointsTxt = v.findViewById(R.id.points);
        }
    }
}
