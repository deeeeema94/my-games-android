package com.u.mygamesapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.smarteist.autoimageslider.SliderViewAdapter;
import com.u.mygamesapp.R;
import com.u.mygamesapp.models.Content;

import java.util.List;

import static com.u.mygamesapp.api.APIConstants.API_IMAGES_URL;

public class SliderAdapter extends SliderViewAdapter<SliderAdapter.SliderAdapterVH> /*RecyclerView.Adapter<SliderAdapter.SliderAdapterVH>*/ {

    private Context context;
    private int mCount;
    private List<Content> pictures;

    public SliderAdapter(Context context, List<Content> pics) {
        this.context = context;
        pictures = pics;
    }

    public void setCount(int count) {
        this.mCount = count;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_slider_layout, null);
        return new SliderAdapterVH(inflate);
    }

//    @NonNull
//    @Override
//    public SliderAdapterVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_slider_layout, null);
//        return new SliderAdapterVH(inflate);
//    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, final int position) {

        viewHolder.textViewDescription.setTextSize(20);
        viewHolder.textViewDescription.setTextColor(Color.WHITE);
        viewHolder.imageGifContainer.setVisibility(View.VISIBLE);

        viewHolder.textViewDescription.setText(pictures.get(position).getTitle());
        String poster = API_IMAGES_URL + pictures.get(position).getImageUrl();
        Glide.with(viewHolder.itemView)
                .load(poster)
                .centerInside()
                .into(viewHolder.imageViewBackground);

    }

//    @Override
//    public int getItemCount() {
//        return mCount;
//    }

    @Override
    public int getCount() {
        return mCount;
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder /*RecyclerView.ViewHolder*/ {

        View itemView;
        ImageView imageViewBackground;
        ImageView imageGifContainer;
        TextView textViewDescription;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
            imageGifContainer = itemView.findViewById(R.id.iv_gif_container);
            textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider);
            this.itemView = itemView;
        }
    }
}
