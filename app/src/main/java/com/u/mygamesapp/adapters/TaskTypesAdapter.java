package com.u.mygamesapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.u.mygamesapp.R;
import com.u.mygamesapp.fragments.TasksFragment;
import com.u.mygamesapp.models.TaskType;

import java.util.List;

import static com.u.mygamesapp.api.APIConstants.API_IMAGES_URL;

public class TaskTypesAdapter extends RecyclerView.Adapter<TaskTypesAdapter.ItemViewHolder> {
    private Context mContext;
    private List<TaskType> taskTypes;
    private OnItemClickListener mListener;

    public void setOnItemClickListener(FragmentActivity activity) {
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public TaskTypesAdapter(Context context, List<TaskType> types) {
        mContext = context;
        taskTypes = types;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_task_type, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        final TaskType currentItem = taskTypes.get(position);

        Glide.with(mContext)
                .load(API_IMAGES_URL + currentItem.getImageUrl())
                .fitCenter()
                .placeholder(R.drawable.icon)
                .into(holder.img);

        final AppCompatActivity activity = (AppCompatActivity) holder.img.getContext();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new TasksFragment(currentItem.getId()))
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return taskTypes.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public ImageView img;

        public ItemViewHolder(View v) {
            super(v);
            img = v.findViewById(R.id.img);
        }
    }
}
