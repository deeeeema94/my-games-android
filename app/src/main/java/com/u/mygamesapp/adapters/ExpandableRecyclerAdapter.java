package com.u.mygamesapp.adapters;

import android.os.Bundle;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;

import com.u.mygamesapp.helpers.ChildViewHolder;
import com.u.mygamesapp.helpers.ParentViewHolder;
import com.u.mygamesapp.models.ExpandableWrapper;
import com.u.mygamesapp.models.Parent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class ExpandableRecyclerAdapter<P extends Parent<C>, C, PVH extends ParentViewHolder, CVH extends ChildViewHolder>
        extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String EXPANDED_STATE_MAP = "ExpandableRecyclerAdapter.ExpandedStateMap";
    /**
     * Default ViewType for parent rows
     */
    public static final int TYPE_PARENT = 0;
    /**
     * Default ViewType for children rows
     */
    public static final int TYPE_CHILD = 1;
    /**
     * Start of user-defined view types
     */
    public static final int TYPE_FIRST_USER = 2;
    private static final int INVALID_FLAT_POSITION = -1;

    /**
     * A {@link List} of all currently expanded parents and their children, in order.
     * Changes to this list should be made through the add/remove methods
     * available in {@link ExpandableRecyclerAdapter}.
     */
    @NonNull
    protected List<ExpandableWrapper<P, C>> mFlatItemList;

    @NonNull
    private List<P> mParentList;

    @Nullable
    private ExpandCollapseListener mExpandCollapseListener;

    @NonNull
    private List<RecyclerView> mAttachedRecyclerViewPool;

    private Map<P, Boolean> mExpansionStateMap;


    public interface ExpandCollapseListener {
        /**
         * Called when a parent is expanded.
         *
         * @param parentPosition The position of the parent in the list being expanded
         */
        @UiThread
        void onParentExpanded(int parentPosition);

        /**
         * Called when a parent is collapsed.
         *
         * @param parentPosition The position of the parent in the list being collapsed
         */
        @UiThread
        void onParentCollapsed(int parentPosition);
    }

    public ExpandableRecyclerAdapter(@NonNull List<P> parentList) {
        super();
        mParentList = parentList;
        mFlatItemList = generateFlattenedParentChildList(parentList);
        mAttachedRecyclerViewPool = new ArrayList<>();
        mExpansionStateMap = new HashMap<>(mParentList.size());
    }

    @NonNull
    @Override
    @UiThread
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (isParentViewType(viewType)) {
            PVH pvh = onCreateParentViewHolder(viewGroup, viewType);
            pvh.setParentViewHolderExpandCollapseListener(mParentViewHolderExpandCollapseListener);
            pvh.mExpandableAdapter = this;
            return pvh;
        } else {
            CVH cvh = onCreateChildViewHolder(viewGroup, viewType);
            cvh.mExpandableAdapter = this;
            return cvh;
        }
    }


    @Override
    @SuppressWarnings("unchecked")
    @UiThread
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int flatPosition) {
        if (flatPosition > mFlatItemList.size()) {
            throw new IllegalStateException("Trying to bind item out of bounds, size " + mFlatItemList.size()
                    + " flatPosition " + flatPosition + ". Was the data changed without a call to notify...()?");
        }

        ExpandableWrapper<P, C> listItem = mFlatItemList.get(flatPosition);
        if (listItem.isParent()) {
            PVH parentViewHolder = (PVH) holder;

            if (parentViewHolder.shouldItemViewClickToggleExpansion()) {
                parentViewHolder.setMainItemClickToExpand();
            }

            parentViewHolder.setExpanded(listItem.isExpanded());
            parentViewHolder.mParent = listItem.getParent();
            onBindParentViewHolder(parentViewHolder, getNearestParentPosition(flatPosition), listItem.getParent());
        } else {
            CVH childViewHolder = (CVH) holder;
            childViewHolder.mChild = listItem.getChild();
            onBindChildViewHolder(childViewHolder, getNearestParentPosition(flatPosition), getChildPosition(flatPosition), listItem.getChild());
        }
    }


    @NonNull
    @UiThread
    public abstract PVH onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType);

    /**
     * Callback called from {@link #onCreateViewHolder(ViewGroup, int)} when
     * the list item created is a child.
     *
     * @param childViewGroup The {@link ViewGroup} in the list for which a {@link CVH}
     *                       is being created
     * @return A {@code CVH} corresponding to the child with the {@code ViewGroup} childViewGroup
     */
    @NonNull
    @UiThread
    public abstract CVH onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType);

    /**
     * Callback called from onBindViewHolder(RecyclerView.ViewHolder, int)
     * when the list item bound to is a parent.
     * <p>
     * Bind data to the {@link PVH} here.
     *
     * @param parentViewHolder The {@code PVH} to bind data to
     * @param parentPosition The position of the parent to bind
     * @param parent The parent which holds the data to be bound to the {@code PVH}
     */
    @UiThread
    public abstract void onBindParentViewHolder(@NonNull PVH parentViewHolder, int parentPosition, @NonNull P parent);

    /**
     * Callback called from onBindViewHolder(RecyclerView.ViewHolder, int)
     * when the list item bound to is a child.
     * <p>
     * Bind data to the {@link CVH} here.
     *
     * @param childViewHolder The {@code CVH} to bind data to
     * @param parentPosition The position of the parent that contains the child to bind
     * @param childPosition The position of the child to bind
     * @param child The child which holds that data to be bound to the {@code CVH}
     */
    @UiThread
    public abstract void onBindChildViewHolder(@NonNull CVH childViewHolder, int parentPosition, int childPosition, @NonNull C child);

    /**
     * Gets the number of parents and children currently expanded.
     *
     * @return The size of {@link #mFlatItemList}
     */
    @Override
    @UiThread
    public int getItemCount() {
        return mFlatItemList.size();
    }

    /**
     * For multiple view type support look at overriding {@link #getParentViewType(int)} and
     * {@link #getChildViewType(int, int)}. Almost all cases should override those instead
     * of this method.
     *
     * @param flatPosition The index in the merged list of children and parents to get the view type of
     * @return Gets the view type of the item at the given flatPosition.
     */
    @Override
    @UiThread
    public int getItemViewType(int flatPosition) {
        ExpandableWrapper<P, C> listItem = mFlatItemList.get(flatPosition);
        if (listItem.isParent()) {
            return getParentViewType(getNearestParentPosition(flatPosition));
        } else {
            return getChildViewType(getNearestParentPosition(flatPosition), getChildPosition(flatPosition));
        }
    }


    public int getParentViewType(int parentPosition) {
        return TYPE_PARENT;
    }



    public int getChildViewType(int parentPosition, int childPosition) {
        return TYPE_CHILD;
    }


    public boolean isParentViewType(int viewType) {
        return viewType == TYPE_PARENT;
    }


    @NonNull
    @UiThread
    public List<P> getParentList() {
        return mParentList;
    }


    @UiThread
    public void setParentList(@NonNull List<P> parentList, boolean preserveExpansionState) {
        mParentList = parentList;
        notifyParentDataSetChanged(preserveExpansionState);
    }


    @Override
    @UiThread
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        mAttachedRecyclerViewPool.add(recyclerView);
    }



    @Override
    @UiThread
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        mAttachedRecyclerViewPool.remove(recyclerView);
    }

    @UiThread
    public void setExpandCollapseListener(@Nullable ExpandCollapseListener expandCollapseListener) {
        mExpandCollapseListener = expandCollapseListener;
    }


    @UiThread
    protected void parentExpandedFromViewHolder(int flatParentPosition) {
        ExpandableWrapper<P, C> parentWrapper = mFlatItemList.get(flatParentPosition);
        updateExpandedParent(parentWrapper, flatParentPosition, true);
    }


    @UiThread
    protected void parentCollapsedFromViewHolder(int flatParentPosition) {
        ExpandableWrapper<P, C> parentWrapper = mFlatItemList.get(flatParentPosition);
        updateCollapsedParent(parentWrapper, flatParentPosition, true);
    }

    private ParentViewHolder.ParentViewHolderExpandCollapseListener mParentViewHolderExpandCollapseListener = new ParentViewHolder.ParentViewHolderExpandCollapseListener() {


        @Override
        @UiThread
        public void onParentExpanded(int flatParentPosition) {
            parentExpandedFromViewHolder(flatParentPosition);
        }

        @Override
        @UiThread
        public void onParentCollapsed(int flatParentPosition) {
            parentCollapsedFromViewHolder(flatParentPosition);
        }
    };

    // region Programmatic Expansion/Collapsing


    @UiThread
    public void expandParent(@NonNull P parent) {
        ExpandableWrapper<P, C> parentWrapper = new ExpandableWrapper<>(parent);
        int flatParentPosition = mFlatItemList.indexOf(parentWrapper);
        if (flatParentPosition == INVALID_FLAT_POSITION) {
            return;
        }

        expandViews(mFlatItemList.get(flatParentPosition), flatParentPosition);
    }


    @UiThread
    public void expandParent(int parentPosition) {
        expandParent(mParentList.get(parentPosition));
    }


    @UiThread
    public void expandParentRange(int startParentPosition, int parentCount) {
        int endParentPosition = startParentPosition + parentCount;
        for (int i = startParentPosition; i < endParentPosition; i++) {
            expandParent(i);
        }
    }


    @UiThread
    public void expandAllParents() {
        for (P parent : mParentList) {
            expandParent(parent);
        }
    }


    @UiThread
    public void collapseParent(@NonNull P parent) {
        ExpandableWrapper<P, C> parentWrapper = new ExpandableWrapper<>(parent);
        int flatParentPosition = mFlatItemList.indexOf(parentWrapper);
        if (flatParentPosition == INVALID_FLAT_POSITION) {
            return;
        }

        collapseViews(mFlatItemList.get(flatParentPosition), flatParentPosition);
    }


    @UiThread
    public void collapseParent(int parentPosition) {
        collapseParent(mParentList.get(parentPosition));
    }


    @UiThread
    public void collapseParentRange(int startParentPosition, int parentCount) {
        int endParentPosition = startParentPosition + parentCount;
        for (int i = startParentPosition; i < endParentPosition; i++) {
            collapseParent(i);
        }
    }

    /**
     * Collapses all parents in the list.
     */
    @UiThread
    public void collapseAllParents() {
        for (P parent : mParentList) {
            collapseParent(parent);
        }
    }


    @UiThread
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        savedInstanceState.putSerializable(EXPANDED_STATE_MAP, generateExpandedStateMap());
    }


    @SuppressWarnings("unchecked")
    @UiThread
    public void onRestoreInstanceState(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState == null
                || !savedInstanceState.containsKey(EXPANDED_STATE_MAP)) {
            return;
        }

        HashMap<Integer, Boolean> expandedStateMap = (HashMap<Integer, Boolean>) savedInstanceState.getSerializable(EXPANDED_STATE_MAP);
        if (expandedStateMap == null) {
            return;
        }

        List<ExpandableWrapper<P, C>> itemList = new ArrayList<>();
        int parentsCount = mParentList.size();
        for (int i = 0; i < parentsCount; i++) {
            ExpandableWrapper<P, C> parentWrapper = new ExpandableWrapper<>(mParentList.get(i));
            itemList.add(parentWrapper);

            if (expandedStateMap.containsKey(i)) {
                boolean expanded = expandedStateMap.get(i);
                parentWrapper.setExpanded(expanded);

                if (expanded) {
                    List<ExpandableWrapper<P, C>> wrappedChildList = parentWrapper.getWrappedChildList();
                    int childrenCount = wrappedChildList.size();
                    for (int j = 0; j < childrenCount; j++) {
                        ExpandableWrapper<P, C> childWrapper = wrappedChildList.get(j);
                        itemList.add(childWrapper);
                    }
                }
            }
        }

        mFlatItemList = itemList;

        notifyDataSetChanged();
    }


    @SuppressWarnings("unchecked")
    @UiThread
    private void expandViews(@NonNull ExpandableWrapper<P, C> parentWrapper, int flatParentPosition) {
        PVH viewHolder;
        for (RecyclerView recyclerView : mAttachedRecyclerViewPool) {
            viewHolder = (PVH) recyclerView.findViewHolderForAdapterPosition(flatParentPosition);
            if (viewHolder != null && !viewHolder.isExpanded()) {
                viewHolder.setExpanded(true);
                viewHolder.onExpansionToggled(false);
            }
        }

        updateExpandedParent(parentWrapper, flatParentPosition, false);
    }


    @SuppressWarnings("unchecked")
    @UiThread
    private void collapseViews(@NonNull ExpandableWrapper<P, C> parentWrapper, int flatParentPosition) {
        PVH viewHolder;
        for (RecyclerView recyclerView : mAttachedRecyclerViewPool) {
            viewHolder = (PVH) recyclerView.findViewHolderForAdapterPosition(flatParentPosition);
            if (viewHolder != null && viewHolder.isExpanded()) {
                viewHolder.setExpanded(false);
                viewHolder.onExpansionToggled(true);
            }
        }

        updateCollapsedParent(parentWrapper, flatParentPosition, false);
    }


    @UiThread
    private void updateExpandedParent(@NonNull ExpandableWrapper<P, C> parentWrapper, int flatParentPosition, boolean expansionTriggeredByListItemClick) {
        if (parentWrapper.isExpanded()) {
            return;
        }

        parentWrapper.setExpanded(true);
        mExpansionStateMap.put(parentWrapper.getParent(), true);

        List<ExpandableWrapper<P, C>> wrappedChildList = parentWrapper.getWrappedChildList();
        if (wrappedChildList != null) {
            int childCount = wrappedChildList.size();
            for (int i = 0; i < childCount; i++) {
                mFlatItemList.add(flatParentPosition + i + 1, wrappedChildList.get(i));
            }

            notifyItemRangeInserted(flatParentPosition + 1, childCount);
        }

        if (expansionTriggeredByListItemClick && mExpandCollapseListener != null) {
            mExpandCollapseListener.onParentExpanded(getNearestParentPosition(flatParentPosition));
        }
    }


    @UiThread
    private void updateCollapsedParent(@NonNull ExpandableWrapper<P, C> parentWrapper, int flatParentPosition, boolean collapseTriggeredByListItemClick) {
        if (!parentWrapper.isExpanded()) {
            return;
        }

        parentWrapper.setExpanded(false);
        mExpansionStateMap.put(parentWrapper.getParent(), false);

        List<ExpandableWrapper<P, C>> wrappedChildList = parentWrapper.getWrappedChildList();
        if (wrappedChildList != null) {
            int childCount = wrappedChildList.size();
            for (int i = childCount - 1; i >= 0; i--) {
                mFlatItemList.remove(flatParentPosition + i + 1);
            }

            notifyItemRangeRemoved(flatParentPosition + 1, childCount);
        }

        if (collapseTriggeredByListItemClick && mExpandCollapseListener != null) {
            mExpandCollapseListener.onParentCollapsed(getNearestParentPosition(flatParentPosition));
        }
    }


    @UiThread
    public int getNearestParentPosition(int flatPosition) {
        if (flatPosition == 0) {
            return 0;
        }

        int parentCount = -1;
        for (int i = 0; i <= flatPosition; i++) {
            ExpandableWrapper<P, C> listItem = mFlatItemList.get(i);
            if (listItem.isParent()) {
                parentCount++;
            }
        }
        return parentCount;
    }


    @UiThread
    public int getChildPosition(int flatPosition) {
        if (flatPosition == 0) {
            return 0;
        }

        int childCount = 0;
        for (int i = 0; i < flatPosition; i++) {
            ExpandableWrapper<P, C> listItem = mFlatItemList.get(i);
            if (listItem.isParent()) {
                childCount = 0;
            } else {
                childCount++;
            }
        }
        return childCount;
    }

    // endregion

    // region Data Manipulation


    @UiThread
    public void notifyParentDataSetChanged(boolean preserveExpansionState) {
        if (preserveExpansionState) {
            mFlatItemList = generateFlattenedParentChildList(mParentList, mExpansionStateMap);
        } else {
            mFlatItemList = generateFlattenedParentChildList(mParentList);
        }
        notifyDataSetChanged();
    }


    @UiThread
    public void notifyParentInserted(int parentPosition) {
        P parent = mParentList.get(parentPosition);

        int flatParentPosition;
        if (parentPosition < mParentList.size() - 1) {
            flatParentPosition = getFlatParentPosition(parentPosition);
        } else {
            flatParentPosition = mFlatItemList.size();
        }

        int sizeChanged = addParentWrapper(flatParentPosition, parent);
        notifyItemRangeInserted(flatParentPosition, sizeChanged);
    }


    @UiThread
    public void notifyParentRangeInserted(int parentPositionStart, int itemCount) {
        int initialFlatParentPosition;
        if (parentPositionStart < mParentList.size() - itemCount) {
            initialFlatParentPosition = getFlatParentPosition(parentPositionStart);
        } else {
            initialFlatParentPosition = mFlatItemList.size();
        }

        int sizeChanged = 0;
        int flatParentPosition = initialFlatParentPosition;
        int changed;
        int parentPositionEnd = parentPositionStart + itemCount;
        for (int i = parentPositionStart; i < parentPositionEnd; i++) {
            P parent = mParentList.get(i);
            changed = addParentWrapper(flatParentPosition, parent);
            flatParentPosition += changed;
            sizeChanged += changed;
        }

        notifyItemRangeInserted(initialFlatParentPosition, sizeChanged);
    }

    @UiThread
    private int addParentWrapper(int flatParentPosition, P parent) {
        int sizeChanged = 1;
        ExpandableWrapper<P, C> parentWrapper = new ExpandableWrapper<>(parent);
        mFlatItemList.add(flatParentPosition, parentWrapper);
        if (parentWrapper.isParentInitiallyExpanded()) {
            parentWrapper.setExpanded(true);
            List<ExpandableWrapper<P, C>> wrappedChildList = parentWrapper.getWrappedChildList();
            mFlatItemList.addAll(flatParentPosition + sizeChanged, wrappedChildList);
            sizeChanged += wrappedChildList.size();
        }
        return sizeChanged;
    }


    @UiThread
    public void notifyParentRemoved(int parentPosition) {
        int flatParentPosition = getFlatParentPosition(parentPosition);
        int sizeChanged = removeParentWrapper(flatParentPosition);

        notifyItemRangeRemoved(flatParentPosition, sizeChanged);
    }


    public void notifyParentRangeRemoved(int parentPositionStart, int itemCount) {
        int sizeChanged = 0;
        int flatParentPositionStart = getFlatParentPosition(parentPositionStart);
        for (int i = 0; i < itemCount; i++) {
            sizeChanged += removeParentWrapper(flatParentPositionStart);
        }

        notifyItemRangeRemoved(flatParentPositionStart, sizeChanged);
    }

    @UiThread
    private int removeParentWrapper(int flatParentPosition) {
        int sizeChanged = 1;
        ExpandableWrapper<P, C> parentWrapper = mFlatItemList.remove(flatParentPosition);
        if (parentWrapper.isExpanded()) {
            int childListSize = parentWrapper.getWrappedChildList().size();
            for (int i = 0; i < childListSize; i++) {
                mFlatItemList.remove(flatParentPosition);
                sizeChanged++;
            }
        }
        return sizeChanged;
    }

    @UiThread
    public void notifyParentChanged(int parentPosition) {
        P parent = mParentList.get(parentPosition);
        int flatParentPositionStart = getFlatParentPosition(parentPosition);
        int sizeChanged = changeParentWrapper(flatParentPositionStart, parent);

        notifyItemRangeChanged(flatParentPositionStart, sizeChanged);
    }


    @UiThread
    public void notifyParentRangeChanged(int parentPositionStart, int itemCount) {
        int flatParentPositionStart = getFlatParentPosition(parentPositionStart);

        int flatParentPosition = flatParentPositionStart;
        int sizeChanged = 0;
        int changed;
        P parent;
        for (int j = 0; j < itemCount; j++) {
            parent = mParentList.get(parentPositionStart);
            changed = changeParentWrapper(flatParentPosition, parent);
            sizeChanged += changed;
            flatParentPosition += changed;
            parentPositionStart++;
        }
        notifyItemRangeChanged(flatParentPositionStart, sizeChanged);
    }

    private int changeParentWrapper(int flatParentPosition, P parent) {
        ExpandableWrapper<P, C> parentWrapper = mFlatItemList.get(flatParentPosition);
        parentWrapper.setParent(parent);
        int sizeChanged = 1;
        if (parentWrapper.isExpanded()) {
            List<ExpandableWrapper<P, C>> wrappedChildList = parentWrapper.getWrappedChildList();
            int childSize = wrappedChildList.size();
            for (int i = 0; i < childSize; i++) {
                mFlatItemList.set(flatParentPosition + i + 1, wrappedChildList.get(i));
                sizeChanged++;
            }
        }

        return sizeChanged;
    }

    @UiThread
    public void notifyParentMoved(int fromParentPosition, int toParentPosition) {
        int fromFlatParentPosition = getFlatParentPosition(fromParentPosition);
        ExpandableWrapper<P, C> fromParentWrapper = mFlatItemList.get(fromFlatParentPosition);

        // If the parent is collapsed we can take advantage of notifyItemMoved otherwise
        // we are forced to do a "manual" move by removing and then adding the parent + children
        // (no notifyItemRangeMovedAvailable)
        boolean isCollapsed = !fromParentWrapper.isExpanded();
        boolean isExpandedNoChildren = !isCollapsed && (fromParentWrapper.getWrappedChildList().size() == 0);
        if (isCollapsed || isExpandedNoChildren) {
            int toFlatParentPosition = getFlatParentPosition(toParentPosition);
            ExpandableWrapper<P, C> toParentWrapper = mFlatItemList.get(toFlatParentPosition);
            mFlatItemList.remove(fromFlatParentPosition);
            int childOffset = 0;
            if (toParentWrapper.isExpanded()) {
                childOffset = toParentWrapper.getWrappedChildList().size();
            }
            mFlatItemList.add(toFlatParentPosition + childOffset, fromParentWrapper);

            notifyItemMoved(fromFlatParentPosition, toFlatParentPosition + childOffset);
        } else {
            // Remove the parent and children
            int sizeChanged = 0;
            int childListSize = fromParentWrapper.getWrappedChildList().size();
            for (int i = 0; i < childListSize + 1; i++) {
                mFlatItemList.remove(fromFlatParentPosition);
                sizeChanged++;
            }
            notifyItemRangeRemoved(fromFlatParentPosition, sizeChanged);


            // Add the parent and children at new position
            int toFlatParentPosition = getFlatParentPosition(toParentPosition);
            int childOffset = 0;
            if (toFlatParentPosition != INVALID_FLAT_POSITION) {
                ExpandableWrapper<P, C> toParentWrapper = mFlatItemList.get(toFlatParentPosition);
                if (toParentWrapper.isExpanded()) {
                    childOffset = toParentWrapper.getWrappedChildList().size();
                }
            } else {
                toFlatParentPosition = mFlatItemList.size();
            }

            mFlatItemList.add(toFlatParentPosition + childOffset, fromParentWrapper);
            List<ExpandableWrapper<P, C>> wrappedChildList = fromParentWrapper.getWrappedChildList();
            sizeChanged = wrappedChildList.size() + 1;

            mFlatItemList.addAll(toFlatParentPosition + childOffset + 1, wrappedChildList);

            notifyItemRangeInserted(toFlatParentPosition + childOffset, sizeChanged);
        }
    }

    @UiThread
    public void notifyChildInserted(int parentPosition, int childPosition) {
        int flatParentPosition = getFlatParentPosition(parentPosition);
        ExpandableWrapper<P, C> parentWrapper = mFlatItemList.get(flatParentPosition);

        parentWrapper.setParent(mParentList.get(parentPosition));
        if (parentWrapper.isExpanded()) {
            ExpandableWrapper<P, C> child = parentWrapper.getWrappedChildList().get(childPosition);
            mFlatItemList.add(flatParentPosition + childPosition + 1, child);
            notifyItemInserted(flatParentPosition + childPosition + 1);
        }
    }


    @UiThread
    public void notifyChildRangeInserted(int parentPosition, int childPositionStart, int itemCount) {
        int flatParentPosition = getFlatParentPosition(parentPosition);
        ExpandableWrapper<P, C> parentWrapper = mFlatItemList.get(flatParentPosition);

        parentWrapper.setParent(mParentList.get(parentPosition));
        if (parentWrapper.isExpanded()) {
            List<ExpandableWrapper<P, C>> wrappedChildList = parentWrapper.getWrappedChildList();
            for (int i = 0; i < itemCount; i++) {
                ExpandableWrapper<P, C> child = wrappedChildList.get(childPositionStart + i);
                mFlatItemList.add(flatParentPosition + childPositionStart + i + 1, child);
            }
            notifyItemRangeInserted(flatParentPosition + childPositionStart + 1, itemCount);
        }
    }

    @UiThread
    public void notifyChildRemoved(int parentPosition, int childPosition) {
        int flatParentPosition = getFlatParentPosition(parentPosition);
        ExpandableWrapper<P, C> parentWrapper = mFlatItemList.get(flatParentPosition);
        parentWrapper.setParent(mParentList.get(parentPosition));

        if (parentWrapper.isExpanded()) {
            mFlatItemList.remove(flatParentPosition + childPosition + 1);
            notifyItemRemoved(flatParentPosition + childPosition + 1);
        }
    }

    @UiThread
    public void notifyChildRangeRemoved(int parentPosition, int childPositionStart, int itemCount) {
        int flatParentPosition = getFlatParentPosition(parentPosition);
        ExpandableWrapper<P, C> parentWrapper = mFlatItemList.get(flatParentPosition);
        parentWrapper.setParent(mParentList.get(parentPosition));

        if (parentWrapper.isExpanded()) {
            for (int i = 0; i < itemCount; i++) {
                mFlatItemList.remove(flatParentPosition + childPositionStart + 1);
            }
            notifyItemRangeRemoved(flatParentPosition + childPositionStart + 1, itemCount);
        }
    }


    @UiThread
    public void notifyChildChanged(int parentPosition, int childPosition) {
        P parent = mParentList.get(parentPosition);
        int flatParentPosition = getFlatParentPosition(parentPosition);
        ExpandableWrapper<P, C> parentWrapper = mFlatItemList.get(flatParentPosition);
        parentWrapper.setParent(parent);
        if (parentWrapper.isExpanded()) {
            int flatChildPosition = flatParentPosition + childPosition + 1;
            ExpandableWrapper<P, C> child = parentWrapper.getWrappedChildList().get(childPosition);
            mFlatItemList.set(flatChildPosition, child);
            notifyItemChanged(flatChildPosition);
        }
    }

    @UiThread
    public void notifyChildRangeChanged(int parentPosition, int childPositionStart, int itemCount) {
        P parent = mParentList.get(parentPosition);
        int flatParentPosition = getFlatParentPosition(parentPosition);
        ExpandableWrapper<P, C> parentWrapper = mFlatItemList.get(flatParentPosition);
        parentWrapper.setParent(parent);
        if (parentWrapper.isExpanded()) {
            int flatChildPosition = flatParentPosition + childPositionStart + 1;
            for (int i = 0; i < itemCount; i++) {
                ExpandableWrapper<P, C> child
                        = parentWrapper.getWrappedChildList().get(childPositionStart + i);
                mFlatItemList.set(flatChildPosition + i, child);
            }
            notifyItemRangeChanged(flatChildPosition, itemCount);
        }
    }

    @UiThread
    public void notifyChildMoved(int parentPosition, int fromChildPosition, int toChildPosition) {
        P parent = mParentList.get(parentPosition);
        int flatParentPosition = getFlatParentPosition(parentPosition);

        ExpandableWrapper<P, C> parentWrapper = mFlatItemList.get(flatParentPosition);
        parentWrapper.setParent(parent);
        if (parentWrapper.isExpanded()) {
            ExpandableWrapper<P, C> fromChild = mFlatItemList.remove(flatParentPosition + 1 + fromChildPosition);
            mFlatItemList.add(flatParentPosition + 1 + toChildPosition, fromChild);
            notifyItemMoved(flatParentPosition + 1 + fromChildPosition, flatParentPosition + 1 + toChildPosition);
        }
    }


    private List<ExpandableWrapper<P, C>> generateFlattenedParentChildList(List<P> parentList) {
        List<ExpandableWrapper<P, C>> flatItemList = new ArrayList<>();

        int parentCount = parentList.size();
        for (int i = 0; i < parentCount; i++) {
            P parent = parentList.get(i);
            generateParentWrapper(flatItemList, parent, parent.isInitiallyExpanded());
        }

        return flatItemList;
    }

    private List<ExpandableWrapper<P, C>> generateFlattenedParentChildList(List<P> parentList, Map<P, Boolean> savedLastExpansionState) {
        List<ExpandableWrapper<P, C>> flatItemList = new ArrayList<>();

        int parentCount = parentList.size();
        for (int i = 0; i < parentCount; i++) {
            P parent = parentList.get(i);
            Boolean lastExpandedState = savedLastExpansionState.get(parent);
            boolean shouldExpand = lastExpandedState == null ? parent.isInitiallyExpanded() : lastExpandedState;

            generateParentWrapper(flatItemList, parent, shouldExpand);
        }

        return flatItemList;
    }

    private void generateParentWrapper(List<ExpandableWrapper<P, C>> flatItemList, P parent, boolean shouldExpand) {
        ExpandableWrapper<P, C> parentWrapper = new ExpandableWrapper<>(parent);
        flatItemList.add(parentWrapper);
        if (shouldExpand) {
            generateExpandedChildren(flatItemList, parentWrapper);
        }
    }

    private void generateExpandedChildren(List<ExpandableWrapper<P, C>> flatItemList, ExpandableWrapper<P, C> parentWrapper) {
        parentWrapper.setExpanded(true);

        List<ExpandableWrapper<P, C>> wrappedChildList = parentWrapper.getWrappedChildList();
        int childCount = wrappedChildList.size();
        for (int j = 0; j < childCount; j++) {
            ExpandableWrapper<P, C> childWrapper = wrappedChildList.get(j);
            flatItemList.add(childWrapper);
        }
    }

    @NonNull
    @UiThread
    private HashMap<Integer, Boolean> generateExpandedStateMap() {
        HashMap<Integer, Boolean> parentHashMap = new HashMap<>();
        int childCount = 0;

        int listItemCount = mFlatItemList.size();
        for (int i = 0; i < listItemCount; i++) {
            if (mFlatItemList.get(i) != null) {
                ExpandableWrapper<P, C> listItem = mFlatItemList.get(i);
                if (listItem.isParent()) {
                    parentHashMap.put(i - childCount, listItem.isExpanded());
                } else {
                    childCount++;
                }
            }
        }

        return parentHashMap;
    }

    @UiThread
    private int getFlatParentPosition(int parentPosition) {
        int parentCount = 0;
        int listItemCount = mFlatItemList.size();
        for (int i = 0; i < listItemCount; i++) {
            if (mFlatItemList.get(i).isParent()) {
                parentCount++;

                if (parentCount > parentPosition) {
                    return i;
                }
            }
        }

        return INVALID_FLAT_POSITION;
    }
}

