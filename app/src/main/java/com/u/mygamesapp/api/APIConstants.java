package com.u.mygamesapp.api;

public class APIConstants {

    public static final String API_BASE_URL = "http://www.gamesapi.somee.com/api/";
    public static final String API_IMAGES_URL = "http://www.gamesadmin.somee.com";

    public static final String UPLOAD_IMAGE = "/api/APIImages/SaveMedia";

    public static final String LOGIN = "Authenticate/Login";
    public static final String FORGET_PASSWORD = "Authenticate/ForgotPassword";
    public static final String RESET_PASS = "Authenticate/ResetPassword";
    public static final String CHANGE_PASSWORD = "Authenticate/ChangePassword";
    public static final String VERIFY = "Authenticate/Verify";
    public static final String RESEND_CODE = "Authenticate/ResendVerifyCode";
    public static final String REGISTER = "Authenticate/Register";
    public static final String GET_PROFILE = "Authenticate/GetProfile";
    public static final String UPDATE_PROFILE = "Authenticate/UpdateProfile";
    public static final String SET_FACEBOOK_DATA = "Authenticate/SetFacebookData";
    public static final String SET_FCM_TOKEN = "Authenticate/SetFCMToken";

    public static final String TASKS = "AppTasks/";
    public static final String PRIZES = "Prizes/";
    public static final String QUESTIONS = "Questions/";
    public static final String SPORT_MATCHES = "SportMatches/";
    public static final String VERSION = "MobileVersion/";
    public static final String CONTACT_US = "ContactUs/";
    public static final String CITIES = "Cities/";
    public static final String LUCKY_WHEEL = "LuckyWheels/";
    public static final String DAILY_BONUS = "DailyBonusess/";
    public static final String CUSTOMER_PRIZES = "CustomerPrizes/";
    public static final String CUSTOMER_TASKS = "CustomerTasks/";
    public static final String CUSTOMER_MESSAGES = "CustomerMessages/";
    public static final String GROUPS = "Groups/";

    public static final String GET_ALL = "GetAll";
    public static final String GET_ONE = "Get";
    public static final String ADD = "Add";
    public static final String ADD_LUCKY = "AddLucky";
    public static final String ADD_DAILY_BONUS = "AddDailyBonus";
    public static final String ADD_START = "AddStart";
    public static final String REQUEST_PRIZE = "RequestPrize";
    public static final String ADD_END = "AddEnd";
    public static final String ADD_SHARE_APP_END = "AddShareAppEnd";


    public static final String HOME_PAGE = "Home/GetHome";
    public static final String CONDITIONS = "Conditions/GetAll";
    public static final String INTRO_IMAGES = "Contents/GetIntro";
    public static final String INTRO_VIDEO = "Contents/GetIntroVideo";
    public static final String ABOUT_US = "Contents/AboutUs";
    public static final String CONTACT = "Contacts/";
}
