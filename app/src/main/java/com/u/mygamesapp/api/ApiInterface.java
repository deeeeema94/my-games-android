package com.u.mygamesapp.api;

import com.u.mygamesapp.models.Customer;
import com.u.mygamesapp.responsemodels.APIResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

public interface ApiInterface {

    @Multipart
    @POST
    Call<APIResponse<Customer>> uploadImage(@Url String url, @Part MultipartBody.Part filePart);

}
