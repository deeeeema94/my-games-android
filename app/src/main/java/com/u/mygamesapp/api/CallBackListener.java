package com.u.mygamesapp.api;

public interface CallBackListener {

    void onFinish(String response);

    void onProgress(int process);

}
