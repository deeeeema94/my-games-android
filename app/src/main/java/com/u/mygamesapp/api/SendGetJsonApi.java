package com.u.mygamesapp.api;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;

import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.GodFatherActivity;
import com.u.mygamesapp.activities.authentication.LoginActivity;
import com.u.mygamesapp.helpers.CustomDialog;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import static com.u.mygamesapp.api.APIConstants.API_BASE_URL;
import static com.u.mygamesapp.helpers.Constants.DIALOG_TYPE_SPINNER;
import static com.u.mygamesapp.helpers.Constants.ENGLISH_LANG;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_LANG;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_TOKEN;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.clearUserPreferences;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readFromPreferences;

public class SendGetJsonApi  {

    private static String ApiUrl;
    private static String JsonSend;
    private static CallBackListener callBackListener;
    private static HttpURLConnection urlConnection;
    private static Context context;
    public static CustomDialog cdd;
    private static boolean isWithProgress = true;

    private static String WebSite()
    {
        return API_BASE_URL;
    }

    public SendGetJsonApi (Context context, String apiUrl, String jsonSend, CallBackListener callBackListener) {
        new SendGetJsonApi(context, apiUrl, jsonSend, callBackListener, true);
    }

    public SendGetJsonApi (Context mContext, String mApiUrl, String mJsonSend, CallBackListener mCallBackListener, boolean mIsWithProgress) {
        context = mContext;
        ApiUrl = mApiUrl;
        JsonSend = mJsonSend;
        callBackListener = mCallBackListener;
        isWithProgress = mIsWithProgress;
    }

    public void Execute()
    {
        asyncTask atask = new asyncTask();
        atask.execute();
    }

    private static class asyncTask extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (isWithProgress){
                cdd = new CustomDialog(context, DIALOG_TYPE_SPINNER);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    Objects.requireNonNull(cdd.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                }
                cdd.setCancelable(false);
                cdd.show();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            StringBuilder builder = new StringBuilder();
            String link = WebSite() + ApiUrl;
            try {
                URL url = new URL(link);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                int connectTimeout = 15000;
                urlConnection.setConnectTimeout(connectTimeout);
                int readTimeout = 30000;
                urlConnection.setReadTimeout(readTimeout);

                String token = readFromPreferences(context, DEFAULT_FILE_NAME, PREF_TOKEN,"");
                if(!token.equals("")) {
                    String auth = "Bearer " + token;
                    urlConnection.setRequestProperty("Authorization", auth);
                }

                String lang = readFromPreferences(context, DEFAULT_FILE_NAME, PREF_LANG, ENGLISH_LANG);
                urlConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                urlConnection.setRequestProperty("Accept-Language" , lang);
                urlConnection.setRequestProperty("platform" , "Android");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestProperty("Cache-Control", "max-age=640000");

                urlConnection.setRequestProperty("app-version" , GodFatherActivity.appVersion);
                urlConnection.setRequestProperty("os-version" , GodFatherActivity.osVersion);
                urlConnection.setRequestProperty("mobile-brand" , GodFatherActivity.mobileBrand);
                urlConnection.setRequestProperty("device_id" , GodFatherActivity.deviceId);

                urlConnection.setRequestMethod("POST");
                urlConnection.connect();
                OutputStream os = urlConnection.getOutputStream();
                OutputStreamWriter osw = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    osw = new OutputStreamWriter(os, StandardCharsets.UTF_8);
                }
                if (osw != null) {
                    osw.write(JsonSend);
                }
                if (osw != null) {
                    osw.flush();
                }
                if (osw != null) {
                    osw.close();
                }
                InputStream in;
                int status = urlConnection.getResponseCode();
                if (status == 200) {
                    in = new BufferedInputStream(urlConnection.getInputStream());
                }
                else if(status == 401){
                    in = urlConnection.getErrorStream();
                    logOut();
                }
                else {
                    in = urlConnection.getErrorStream();
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
            } catch (Exception e) {
                return context.getString(R.string.went_wrong);
            } finally {
                urlConnection.disconnect();
            }
            return builder.toString();
        }

        @Override
        protected void onPostExecute(String resultjson)
        {
            if(isWithProgress){
                cdd.dismiss();
            }
            if ( context instanceof Activity) {
                Activity activity = (Activity)context;
                if ( activity.isFinishing() ) {
                    return;
                }
            }
            callBackListener.onFinish(resultjson);
        }

        @Override
        protected void onProgressUpdate(Integer... values)
        {
            if (context instanceof Activity) {
                Activity activity = (Activity)context;
                if ( activity.isFinishing() ) {
                    return;
                }
            }
            callBackListener.onProgress(values[0]);
        }

        private void logOut() {
            clearUserPreferences(context, DEFAULT_FILE_NAME);
            Intent intent = new Intent(context, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
            ((Activity)context).finish();
        }
    }
}
