package com.u.mygamesapp.fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.AboutUsActivity;
import com.u.mygamesapp.activities.BaseActivity;
import com.u.mygamesapp.activities.ContactsActivity;
import com.u.mygamesapp.activities.FAQActivity;
import com.u.mygamesapp.activities.FullScreenVideo;
import com.u.mygamesapp.activities.SendContactUsActivity;

import java.util.Objects;

import static com.u.mygamesapp.activities.GodFatherActivity.currLang;
import static com.u.mygamesapp.helpers.Constants.ARABIC_LANG;
import static com.u.mygamesapp.helpers.Constants.ENGLISH_LANG;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_LANG;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.saveToPreferences;

public class MoreFragment extends Fragment {

    public static String MORE_FRAGMENT_TAG  = MoreFragment.class.getSimpleName();

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_more, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(R.string.more);
        BaseActivity.pointsLayout.setVisibility(View.GONE);

        TextView langTxt = view.findViewById(R.id.change_lang_txt);
        langTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view13) {
                MoreFragment.this.changeLanguage();
            }
        });

        TextView accountTxt = view.findViewById(R.id.my_account_txt);
        accountTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view13) {
                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new AccountFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });

        TextView aboutTxt = view.findViewById(R.id.about_us_txt);
        aboutTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view13) {
                Intent aboutIntent = new Intent(getActivity(), AboutUsActivity.class);
                Objects.requireNonNull(getActivity()).startActivity(aboutIntent);
            }
        });

        TextView contactTxt = view.findViewById(R.id.contact_us_txt);
        contactTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view13) {
                Intent contactIntent = new Intent(getActivity(), SendContactUsActivity.class);
                Objects.requireNonNull(getActivity()).startActivity(contactIntent);
            }
        });

        TextView contactsTxt = view.findViewById(R.id.contacts_txt);
        contactsTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view13) {
                Intent contactsIntent = new Intent(getActivity(), ContactsActivity.class);
                Objects.requireNonNull(getActivity()).startActivity(contactsIntent);
            }
        });

        TextView FAQsTxt = view.findViewById(R.id.faqs_txt);
        FAQsTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view13) {
                Intent contactsIntent = new Intent(getActivity(), FAQActivity.class);
                Objects.requireNonNull(getActivity()).startActivity(contactsIntent);
            }
        });

        TextView help_txt = view.findViewById(R.id.help_txt);
        help_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view13) {
                Intent videoIntent = new Intent(getActivity(), FullScreenVideo.class);
                videoIntent.putExtra("isWatch", true);
                Objects.requireNonNull(getActivity()).startActivity(videoIntent);
            }
        });
    }

    public void changeLanguage()
    {
        if (currLang.equals(ARABIC_LANG))
        {
            currLang = ENGLISH_LANG;
        }
        else
        {
            currLang = ARABIC_LANG;
        }
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_LANG, currLang);
        getActivity().finish();
        getActivity().startActivity(new Intent(getActivity(), BaseActivity.class));
    }

}
