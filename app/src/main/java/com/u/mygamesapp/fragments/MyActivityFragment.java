package com.u.mygamesapp.fragments;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;

import java.util.Objects;

import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_MESSAGES_COUNT;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readFromPreferences;

public class MyActivityFragment extends Fragment {

    public static String MY_ACTIVITY_FRAGMENT_TAG  = MyActivityFragment.class.getSimpleName();

    private ImageView inbox, prizesLog, tasksLog;
    private TextView messageCount;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_activity, container, false);
    }

    @SuppressLint("DefaultLocale")
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BaseActivity.pointsLayout.setVisibility(View.GONE);

        inbox = view.findViewById(R.id.inbox_tab);
        prizesLog = view.findViewById(R.id.prizes_log_tab);
        tasksLog = view.findViewById(R.id.tasks_log_tab);
        messageCount = view.findViewById(R.id.message_count);

        int messagesCount = Integer.parseInt(readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_MESSAGES_COUNT, "0"));
        if(messagesCount > 0){
            messageCount.setVisibility(View.VISIBLE);
            messageCount.setText(String.format("%d", messagesCount));
        }
        else {
            messageCount.setVisibility(View.INVISIBLE);
        }

        inbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new InboxFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });

        prizesLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new PrizesHistoryFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });

        tasksLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new TasksHistoryFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });
    }
}
