package com.u.mygamesapp.fragments;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.lukedeighton.wheelview.WheelView;
import com.lukedeighton.wheelview.adapter.WheelArrayAdapter;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.helpers.CustomDialog;
import com.u.mygamesapp.helpers.JsonParser;
import com.u.mygamesapp.helpers.TextDrawable;
import com.u.mygamesapp.models.CustomerPrize;
import com.u.mygamesapp.models.LuckyWheel;
import com.u.mygamesapp.models.Prize;
import com.u.mygamesapp.requestmodels.PaginationRequest;
import com.u.mygamesapp.responsemodels.APIResponse;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import static com.u.mygamesapp.api.APIConstants.ADD_LUCKY;
import static com.u.mygamesapp.api.APIConstants.API_IMAGES_URL;
import static com.u.mygamesapp.api.APIConstants.CUSTOMER_PRIZES;
import static com.u.mygamesapp.api.APIConstants.GET_ONE;
import static com.u.mygamesapp.api.APIConstants.LUCKY_WHEEL;
import static com.u.mygamesapp.helpers.Constants.DIALOG_TYPE_PRIZE;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_IS_LUCKY_VALID;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readFromPreferences;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.saveToPreferences;

public class LuckyWheelFragment extends Fragment {

//    List<LuckyItem> luckyItems = new ArrayList<>();
    LuckyWheel luckyWheel = new LuckyWheel();
    private static Prize[] prizes;
    private static List<Prize> validPrizes;
//    private LuckyWheelView luckyWheelView;
    private boolean isLuckyValid;
    private Button spinBtn;
    private Gson gson = new Gson();
    private TextView comeTxt;
    private RelativeLayout luckyLayout;
    private WheelView wheelView;
    private LottieAnimationView frame;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lucky_wheel, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.daily_bonus));
        BaseActivity.pointsLayout.setVisibility(View.GONE);

        initLayout(view);

        getPrizes();
    }

    private void initLayout(View v) {
//        luckyWheelView = v.findViewById(R.id.luckyWheel);
        wheelView = v.findViewById(R.id.wheel_view);
        spinBtn = v.findViewById(R.id.play);
        comeTxt = v.findViewById(R.id.come_txt);
        luckyLayout = v.findViewById(R.id.lucky_layout);
        frame = v.findViewById(R.id.frame);
        frame.playAnimation();
    }

    private void setPrizesToUI(){
        isLuckyValid = Boolean.parseBoolean(readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_IS_LUCKY_VALID, "False"));
        if(!isLuckyValid){
            comeTxt.setVisibility(View.VISIBLE);
            spinBtn.setEnabled(false);
            luckyLayout.setAlpha(0.5F);
        }
        else{
            comeTxt.setVisibility(View.GONE);
            spinBtn.setEnabled(true);
            luckyLayout.setAlpha(1F);
//            luckyWheelView.setLuckyRoundItemSelectedListener(new LuckyWheelView.LuckyRoundItemSelectedListener() {
//                @Override
//                public void LuckyRoundItemSelected(int index) {
//                    GetPrizeBtnAction(luckyItems.get(index).Id);
//                }
//            });
            spinBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    wheelView.setAdapter(new MaterialColorAdapter(new LinkedList<Prize>()));
                    wheelView.setMidSelected();
                    frame.cancelAnimation();
                }
            });
        }
//        for (int i = 0; i < prizes.length; i++){
//            LuckyItem luckyItem1 = new LuckyItem();
//            luckyItem1.Id = prizes[i].getId();
//            luckyItem1.topText = prizes[i].getDisplayName();
//            luckyItem1.color = 0xffFFF3E0;
//            luckyItems.add(luckyItem1);
//        }
//        luckyWheelView.setData(luckyItems);
//        luckyWheelView.setRound(5);
//
//        luckyWheelView.setLuckyWheelBackgrouldColor(0xffEB3349);
//        luckyWheelView.setLuckyWheelTextColor(0xff000000);
//        luckyWheelView.setLuckyWheelCenterImage(getResources().getDrawable(R.drawable.icon));
//        luckyWheelView.setLuckyWheelCursorImage(R.drawable.ic_cursor);
    }

//    private int getRandomIndex() {
//        Random rand = new Random();
//        return rand.nextInt(luckyItems.size() - 1) + 0;
//    }
//
//    private int getRandomRound() {
//        Random rand = new Random();
//        return rand.nextInt(10) + 15;
//    }

    private void getPrizes(){
        PaginationRequest paginationRequest = new PaginationRequest(1, 100);
        String json = gson.toJson(paginationRequest);
        new SendGetJsonApi(getActivity(), LUCKY_WHEEL + GET_ONE, json,
                new CallBackListener() {
                    @Override
                    public void onFinish(String result)
                    {
                        APIResponse<LuckyWheel> apiResponse = JsonParser.getResponse(result, LuckyWheel.class);
                        try {
                            if (apiResponse != null && apiResponse.getResult().equals("success")) {
                                luckyWheel = apiResponse.getContent();
                                prizes = luckyWheel.getPrizes();
                                validPrizes = new LinkedList<>();
                                for (int i = 0; i < prizes.length; i++){
                                    if(prizes[i].isValid()){
                                        validPrizes.add(prizes[i]);
                                    }
                                }

//                                wheelView.setWheelItemCount(prizes.length);
                                wheelView.setAdapter(new MaterialColorAdapter(new LinkedList<Prize>(Arrays.asList(prizes))));
                                setPrizesToUI();
                            }
                            else
                            {
                                String error_des = apiResponse != null ? apiResponse.getResult() : null;
                                if ((error_des != null && !error_des.equals(""))) {
                                    Toast.makeText(getActivity(), error_des, Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(getActivity(), getString(R.string.went_wrong), Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                        catch ( Exception e)
                        {
                            Toast.makeText(getActivity(), getString(R.string.went_wrong), Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }, true).Execute();
    }

    public void GetPrizeBtnAction(final int prizeId)
    {
        CustomerPrize inModel = new CustomerPrize(prizeId);

        String json = gson.toJson(inModel);

        new SendGetJsonApi(getActivity(), CUSTOMER_PRIZES + ADD_LUCKY, json,
                new CallBackListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_IS_LUCKY_VALID, String.valueOf(false));
                            APIResponse<CustomerPrize> customerPrizeAPIResponse = getResponse(result, CustomerPrize.class);
                            if (customerPrizeAPIResponse != null && customerPrizeAPIResponse.getResult().equals("success"))
                            {
                                comeTxt.setVisibility(View.VISIBLE);
                                spinBtn.setEnabled(false);
                                luckyLayout.setAlpha(0.5F);
                                //luckyWheelView.setLuckyRoundItemSelectedListener(null);
                                final Prize prize = customerPrizeAPIResponse.getContent().getPrize();
                                final CustomDialog cdd = new CustomDialog(getActivity(), DIALOG_TYPE_PRIZE);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                    Objects.requireNonNull(cdd.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                }
                                cdd.show();
                                Glide.with(Objects.requireNonNull(getActivity()))
                                        .load(API_IMAGES_URL + (prize != null ? prize.getImageUrl() : null))
                                        .centerInside()
                                        .into(cdd.prizeImg);
                                assert prize != null;
                                cdd.prizeName.setText(prize.getDisplayName());
                                cdd.ok.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        List<Prize> leftPrizes = new LinkedList<>();
                                        for (int i = 0; i < prizes.length; i++){
                                            if(prizes[i].getId() != prizeId){
                                                leftPrizes.add(prizes[i]);
                                            }
                                        }
                                        wheelView.setAdapter(new MaterialColorAdapter(leftPrizes));
                                        cdd.dismiss();
                                    }
                                });
                            }
                            else
                            {
                                String error_des = customerPrizeAPIResponse != null ? customerPrizeAPIResponse.getResult() : null;
                                if((error_des != null && !error_des.equals(""))){
                                    Toast.makeText(getActivity(), error_des, Toast.LENGTH_LONG).show();
                                }else
                                {
                                    Toast.makeText(getActivity(), getString(R.string.went_wrong), Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                        catch ( Exception e)
                        {
                            Toast.makeText(getActivity(), getString(R.string.went_wrong), Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }, true).Execute();
    }

    class MaterialColorAdapter extends WheelArrayAdapter<Prize> {

        private List<Prize> prizeList;
        MaterialColorAdapter(List<Prize> entries) {
            super(entries);
            prizeList = entries;
        }

        @Override
        public int getCount() {
            if(prizeList.size() > 0){
                return  prizeList.size();
            }
            else {
                return prizes.length;
            }
        }

        @Override
        public Drawable getDrawable(int position) {
            wheelView.setOnWheelItemClickListener(null);

            if(prizeList != null && prizeList.size() > 0){
                if(prizeList.get(position).getPrizeType().getName().equals("units")){
                    Drawable[] drawable = new Drawable[] {
                            getResources().getDrawable(R.drawable.units),
                            new TextDrawable(prizeList.get(position).getDisplayName())
                    };
                    return new LayerDrawable(drawable);
                }
                else if(prizeList.get(position).getPrizeType().getName().equals("cash")){
                    Drawable[] drawable = new Drawable[] {
                            getResources().getDrawable(R.drawable.cash),
                            new TextDrawable(prizeList.get(position).getDisplayName())
                    };
                    return new LayerDrawable(drawable);
                }
                else{
                    Drawable[] drawable = new Drawable[] {
                            getResources().getDrawable(R.drawable.coupon),
                            new TextDrawable(prizeList.get(position).getDisplayName())
                    };
                    return new LayerDrawable(drawable);
                }
            }
            else {
                wheelView.setOnWheelItemClickListener(new WheelView.OnWheelItemClickListener() {
                    @Override
                    public void onWheelItemClick(WheelView parent, int position, boolean isSelected) {
                        Random r = new Random();
                        int selectedPrizeIndex = r.nextInt(validPrizes.size() - 1);
                        Prize selectedPrize = validPrizes.get(selectedPrizeIndex);
                        frame.playAnimation();
                        GetPrizeBtnAction(selectedPrize.getId());
                    }
                });
                Drawable[] drawable = new Drawable[] {
                        getResources().getDrawable(R.drawable.gift),
                        new TextDrawable("")
                };
                return new LayerDrawable(drawable);
            }
        }
    }
}