package com.u.mygamesapp.fragments;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;
import com.u.mygamesapp.adapters.TasksAdapter;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.models.AppTask;
import com.u.mygamesapp.requestmodels.PaginationRequest;
import com.u.mygamesapp.responsemodels.PagedAPIResponse;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static com.u.mygamesapp.api.APIConstants.GET_ALL;
import static com.u.mygamesapp.api.APIConstants.TASKS;
import static com.u.mygamesapp.helpers.JsonParser.getPagedResponse;

public class TasksFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static String TASKS_FRAGMENT_TAG  = TasksFragment.class.getSimpleName();

    private RecyclerView tasksRecyclerView;
    private List<AppTask> appTasks;
    private SwipeRefreshLayout swipeRefreshLayout;
    private int taskTypeId;
    private Gson gson = new Gson();

    public TasksFragment(int taskTypeId) {
        this.taskTypeId = taskTypeId;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tasks, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(R.string.home);
        BaseActivity.pointsLayout.setVisibility(View.VISIBLE);

        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getData();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
        );

        tasksRecyclerView = view.findViewById(R.id.tasks_recycler);
        GridLayoutManager mLayoutManager1 = new GridLayoutManager(getActivity(), 1);
        tasksRecyclerView.setLayoutManager(mLayoutManager1);

        getData();
    }

    @Override
    public void onRefresh() {
        if (swipeRefreshLayout.isRefreshing()) {
            getData();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void getData() {
        PaginationRequest request = new PaginationRequest(1, 200);
        request.setTaskTypeId(taskTypeId);
        String json = gson.toJson(request);
        new SendGetJsonApi(getActivity(), TASKS + GET_ALL, json,
                new CallBackListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            PagedAPIResponse<AppTask[]> pagedAPIResponse = getPagedResponse(result, AppTask[].class);
                            if (pagedAPIResponse != null && pagedAPIResponse.getMessage().equals("success"))
                            {
                                appTasks = new LinkedList<>(Arrays.asList(pagedAPIResponse.getContent().getContent()));
                                if(appTasks.size() > 0){
                                    onSuccess(appTasks);
                                }
                                else
                                {
                                    Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.fragment_container, new NoDataFragment())
                                            .commit();
                                }
                            }
                            else
                            {
                                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.fragment_container, new NoDataFragment())
                                        .commit();
                            }
                        }
                        catch ( Exception e)
                        {
                            Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container, new NoDataFragment())
                                    .commit();
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ).Execute();
    }

    private void onSuccess(List<AppTask> tasks) {

        TasksAdapter tasksAdapter = new TasksAdapter(getActivity(), tasks);
        tasksRecyclerView.setAdapter(tasksAdapter);
        tasksAdapter.setOnItemClickListener(getActivity());
    }
}
