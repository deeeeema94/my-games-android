package com.u.mygamesapp.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;
import com.u.mygamesapp.activities.FullScreenVideo;
import com.u.mygamesapp.activities.WebViewActivity;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.helpers.CustomDialog;
import com.u.mygamesapp.models.AppTask;
import com.u.mygamesapp.models.Customer;
import com.u.mygamesapp.models.CustomerTask;
import com.u.mygamesapp.responsemodels.APIResponse;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;
import static com.u.mygamesapp.api.APIConstants.ADD_END;
import static com.u.mygamesapp.api.APIConstants.ADD_START;
import static com.u.mygamesapp.api.APIConstants.API_IMAGES_URL;
import static com.u.mygamesapp.api.APIConstants.GET_ONE;
import static com.u.mygamesapp.api.APIConstants.TASKS;
import static com.u.mygamesapp.helpers.Constants.DIALOG_TYPE_PRIZE;
import static com.u.mygamesapp.helpers.Constants.INSTALL_APP_REQUEST_CODE;
import static com.u.mygamesapp.helpers.Constants.OPEN_LINK_REQUEST_CODE;
import static com.u.mygamesapp.helpers.Constants.TASK_TYPE_GENERAL;
import static com.u.mygamesapp.helpers.Constants.TASK_TYPE_INSTALL_APP;
import static com.u.mygamesapp.helpers.Constants.TASK_TYPE_QUIZ;
import static com.u.mygamesapp.helpers.Constants.TASK_TYPE_SHARE_THIS_APP;
import static com.u.mygamesapp.helpers.Constants.TASK_TYPE_SURVEY;
import static com.u.mygamesapp.helpers.Constants.TASK_TYPE_WATCH_LOCAL_AD;
import static com.u.mygamesapp.helpers.Constants.WATCH_LOCAL_AD_REQUEST_CODE;
import static com.u.mygamesapp.helpers.Constants.showRedToast;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_CURRENT_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_COLOR;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_ID;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_NEXT_GROUP_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_SHARE_CODE;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_TOTAL_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readFromPreferences;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.saveToPreferences;

public class TaskDetailsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static String TASK_DETAILS_FRAGMENT_TAG  = TaskDetailsFragment.class.getSimpleName();

    private TextView name, points, overview, started, ends;
    private Button startBtn, watchBtn;
    private ImageView img;
    private AppTask appTask;
    private SwipeRefreshLayout swipeRefreshLayout;
    private int appTaskId;
    private Gson gson = new Gson();
    private boolean IsFromDoneTask = false;
    private static Customer customer;
    private ViewGroup viewGroup;
    private View dialogView;
    private boolean IsFailed = false;

    public TaskDetailsFragment(int taskId){
        appTaskId = taskId;
    }

    public TaskDetailsFragment(int taskId, boolean isFromDoneTask, boolean isFailed){
        appTaskId = taskId;
        IsFromDoneTask = isFromDoneTask;
        IsFailed = isFailed;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_task_details, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(R.string.home);
        BaseActivity.pointsLayout.setVisibility(View.VISIBLE);

        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getData();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
        );

        viewGroup = view.findViewById(android.R.id.content);
        dialogView = LayoutInflater.from(view.getContext()).inflate(R.layout.dialog_custom_upgrade_group, viewGroup, false);

        name = view.findViewById(R.id.name);
        overview = view.findViewById(R.id.overview);
        started = view.findViewById(R.id.strted);
        ends = view.findViewById(R.id.ends);
        img = view.findViewById(R.id.img);
        points = view.findViewById(R.id.points);
        startBtn = view.findViewById(R.id.start_btn);
        watchBtn = view.findViewById(R.id.watch_btn);

        getData();
    }

    @Override
    public void onRefresh() {
        if (swipeRefreshLayout.isRefreshing()) {
            getData();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void getData() {
        AppTask inModel = new AppTask(appTaskId);
        String json = gson.toJson(inModel);
        new SendGetJsonApi(getActivity(), TASKS + GET_ONE, json,
                new CallBackListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<AppTask> appTaskAPIResponse = getResponse(result, AppTask.class);
                            if (appTaskAPIResponse != null && appTaskAPIResponse.getResult().equals("success"))
                            {
                                appTask = appTaskAPIResponse.getContent();
                                onSuccess(appTask);
                            }
                            else
                            {
                                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.fragment_container, new NoDataFragment())
                                        .addToBackStack(null)
                                        .commit();
                            }
                        }
                        catch ( Exception e)
                        {
                            Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container, new NoDataFragment())
                                    .addToBackStack(null)
                                    .commit();
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ).Execute();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("DefaultLocale")
    private void onSuccess(final AppTask appTask) {
        Objects.requireNonNull(getActivity()).setTitle(appTask.getDisplayName());
        name.setText(appTask.getDisplayName());
        points.setText(String.format("%d", appTask.getPoints()));
        overview.setText(appTask.getDescription() != null ? appTask.getDescription() : "");

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");

        Date endDate = null, startDate = null;
        try {
            endDate = format.parse(appTask.getEndDate());
            String end = formatter.format(endDate.getTime());
            ends.setText(String.format("%s", end));
            startDate = format.parse(appTask.getStartDate());
            String start = formatter.format(startDate.getTime());
            started.setText(String.format("%s", start));
        } catch (Exception e) {
            e.printStackTrace();
        }

        Glide.with(getActivity())
                .load(API_IMAGES_URL + appTask.getImageUrl())
                .optionalCenterInside()
                .into(img);

        if(appTask.isDone()){
            if(appTask.getTaskType().getName().equals(TASK_TYPE_WATCH_LOCAL_AD)){
                watchBtn.setEnabled(true);
                watchBtn.setVisibility(View.VISIBLE);
                watchBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), FullScreenVideo.class);
                        String videoUrl = appTask.getLink();
                        if(!videoUrl.startsWith("http")){
                            videoUrl = API_IMAGES_URL + videoUrl;
                        }
                        intent.putExtra("videoUrl", videoUrl);
                        intent.putExtra("isWatch", true);
                        startActivity(intent);
                    }
                });
            }
            else {
                watchBtn.setEnabled(false);
                watchBtn.setVisibility(View.GONE);
                startBtn.setEnabled(false);
                startBtn.setText(R.string.done_task);
            }
        }
        else{
            if(appTask.isReachLimit()){
                if(appTask.getTaskType().getName().equals(TASK_TYPE_WATCH_LOCAL_AD)){
                    watchBtn.setEnabled(true);
                    watchBtn.setVisibility(View.VISIBLE);
                    watchBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(getActivity(), FullScreenVideo.class);
                            String videoUrl = appTask.getLink();
                            if(!videoUrl.startsWith("http")){
                                videoUrl = API_IMAGES_URL + videoUrl;
                            }
                            intent.putExtra("videoUrl", videoUrl);
                            intent.putExtra("videoDuration", appTask.getVedioDuration());
                            intent.putExtra("isWatch", true);
                            startActivity(intent);
                        }
                    });
                }
                startBtn.setEnabled(false);
                startBtn.setText(R.string.task_reached_limit);
            }
            else{
                startBtn.setEnabled(true);
                startBtn.setText(R.string.start);
            }
        }

        if(IsFromDoneTask){
            if(IsFailed)
                showFailDialog();
            else
                showConDialog();
        }

        if(appTask.getTaskType().getName().equals(TASK_TYPE_SURVEY) || appTask.getTaskType().getName().equals(TASK_TYPE_QUIZ)){
            startBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, new QuestionFragment(appTask.getId(), 1))
                            .commit();
                }
            });
        }
        else if(appTask.getTaskType().getName().equals(TASK_TYPE_INSTALL_APP)){
            boolean isInstalled = isPackageInstalled(appTask.getPackageName(), getActivity().getPackageManager());
            if(isInstalled){
                startBtn.setEnabled(false);
                startBtn.setText(R.string.app_installed);
            }
            else {
                startBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        addTaskStarted();
                    }
                });
            }
        }
        else if(appTask.getTaskType().getName().equals(TASK_TYPE_WATCH_LOCAL_AD)){
            startBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addTaskStarted();
                }
            });
        }
        else if(appTask.getTaskType().getName().equals(TASK_TYPE_SHARE_THIS_APP)){
            startBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String code = readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_SHARE_CODE, "");
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, String.format("%s %s", appTask.getLink(), code));
                    sendIntent.setType("text/plain");
                    startActivity(Intent.createChooser(sendIntent, "choose one"));
                }
            });
        }
        else if(appTask.getTaskType().getName().equals(TASK_TYPE_GENERAL)){
            startBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addTaskStarted();
                }
            });
        }
    }

    private void showConDialog() {
        final CustomDialog cdd = new CustomDialog(getActivity(), DIALOG_TYPE_PRIZE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(cdd.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        cdd.show();
        cdd.setCancelable(false);
        Glide.with(getActivity())
                .load(getImage("star"))
                .centerInside()
                .into(cdd.prizeImg);
        cdd.prizeName.setText(String.format("%d %s", appTask.getPoints(), getString(R.string.points)));
        cdd.ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cdd.dismiss();
                if(customer != null){
                    setUserGroupInfo();
                }
            }
        });
    }

    private void showFailDialog() {
        final CustomDialog cdd = new CustomDialog(getActivity(), DIALOG_TYPE_PRIZE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(cdd.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        cdd.show();
        cdd.setCancelable(false);
        cdd.prizeImg.setVisibility(View.GONE);
        cdd.icon.setVisibility(View.GONE);
        cdd.iconSad.setVisibility(View.VISIBLE);
        cdd.prizeName.setText(R.string.failed_the_quiz);
        cdd.ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cdd.dismiss();
                if(customer != null){
                    setUserGroupInfo();
                }
            }
        });
    }

    public int getImage(String imageName) {
        int drawableResourceId = this.getResources().getIdentifier(imageName, "drawable", getActivity().getPackageName());
        return drawableResourceId;
    }

    private boolean isPackageInstalled(String packageName, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private void addTaskStarted() {
        CustomerTask inModel = new CustomerTask(appTaskId);
        String json = gson.toJson(inModel);
        new SendGetJsonApi(getActivity(), TASKS + ADD_START, json,
                new CallBackListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<CustomerTask> appTaskAPIResponse = getResponse(result, CustomerTask.class);
                            if (appTaskAPIResponse != null && appTaskAPIResponse.getResult().equals("success"))
                            {
                                if(appTask.getTaskType().getName().equals(TASK_TYPE_INSTALL_APP)) {
                                    final String appPackageName = appTask.getPackageName(); // getPackageName() from Context or Activity object
                                    try {
                                        startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)), INSTALL_APP_REQUEST_CODE);
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                        startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)), INSTALL_APP_REQUEST_CODE);
                                    }
                                }
                                else if(appTask.getTaskType().getName().equals(TASK_TYPE_WATCH_LOCAL_AD)){
                                    Intent intent = new Intent(getActivity(), FullScreenVideo.class);
                                    String videoUrl = appTask.getLink();
                                    if(!videoUrl.startsWith("http")){
                                        videoUrl = API_IMAGES_URL + videoUrl;
                                    }
                                    intent.putExtra("videoUrl", videoUrl);
                                    intent.putExtra("videoDuration", appTask.getVedioDuration());
                                    intent.putExtra("isWatch", false);
                                    startActivityForResult(intent, WATCH_LOCAL_AD_REQUEST_CODE);
                                }
                                else if(appTask.getTaskType().getName().equals(TASK_TYPE_GENERAL)){
                                    Intent webIntent = new Intent(getActivity(), WebViewActivity.class);
                                    webIntent.putExtra("link", appTask.getLink());
                                    startActivityForResult(webIntent, OPEN_LINK_REQUEST_CODE);
                                }
                            }
                            else
                            {
                                showRedToast(getApplicationContext(), getString(R.string.went_wrong));
                            }
                        }
                        catch ( Exception e)
                        {
                            showRedToast(getApplicationContext(), getString(R.string.went_wrong));
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ).Execute();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == INSTALL_APP_REQUEST_CODE){
            boolean isInstalled = isPackageInstalled(appTask.getPackageName(), getActivity().getPackageManager());
            if(isInstalled){
                addTaskEnded();
            }
        }
        else if(requestCode == WATCH_LOCAL_AD_REQUEST_CODE && resultCode == RESULT_OK){
            addTaskEnded();
        }
        else if(requestCode == OPEN_LINK_REQUEST_CODE && resultCode == RESULT_OK){
            boolean isDone = true;
            if (data != null) {
                isDone = data.getBooleanExtra("isDone", true);
            }
            if(isDone){
                addTaskEnded();
            }
        }
    }

    private void addTaskEnded() {
        CustomerTask inModel = new CustomerTask(appTaskId);
        String json = gson.toJson(inModel);
        new SendGetJsonApi(getActivity(), TASKS + ADD_END, json,
                new CallBackListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<CustomerTask> appTaskAPIResponse = getResponse(result, CustomerTask.class);
                            if (appTaskAPIResponse != null && appTaskAPIResponse.getResult().equals("success"))
                            {
                                customer = appTaskAPIResponse.getCurrentCustomer();
                                startBtn.setEnabled(false);
                                showConDialog();
                            }
                            else
                            {
                                showRedToast(getApplicationContext(), getString(R.string.went_wrong));
                            }
                        }
                        catch ( Exception e)
                        {
                            showRedToast(getApplicationContext(), getString(R.string.went_wrong));
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ).Execute();
    }

    private void setUserGroupInfo(){
        String groupId = readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_ID, "0");
        boolean isUpgradeGroup = false;
        if(!groupId.equals(String.valueOf(customer.getGroupId()))){
            isUpgradeGroup = true;
        }
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_TOTAL_POINTS, String.valueOf(customer.getTotalPoints()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_CURRENT_POINTS, String.valueOf(customer.getCurrentPoints()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_ID, String.valueOf(customer.getGroupId()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_NAME, String.valueOf(customer.getGroup().getDisplayName()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_COLOR, String.valueOf(customer.getGroup().getColor()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_NEXT_GROUP_POINTS, String.valueOf(customer.getNextGroupPoints()));
        BaseActivity.setUserGroupInfo(customer.getGroup().getDisplayName(), String.valueOf(customer.getCurrentPoints()),
                String.valueOf(customer.getTotalPoints()), customer.getGroup().getColor(), customer.getNextGroupPoints());
        if(isUpgradeGroup){
            showUpgradeDialog();
        }
    }

    private void showUpgradeDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.CustomAlertDialog);
        TextView groupName = dialogView.findViewById(R.id.group_name);
        CardView cardView = dialogView.findViewById(R.id.dialog_card);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        groupName.setText(String.format("%s %s", customer.getGroup().getDisplayName(), getActivity().getString(R.string.group)));
        cardView.setCardBackgroundColor(Color.parseColor(customer.getGroup().getColor()));
        alertDialog.show();
    }
}
