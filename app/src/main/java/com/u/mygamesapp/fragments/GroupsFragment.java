package com.u.mygamesapp.fragments;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;
import com.u.mygamesapp.adapters.GroupsAdapter;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.models.Group;
import com.u.mygamesapp.requestmodels.PaginationRequest;
import com.u.mygamesapp.responsemodels.PagedAPIResponse;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static com.u.mygamesapp.api.APIConstants.GET_ALL;
import static com.u.mygamesapp.api.APIConstants.GROUPS;
import static com.u.mygamesapp.helpers.JsonParser.getPagedResponse;

public class GroupsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static String INBOX_FRAGMENT_TAG  = GroupsFragment.class.getSimpleName();
    private Boolean isStarted = false;
    private Boolean isVisible = false;

    private RecyclerView groupsRecyclerView;
    private List<Group> groups;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Gson gson = new Gson();
    private TextView txt_no_data;

    public GroupsFragment() {
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_account_tabs, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.groups));
        BaseActivity.pointsLayout.setVisibility(View.GONE);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getData();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
        );

        txt_no_data = view.findViewById(R.id.txt_no_data);
        groupsRecyclerView = view.findViewById(R.id.tasks_recycler);
        GridLayoutManager mLayoutManager1 = new GridLayoutManager(getActivity(), 1);
        groupsRecyclerView.setLayoutManager(mLayoutManager1);
        isStarted = true;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isStarted && isVisible) {
            getData();
        }
    }

    @Override
    public void onRefresh() {
        if (swipeRefreshLayout.isRefreshing()) {
            getData();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void getData() {
        PaginationRequest request = new PaginationRequest(1, 200);
        String json = gson.toJson(request);
        new SendGetJsonApi(getActivity(), GROUPS + GET_ALL, json,
                new CallBackListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onFinish(String result)
                    {
                        isVisible = false;
                        isStarted = false;
                        try {
                            PagedAPIResponse<Group[]> pagedAPIResponse = getPagedResponse(result, Group[].class);
                            if (pagedAPIResponse != null && pagedAPIResponse.getMessage().equals("success"))
                            {
                                groups = new LinkedList<>(Arrays.asList(pagedAPIResponse.getContent().getContent()));
                                if(groups.size() > 0){
                                    txt_no_data.setVisibility(View.GONE);
                                    groupsRecyclerView.setVisibility(View.VISIBLE);
                                    onSuccess(groups);
                                }
                                else
                                {
                                    txt_no_data.setVisibility(View.VISIBLE);
                                    groupsRecyclerView.setVisibility(View.GONE);
                                }
                            }
                            else
                            {
                                txt_no_data.setVisibility(View.VISIBLE);
                                groupsRecyclerView.setVisibility(View.GONE);
                            }
                        }
                        catch ( Exception e)
                        {
                            txt_no_data.setVisibility(View.VISIBLE);
                            groupsRecyclerView.setVisibility(View.GONE);
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ).Execute();
    }

    private void onSuccess(List<Group> gs) {

        GroupsAdapter groupsAdapter = new GroupsAdapter(getActivity(), gs);
        groupsRecyclerView.setAdapter(groupsAdapter);
        groupsAdapter.setOnItemClickListener(getActivity());
    }
}
