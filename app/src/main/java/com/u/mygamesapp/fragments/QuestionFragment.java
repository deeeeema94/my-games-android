package com.u.mygamesapp.fragments;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.models.Customer;
import com.u.mygamesapp.models.CustomerAnswer;
import com.u.mygamesapp.models.Question;
import com.u.mygamesapp.responsemodels.APIResponse;

import java.util.Objects;

import static com.u.mygamesapp.api.APIConstants.ADD;
import static com.u.mygamesapp.api.APIConstants.GET_ONE;
import static com.u.mygamesapp.api.APIConstants.QUESTIONS;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_CURRENT_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_COLOR;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_ID;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_NEXT_GROUP_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_TOTAL_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readFromPreferences;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.saveToPreferences;

public class QuestionFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static String QUESTION_FRAGMENT_TAG  = QuestionFragment.class.getSimpleName();

    private TextView script, description, resultTxt;
    private EditText answer;
    private Button submitBtn;
    com.google.android.material.textfield.TextInputLayout answerLayout;
    private Question question;
    private RadioGroup optionsRadioGroup;
    private SwipeRefreshLayout swipeRefreshLayout;
    private static int appTaskId, itemOrder;
    private Gson gson = new Gson();
    private int option_id;
    private static Customer customer;
    private ViewGroup viewGroup;
    private View dialogView;

    public QuestionFragment(int taskId, int order){
        appTaskId = taskId;
        itemOrder = order;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_question, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.survey));
        BaseActivity.pointsLayout.setVisibility(View.VISIBLE);

        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getData();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
        );

        viewGroup = view.findViewById(android.R.id.content);
        dialogView = LayoutInflater.from(view.getContext()).inflate(R.layout.dialog_custom_upgrade_group, viewGroup, false);

        script = view.findViewById(R.id.script);
        description = view.findViewById(R.id.description);
        resultTxt = view.findViewById(R.id.txt_result);
        optionsRadioGroup = view.findViewById(R.id.radio_options);
        answer = view.findViewById(R.id.edit_answer);
        answerLayout = view.findViewById(R.id.answer_layout);
        submitBtn = view.findViewById(R.id.submit_btn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SubmitAction();
            }
        });

        getData();
    }

    private void SubmitAction() {
        resultTxt.setText("");

        CustomerAnswer inModel;
        if(question.isType()){
            option_id = optionsRadioGroup.getCheckedRadioButtonId();
            boolean isExist = false;
            for(int i = 0; i < question.getQuestionOptions().size(); i++){
                if(question.getQuestionOptions().get(i).getId() == option_id){
                    isExist = true;
                    break;
                }
            }
            if(option_id == 0 || option_id == -1 || !isExist) {
                resultTxt.setText(R.string.option_required);return;
            }
            inModel = new CustomerAnswer(option_id, question.getId());
        }
        else {
            if (answer.getText().toString().trim().equals("")) {
                answer.setError(getString(R.string.field_required));
                answer.requestFocus();
                return;
            }
            inModel = new CustomerAnswer(question.getId(), answer.getText().toString().trim());
        }
        String json = gson.toJson(inModel);
        new SendGetJsonApi(getActivity(), QUESTIONS + ADD, json,
                new CallBackListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<Question> questionAPIResponse = getResponse(result, Question.class);
                            if (questionAPIResponse != null && questionAPIResponse.getResult().equals("success"))
                            {
                                option_id = -1;
                                answer.setText("");
                                if(questionAPIResponse.getContent().isLast()){
                                    customer = questionAPIResponse.getCurrentCustomer();
                                    if(questionAPIResponse.getContent().isAllRight()){
                                        setUserGroupInfo();
                                    }
                                    else {
                                        Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                                .replace(R.id.fragment_container, new TaskDetailsFragment(appTaskId, true, true))
                                                .commit();
                                    }
                                }
                                else {
                                    itemOrder = questionAPIResponse.getContent().getItemOrder() + 1;
                                    getData();
                                }
                            }
                            else
                            {
                                String error_des = null;
                                if (questionAPIResponse != null) {
                                    error_des = questionAPIResponse.getResult();
                                }
                                if((error_des != null && !error_des.equals(""))){
                                    resultTxt.setText(error_des);
                                }else
                                {
                                    resultTxt.setText(getString(R.string.went_wrong));
                                }
                            }
                        }
                        catch ( Exception e)
                        {
                            resultTxt.setText(getString(R.string.went_wrong));
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ).Execute();
    }

    @Override
    public void onRefresh() {
        if (swipeRefreshLayout.isRefreshing()) {
            getData();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void getData() {
        Question inModel = new Question(itemOrder, appTaskId);
        String json = gson.toJson(inModel);
        new SendGetJsonApi(getActivity(), QUESTIONS + GET_ONE, json,
                new CallBackListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<Question> appTaskAPIResponse = getResponse(result, Question.class);
                            if (appTaskAPIResponse != null && appTaskAPIResponse.getResult().equals("success"))
                            {
                                question = appTaskAPIResponse.getContent();
                                onSuccess(question);
                            }
                            else
                            {
                                getActivity().getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.fragment_container, new NoDataFragment())
                                        .addToBackStack(null)
                                        .commit();
                            }
                        }
                        catch ( Exception e)
                        {
                            getActivity().getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container, new NoDataFragment())
                                    .addToBackStack(null)
                                    .commit();
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ).Execute();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("DefaultLocale")
    private void onSuccess(Question question) {
        script.setText(question.getScript());
        description.setText(question.getDescription() != null ? question.getDescription() : "");
        if(question.isType()){
            optionsRadioGroup.removeAllViews();
            final RadioButton[] rb = new RadioButton[question.getQuestionOptions().size()];
            for(int i = 0; i < question.getQuestionOptions().size(); i++){
                rb[i]  = new RadioButton(getActivity());
                rb[i].setText(String.format(" %s", question.getQuestionOptions().get(i).getScript()));
                rb[i].setId(question.getQuestionOptions().get(i).getId());
                optionsRadioGroup.addView(rb[i]);
            }
            optionsRadioGroup.setVisibility(View.VISIBLE);
            answerLayout.setVisibility(View.GONE);
        }
        else {
            optionsRadioGroup.setVisibility(View.GONE);
            answerLayout.setVisibility(View.VISIBLE);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setUserGroupInfo(){
        String groupId = readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_ID, "0");
        boolean isUpgradeGroup = false;
        if(!groupId.equals(String.valueOf(customer.getGroupId()))){
            isUpgradeGroup = true;
        }
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_TOTAL_POINTS, String.valueOf(customer.getTotalPoints()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_CURRENT_POINTS, String.valueOf(customer.getCurrentPoints()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_ID, String.valueOf(customer.getGroupId()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_NAME, String.valueOf(customer.getGroup().getDisplayName()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_COLOR, String.valueOf(customer.getGroup().getColor()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_NEXT_GROUP_POINTS, String.valueOf(customer.getNextGroupPoints()));
        BaseActivity.setUserGroupInfo(customer.getGroup().getDisplayName(), String.valueOf(customer.getCurrentPoints()),
                String.valueOf(customer.getTotalPoints()), customer.getGroup().getColor(), customer.getNextGroupPoints());
        if(isUpgradeGroup){
            showUpgradeDialog();
        }
        else{
            Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, new TaskDetailsFragment(appTaskId, true, false))
                    .commit();
        }
    }

    private void showUpgradeDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.CustomAlertDialog);
        TextView groupName = dialogView.findViewById(R.id.group_name);
        CardView cardView = dialogView.findViewById(R.id.dialog_card);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        groupName.setText(String.format("%s %s", customer.getGroup().getDisplayName(), getActivity().getString(R.string.group)));
        cardView.setCardBackgroundColor(Color.parseColor(customer.getGroup().getColor()));
        alertDialog.show();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new TaskDetailsFragment(appTaskId, true, false))
                        .commit();
            }
        });
    }
}
