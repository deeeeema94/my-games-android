package com.u.mygamesapp.fragments;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.models.Customer;
import com.u.mygamesapp.models.CustomerPrize;
import com.u.mygamesapp.models.Prize;
import com.u.mygamesapp.responsemodels.APIResponse;

import java.util.Objects;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.u.mygamesapp.api.APIConstants.API_IMAGES_URL;
import static com.u.mygamesapp.api.APIConstants.GET_ONE;
import static com.u.mygamesapp.api.APIConstants.PRIZES;
import static com.u.mygamesapp.api.APIConstants.REQUEST_PRIZE;
import static com.u.mygamesapp.helpers.Constants.showGreenToast;
import static com.u.mygamesapp.helpers.Constants.showRedToast;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_CURRENT_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_COLOR;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_ID;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_NEXT_GROUP_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_TOTAL_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readFromPreferences;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.saveToPreferences;

public class PrizeDetailsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static String PRIZE_DETAILS_FRAGMENT_TAG  = PrizeDetailsFragment.class.getSimpleName();

    private TextView name, points, overview;
    private Button requestBtn;
    private ImageView img;
    private Prize prize;
    private SwipeRefreshLayout swipeRefreshLayout;
    private int prizeId;
    private Gson gson = new Gson();
    private static Customer customer;
    private ViewGroup viewGroup;
    private View dialogView;

    public PrizeDetailsFragment(int pId){
        prizeId = pId;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_prize_details, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(R.string.home);
        BaseActivity.pointsLayout.setVisibility(View.VISIBLE);

        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getData();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
        );

        viewGroup = view.findViewById(android.R.id.content);
        dialogView = LayoutInflater.from(view.getContext()).inflate(R.layout.dialog_custom_upgrade_group, viewGroup, false);

        name = view.findViewById(R.id.name);
        overview = view.findViewById(R.id.overview);
        img = view.findViewById(R.id.img);
        points = view.findViewById(R.id.points);
        requestBtn = view.findViewById(R.id.request_btn);

        getData();
    }

    @Override
    public void onRefresh() {
        if (swipeRefreshLayout.isRefreshing()) {
            getData();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void getData() {
        Prize inModel = new Prize(prizeId);
        String json = gson.toJson(inModel);
        new SendGetJsonApi(getActivity(), PRIZES + GET_ONE, json,
                new CallBackListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<Prize> prizeAPIResponse = getResponse(result, Prize.class);
                            if (prizeAPIResponse != null && prizeAPIResponse.getResult().equals("success"))
                            {
                                prize = prizeAPIResponse.getContent();
                                onSuccess(prize);
                            }
                            else
                            {
                                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.fragment_container, new NoDataFragment())
                                        .addToBackStack(null)
                                        .commit();
                            }
                        }
                        catch ( Exception e)
                        {
                            Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container, new NoDataFragment())
                                    .addToBackStack(null)
                                    .commit();
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ).Execute();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("DefaultLocale")
    private void onSuccess(final Prize prize) {
        Objects.requireNonNull(getActivity()).setTitle(prize.getDisplayName());
        name.setText(prize.getDisplayName());
        points.setText(String.format("%d", prize.getPoints()));
        overview.setText(prize.getDescription() != null ? prize.getDescription() : "");

        Glide.with(getActivity())
                .load(API_IMAGES_URL + prize.getImageUrl())
                .centerInside()
                .into(img);

        requestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestPrize();
            }
        });
    }

    private void requestPrize() {
        CustomerPrize inModel = new CustomerPrize(prizeId);
        String json = gson.toJson(inModel);
        new SendGetJsonApi(getActivity(), PRIZES + REQUEST_PRIZE, json,
                new CallBackListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<CustomerPrize> appTaskAPIResponse = getResponse(result, CustomerPrize.class);
                            if (appTaskAPIResponse != null && appTaskAPIResponse.getResult().equals("success"))
                            {
                                customer = appTaskAPIResponse.getCurrentCustomer();
                                String msg = getString(R.string.prize_requested);
                                showGreenToast(getApplicationContext(), msg);
                                setUserGroupInfo();
                            }
                            else
                            {
                                String error_des = appTaskAPIResponse != null ? appTaskAPIResponse.getResult() : null;
                                if((error_des != null && !error_des.equals(""))){
                                    showRedToast(getApplicationContext(), error_des);
                                }else
                                {
                                    showRedToast(getApplicationContext(), getString(R.string.went_wrong));
                                }
                            }
                        }
                        catch ( Exception e)
                        {
                            showRedToast(getApplicationContext(), getString(R.string.went_wrong));
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ).Execute();
    }

    private void setUserGroupInfo(){
        String groupId = readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_ID, "0");
        boolean isUpgradeGroup = false;
        if(!groupId.equals(String.valueOf(customer.getGroupId()))){
            isUpgradeGroup = true;
        }
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_TOTAL_POINTS, String.valueOf(customer.getTotalPoints()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_CURRENT_POINTS, String.valueOf(customer.getCurrentPoints()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_ID, String.valueOf(customer.getGroupId()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_NAME, String.valueOf(customer.getGroup().getDisplayName()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_COLOR, String.valueOf(customer.getGroup().getColor()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_NEXT_GROUP_POINTS, String.valueOf(customer.getNextGroupPoints()));
        BaseActivity.setUserGroupInfo(customer.getGroup().getDisplayName(), String.valueOf(customer.getCurrentPoints()),
                String.valueOf(customer.getTotalPoints()), customer.getGroup().getColor(), customer.getNextGroupPoints());
        if(isUpgradeGroup){
            showUpgradeDialog();
        }
    }

    private void showUpgradeDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.CustomAlertDialog);
        TextView groupName = dialogView.findViewById(R.id.group_name);
        CardView cardView = dialogView.findViewById(R.id.dialog_card);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        groupName.setText(String.format("%s %s", customer.getGroup().getDisplayName(), getActivity().getString(R.string.group)));
        cardView.setCardBackgroundColor(Color.parseColor(customer.getGroup().getColor()));
        alertDialog.show();
    }
}
