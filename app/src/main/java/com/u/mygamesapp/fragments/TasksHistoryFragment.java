package com.u.mygamesapp.fragments;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;
import com.u.mygamesapp.adapters.CustomerTasksAdapter;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.models.CustomerTask;
import com.u.mygamesapp.requestmodels.PaginationRequest;
import com.u.mygamesapp.responsemodels.PagedAPIResponse;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static com.u.mygamesapp.api.APIConstants.CUSTOMER_TASKS;
import static com.u.mygamesapp.api.APIConstants.GET_ALL;
import static com.u.mygamesapp.helpers.JsonParser.getPagedResponse;

public class TasksHistoryFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static String TASKS_HISTORY_FRAGMENT_TAG  = TasksHistoryFragment.class.getSimpleName();

    private RecyclerView tasksRecyclerView;
    private List<CustomerTask> appTasks;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Gson gson = new Gson();
//    private Boolean isStarted = false;
//    private Boolean isVisible = false;
    private TextView txt_no_data;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_account_tabs, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(R.string.home);
        BaseActivity.pointsLayout.setVisibility(View.GONE);

        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getData();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
        );

        txt_no_data = view.findViewById(R.id.txt_no_data);
        tasksRecyclerView = view.findViewById(R.id.tasks_recycler);
        GridLayoutManager mLayoutManager1 = new GridLayoutManager(getActivity(), 1);
        tasksRecyclerView.setLayoutManager(mLayoutManager1);

        getData();
//        isStarted = true;
    }

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        isVisible = isVisibleToUser;
//        if (isStarted && isVisible) {
//            getData();
//        }
//    }

    @Override
    public void onRefresh() {
        if (swipeRefreshLayout.isRefreshing()) {
            getData();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void getData() {
        PaginationRequest request = new PaginationRequest(1, 200);
        String json = gson.toJson(request);
        new SendGetJsonApi(getActivity(), CUSTOMER_TASKS + GET_ALL, json,
                new CallBackListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onFinish(String result)
                    {
//                        isVisible = false;
//                        isStarted = false;
                        try {
                            PagedAPIResponse<CustomerTask[]> pagedAPIResponse = getPagedResponse(result, CustomerTask[].class);
                            if (pagedAPIResponse != null && pagedAPIResponse.getMessage().equals("success"))
                            {
                                appTasks = new LinkedList<>(Arrays.asList(pagedAPIResponse.getContent().getContent()));
                                if(appTasks.size() > 0){
                                    txt_no_data.setVisibility(View.GONE);
                                    tasksRecyclerView.setVisibility(View.VISIBLE);
                                    onSuccess(appTasks);
                                }
                                else
                                {
                                    txt_no_data.setVisibility(View.VISIBLE);
                                    tasksRecyclerView.setVisibility(View.GONE);
                                }
                            }
                            else
                            {
                                txt_no_data.setVisibility(View.VISIBLE);
                                tasksRecyclerView.setVisibility(View.GONE);
                            }
                        }
                        catch ( Exception e)
                        {
                            txt_no_data.setVisibility(View.VISIBLE);
                            tasksRecyclerView.setVisibility(View.GONE);
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ).Execute();
    }

    private void onSuccess(List<CustomerTask> tasks) {

        CustomerTasksAdapter tasksAdapter = new CustomerTasksAdapter(getActivity(), tasks);
        tasksRecyclerView.setAdapter(tasksAdapter);
        tasksAdapter.setOnItemClickListener(getActivity());
    }
}
