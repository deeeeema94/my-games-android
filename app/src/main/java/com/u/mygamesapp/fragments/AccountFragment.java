package com.u.mygamesapp.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;
import com.u.mygamesapp.activities.authentication.ProfileSettingsActivity;

import java.util.Objects;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.u.mygamesapp.api.APIConstants.API_IMAGES_URL;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_CURRENT_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_FIRST_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_COLOR;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_IMAGE;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_LAST_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_TOTAL_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readFromPreferences;

public class AccountFragment extends Fragment {

    public static String ACCOUNT_FRAGMENT_TAG  = AccountFragment.class.getSimpleName();

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_account, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.account));
        BaseActivity.pointsLayout.setVisibility(View.GONE);

//        TabLayout tabLayout = view.findViewById(R.id.tab_layout);
//        tabLayout.addTab(tabLayout.newTab().setText(R.string.inbox));
//        tabLayout.addTab(tabLayout.newTab().setText(R.string.prizes_history));
//        tabLayout.addTab(tabLayout.newTab().setText(R.string.tasks_history));
//        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
//
//        List<Fragment> fragments = new ArrayList<>();
//        InboxFragment inboxFragment = new InboxFragment();
//        fragments.add(inboxFragment);
//
//        PrizesHistoryFragment prizesHistoryFragment = new PrizesHistoryFragment();
//        fragments.add(prizesHistoryFragment);
//
//        TasksHistoryFragment tasksHistoryFragment = new TasksHistoryFragment();
//        fragments.add(tasksHistoryFragment);
//
//        final ViewPager viewPager = view.findViewById(R.id.pager);
//        final PagerAdapter adapter = new PagerAdapter
//                (getActivity().getSupportFragmentManager());
//        adapter.setFragmentList(fragments);
//        viewPager.setOffscreenPageLimit(2);
//        viewPager.setAdapter(adapter);
//        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
//        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                viewPager.setCurrentItem(tab.getPosition());
//            }
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        });

        ImageView userImg = view.findViewById(R.id.user_img);

        String userImageUrl = readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_IMAGE, "");
        if(!userImageUrl.equals("")){
            userImg.setVisibility(View.VISIBLE);
            Glide.with(getApplicationContext())
                    .load(API_IMAGES_URL + userImageUrl)
                    .circleCrop()
                    .placeholder(R.drawable.icon)
                    .into(userImg);
        }

        String firstName = readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_FIRST_NAME, "");
        String lastName = readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_LAST_NAME, "");
        TextView user_profile = view.findViewById(R.id.name);
        user_profile.setText(String.format("%s %s", firstName, lastName));

        ImageView profile_settings = view.findViewById(R.id.profile_settings);
        profile_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Objects.requireNonNull(getActivity()).startActivity(new Intent(getActivity(), ProfileSettingsActivity.class));
            }
        });

        String currentPoints = readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_CURRENT_POINTS, "0");
        TextView current_points = view.findViewById(R.id.current_points);
        current_points.setText(String.format("%s", currentPoints));

        String totalPoints = readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_TOTAL_POINTS, "0");
        TextView total_points = view.findViewById(R.id.total_points);
        total_points.setText(String.format("%s", totalPoints));


        String currentGroup = readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_NAME, "0");
        String color = readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_COLOR, "");
        TextView user_group = view.findViewById(R.id.user_group);
        user_group.setText(String.format("%s", currentGroup));
        user_group.setTextColor(Color.parseColor(color));
    }
}

