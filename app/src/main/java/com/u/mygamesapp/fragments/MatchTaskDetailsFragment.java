package com.u.mygamesapp.fragments;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.helpers.CustomDialog;
import com.u.mygamesapp.models.AppTask;
import com.u.mygamesapp.models.Customer;
import com.u.mygamesapp.models.CustomerTask;
import com.u.mygamesapp.models.SportMatch;
import com.u.mygamesapp.requestmodels.PredictBody;
import com.u.mygamesapp.responsemodels.APIResponse;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.u.mygamesapp.api.APIConstants.ADD_END;
import static com.u.mygamesapp.api.APIConstants.API_IMAGES_URL;
import static com.u.mygamesapp.api.APIConstants.GET_ONE;
import static com.u.mygamesapp.api.APIConstants.SPORT_MATCHES;
import static com.u.mygamesapp.helpers.Constants.DIALOG_TYPE_PREDICT;
import static com.u.mygamesapp.helpers.Constants.showRedToast;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;

public class MatchTaskDetailsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static String MATCH_TASK_DETAILS_FRAGMENT_TAG  = MatchTaskDetailsFragment.class.getSimpleName();

    private TextView name, points, overview, started, ends, timer, time, team1Name, team2Name, league, type;
    private Button startBtn;
    private ImageView team1Flag, team2Flag;
    private AppTask appTask;
    private SportMatch sportMatch;
    private SwipeRefreshLayout swipeRefreshLayout;
    private int appTaskId;
    private Gson gson = new Gson();
    private boolean IsFromDoneTask = false;
    private static Customer customer;
    private ViewGroup viewGroup;
    private View dialogView;
    private long counter;
    private LinearLayout league_layout, type_layout;

    public MatchTaskDetailsFragment(int taskId){
        appTaskId = taskId;
    }

    public MatchTaskDetailsFragment(int taskId, boolean isFromDoneTask){
        appTaskId = taskId;
        IsFromDoneTask = isFromDoneTask;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_match_task_details, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(R.string.home);
        BaseActivity.pointsLayout.setVisibility(View.VISIBLE);

        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getData();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
        );

        viewGroup = view.findViewById(android.R.id.content);
        dialogView = LayoutInflater.from(view.getContext()).inflate(R.layout.dialog_custom_upgrade_group, viewGroup, false);

        name = view.findViewById(R.id.name);
        overview = view.findViewById(R.id.overview);
        started = view.findViewById(R.id.strted);
        ends = view.findViewById(R.id.ends);
        timer = view.findViewById(R.id.timer);
        time = view.findViewById(R.id.time);
        //img = view.findViewById(R.id.img);
        team1Flag = view.findViewById(R.id.team1_flag);
        team2Flag = view.findViewById(R.id.team2_flag);
        team1Name = view.findViewById(R.id.team1_name);
        team2Name = view.findViewById(R.id.team2_name);
        points = view.findViewById(R.id.points);
        startBtn = view.findViewById(R.id.start_btn);

        league = view.findViewById(R.id.league);
        league_layout = view.findViewById(R.id.league_layout);

        type = view.findViewById(R.id.type);
        type_layout = view.findViewById(R.id.type_layout);

        getData();
    }

    @Override
    public void onRefresh() {
        if (swipeRefreshLayout.isRefreshing()) {
            getData();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void getData() {
        SportMatch inModel = new SportMatch(appTaskId);
        String json = gson.toJson(inModel);
        new SendGetJsonApi(getActivity(), SPORT_MATCHES + GET_ONE, json,
                new CallBackListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<SportMatch> appTaskAPIResponse = getResponse(result, SportMatch.class);
                            if (appTaskAPIResponse != null && appTaskAPIResponse.getResult().equals("success"))
                            {
                                sportMatch = appTaskAPIResponse.getContent();
                                appTask = sportMatch.getAppTask();
                                onSuccess();
                            }
                            else
                            {
                                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.fragment_container, new NoDataFragment())
                                        .addToBackStack(null)
                                        .commit();
                            }
                        }
                        catch ( Exception e)
                        {
                            Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container, new NoDataFragment())
                                    .addToBackStack(null)
                                    .commit();
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ).Execute();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("DefaultLocale")
    private void onSuccess() {
        Objects.requireNonNull(getActivity()).setTitle(appTask.getDisplayName());
        name.setText(appTask.getDisplayName());
        points.setText(String.format("%d", appTask.getPoints()));
        overview.setText(appTask.getDescription() != null ? appTask.getDescription() : "");
        team1Name.setText(sportMatch.getFirstTeamName());
        team2Name.setText(sportMatch.getSecondTeamName());

        if(sportMatch.getLeague() != null && !sportMatch.getLeague().equals("")){
            league_layout.setVisibility(View.VISIBLE);
            league.setText(sportMatch.getLeague());
        }
        else {
            league_layout.setVisibility(View.INVISIBLE);
        }

        if(sportMatch.getType() != null && !sportMatch.getType().equals("")){
            type_layout.setVisibility(View.VISIBLE);
            type.setText(sportMatch.getType());
        }
        else {
            type_layout.setVisibility(View.INVISIBLE);
        }

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");

        Date endDate = null, startDate = null, matchDate = null;
        try {
            endDate = format.parse(appTask.getEndDate());
            String end = formatter.format(endDate.getTime());
            ends.setText(String.format("%s", end));

            startDate = format.parse(appTask.getStartDate());
            String start = formatter.format(startDate.getTime());
            started.setText(String.format("%s", start));

            matchDate = format.parse(sportMatch.getMatchTime());
            String mTime = formatter.format(matchDate.getTime());
            time.setText(String.format("%s", mTime));

            Date currentTime = Calendar.getInstance().getTime();
            final Date matchDateTime = matchDate;
            long different = matchDateTime.getTime() - currentTime.getTime();

            final long secondsInMilli = 1000;
            final long minutesInMilli = secondsInMilli * 60;
            final long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;
            long weeksInMilli = daysInMilli * 7;

            long passed155 = matchDateTime.getTime() + 155 * minutesInMilli;
            if(currentTime.getTime() < passed155) {
                long elapsedWeeks = different / weeksInMilli;
                different = different % weeksInMilli;

                long elapsedDays = different / daysInMilli;
                different = different % daysInMilli;


                if (elapsedWeeks > 0) {
                    timer.setText(elapsedWeeks + " " + getString(R.string.weeks));
                } else {
                    if (elapsedDays > 0)
                        timer.setText(elapsedDays + " " + getString(R.string.days));
                    else {
                        counter = different;
                        new CountDownTimer(counter, 1000) {
                            public void onTick(long millisUntilFinished) {

                                long elapsedHours = counter / hoursInMilli;
                                counter = counter % hoursInMilli;

                                long elapsedMinutes = counter / minutesInMilli;
                                counter = counter % minutesInMilli;

                                long elapsedSeconds = counter / secondsInMilli;

                                timer.setText(String.format("%02d", elapsedHours) + " : " + String.format("%02d", elapsedMinutes) + " : " + String.format("%02d", elapsedSeconds));

                                Date currentTime1 = Calendar.getInstance().getTime();

                                counter = matchDateTime.getTime() - currentTime1.getTime();

                            }

                            public void onFinish() {
                                timer.setText(R.string.started);
                                timer.setTextColor(getResources().getColor(R.color.colorAccent));
                            }
                        }.start();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Glide.with(getActivity())
                .load(API_IMAGES_URL + sportMatch.getFirstTeamFlag())
                .optionalCenterInside()
                .into(team1Flag);

        Glide.with(getActivity())
                .load(API_IMAGES_URL + sportMatch.getSecondTeamFlag())
                .optionalCenterInside()
                .into(team2Flag);


        if(appTask.isDone()){
            startBtn.setEnabled(false);
            startBtn.setText(R.string.done_task);
        }
        else{
            if(appTask.isReachLimit()){
                startBtn.setEnabled(false);
                startBtn.setText(R.string.task_reached_limit);
            }
            else{
                startBtn.setEnabled(true);
                startBtn.setText(R.string.start);
            }
        }

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final CustomDialog cdd = new CustomDialog(getActivity(), DIALOG_TYPE_PREDICT);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    Objects.requireNonNull(cdd.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                }
                cdd.show();
                cdd.setCancelable(false);
                Glide.with(getActivity())
                        .load(API_IMAGES_URL + sportMatch.getFirstTeamFlag())
                        .optionalCenterInside()
                        .into(cdd.team1Falg);
                Glide.with(getActivity())
                        .load(API_IMAGES_URL + sportMatch.getSecondTeamFlag())
                        .optionalCenterInside()
                        .into(cdd.team2Flag);
                cdd.team1Name.setText(sportMatch.getFirstTeamName());
                cdd.team2Name.setText(sportMatch.getSecondTeamName());
                cdd.no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cdd.dismiss();
                    }
                });
                cdd.yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String firstTeamScore = cdd.team1Score.getText().toString().trim();
                        String secondTeamScore = cdd.team2Score.getText().toString().trim();
                        if(firstTeamScore.equals("")){
                            cdd.team1Score.setError(getString(R.string.field_required));
                            cdd.team1Score.requestFocus();
                            return;
                        }
                        if(Integer.parseInt(firstTeamScore) < 0){
                            cdd.team1Score.setError(getString(R.string.score_pos));
                            cdd.team1Score.requestFocus();
                            return;
                        }
                        if(secondTeamScore.equals("")){
                            cdd.team2Score.setError(getString(R.string.field_required));
                            cdd.team2Score.requestFocus();
                            return;
                        }
                        if(Integer.parseInt(secondTeamScore) < 0){
                            cdd.team2Score.setError(getString(R.string.score_pos));
                            cdd.team2Score.requestFocus();
                            return;
                        }
                        cdd.dismiss();
                        addTaskEnded(Integer.parseInt(firstTeamScore), Integer.parseInt(secondTeamScore));
                    }
                });
            }
        });
    }

    private void addTaskEnded(int firstTeamScore, int secondTeamScore) {
        PredictBody inModel = new PredictBody(sportMatch.getId(), firstTeamScore, secondTeamScore);
        String json = gson.toJson(inModel);
        new SendGetJsonApi(getActivity(), SPORT_MATCHES + ADD_END, json,
                new CallBackListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<CustomerTask> appTaskAPIResponse = getResponse(result, CustomerTask.class);
                            if (appTaskAPIResponse != null && appTaskAPIResponse.getResult().equals("success"))
                            {
                                customer = appTaskAPIResponse.getCurrentCustomer();
                                startBtn.setEnabled(false);
                            }
                            else
                            {
                                showRedToast(getApplicationContext(), getString(R.string.went_wrong));
                            }
                        }
                        catch ( Exception e)
                        {
                            showRedToast(getApplicationContext(), getString(R.string.went_wrong));
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ).Execute();
    }
}
