package com.u.mygamesapp.fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;

import java.util.Objects;

public class NoInternetFragment extends Fragment {

    public static String NO_INTERNET_FRAGMENT_TAG  = NoInternetFragment.class.getSimpleName();

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(R.string.setting);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_no_internet, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(R.string.no_internet_connection);
        BaseActivity.pointsLayout.setVisibility(View.GONE);

        Button refresh_btn = view.findViewById(R.id.retry_btn);
        refresh_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {
                ConnectivityManager conMgr = (ConnectivityManager) NoInternetFragment.this.getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
            if (netInfo != null) {
                int count = Objects.requireNonNull(getActivity()).getSupportFragmentManager().getBackStackEntryCount();
                if (count <= 1) {
                    Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, new HomeFragment())
                            .commit();
                } else {
                    Objects.requireNonNull(getActivity()).getSupportFragmentManager().popBackStack();
                }
            }
            }
        });

    }

}
