package com.u.mygamesapp.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.IndicatorView.draw.controller.DrawController;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;
import com.u.mygamesapp.adapters.SliderAdapter;
import com.u.mygamesapp.adapters.TasksAdapter;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.helpers.CustomDialog;
import com.u.mygamesapp.models.Customer;
import com.u.mygamesapp.models.CustomerTask;
import com.u.mygamesapp.models.HomePage;
import com.u.mygamesapp.models.Prize;
import com.u.mygamesapp.responsemodels.APIResponse;

import java.util.Objects;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.u.mygamesapp.api.APIConstants.ADD_END;
import static com.u.mygamesapp.api.APIConstants.API_IMAGES_URL;
import static com.u.mygamesapp.api.APIConstants.HOME_PAGE;
import static com.u.mygamesapp.api.APIConstants.SET_FCM_TOKEN;
import static com.u.mygamesapp.api.APIConstants.TASKS;
import static com.u.mygamesapp.helpers.Constants.DIALOG_TYPE_PRIZE;
import static com.u.mygamesapp.helpers.Constants.LUCKY_WHEEL_REQUEST_CODE;
import static com.u.mygamesapp.helpers.Constants.TASK_TYPE_INSTALL_APP;
import static com.u.mygamesapp.helpers.Constants.showRedToast;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_CURRENT_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_DAILY_BONUS_LEVEL;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_FCM_TOKEN;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_COLOR;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_ID;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_IS_BONUS_VALID;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_IS_FCM_TOKEN_SENT;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_IS_LUCKY_VALID;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_MESSAGES_COUNT;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_NEXT_GROUP_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_SHOW_MESSAGES_SNACK_BAR;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_SLIDER_FLIP_DURATION;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_TOTAL_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readFromPreferences;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readIntegerFromPreferences;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.saveToPreferences;

public class HomeFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static String HOME_FRAGMENT_TAG  = HomeFragment.class.getSimpleName();

    private RecyclerView tasksRecyclerView, finishedTaskTypesRecyclerView;
    private TextView header_2_txt;
    private SliderView sliderView;
    private HomePage homePage;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Gson gson = new Gson();
    private static Customer customer;
    private ViewGroup viewGroup;
    private View dialogView;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(R.string.home);
        BaseActivity.pointsLayout.setVisibility(View.VISIBLE);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getHomePage();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
        );

        viewGroup = view.findViewById(android.R.id.content);
        dialogView = LayoutInflater.from(view.getContext()).inflate(R.layout.dialog_custom_upgrade_group, viewGroup, false);

        sliderView = view.findViewById(R.id.imageSlider);
        header_2_txt = view.findViewById(R.id.header_2_txt);
        tasksRecyclerView = view.findViewById(R.id.tasks_recycler);
        GridLayoutManager mLayoutManager1 = new GridLayoutManager(getActivity(), 2);
        tasksRecyclerView.setLayoutManager(mLayoutManager1);

        finishedTaskTypesRecyclerView = view.findViewById(R.id.finished_tasks_recycler);
        GridLayoutManager mLayoutManager2 = new GridLayoutManager(getActivity(), 2);
        finishedTaskTypesRecyclerView.setLayoutManager(mLayoutManager2);

        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/116682556372999/likes",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        GraphResponse r = response;
                    }
                }
        ).executeAsync();

        getHomePage();
    }

    @Override
    public void onRefresh() {
        if (swipeRefreshLayout.isRefreshing()) {
            getHomePage();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void getHomePage() {
        new SendGetJsonApi(getActivity(), HOME_PAGE, "",
                new CallBackListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<HomePage> userInfoAPIResponse = getResponse(result, HomePage.class);
                            if (userInfoAPIResponse != null && userInfoAPIResponse.getResult().equals("success"))
                            {
                                homePage = userInfoAPIResponse.getContent();
                                customer = userInfoAPIResponse.getCurrentCustomer();
                                setFCMToken();
                            }
                            else
                            {
                                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.fragment_container, new NoDataFragment())
                                        .addToBackStack(null)
                                        .commit();
                            }
                        }
                        catch ( Exception e)
                        {
                            Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container, new NoDataFragment())
                                    .addToBackStack(null)
                                    .commit();
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ).Execute();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void onSuccess() {
        boolean showSnackBar = Boolean.parseBoolean(readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_SHOW_MESSAGES_SNACK_BAR, "False"));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_MESSAGES_COUNT, String.valueOf(homePage.getNewMessages()));
        if(homePage.getNewMessages() > 0 && showSnackBar){
            saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_SHOW_MESSAGES_SNACK_BAR, "False");
            @SuppressLint("DefaultLocale") Snackbar snackbar = Snackbar.make(swipeRefreshLayout,
                    String.format("%s %d %s", getString(R.string.you_have), homePage.getNewMessages(), getString(R.string.unread_messages)), Snackbar.LENGTH_LONG)
                    .setAction(R.string.view, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container, new InboxFragment())
                                    .addToBackStack(null)
                                    .commit();
                        }
                    })
                    .setActionTextColor(Color.WHITE);
            View sbView = snackbar.getView();
            sbView.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
            snackbar.show();
        }
        final SliderAdapter adapter = new SliderAdapter(getActivity(), homePage.getSlides());
        adapter.setCount(homePage.getSlides().size());
        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(IndicatorAnimations.SLIDE); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!
        sliderView.setSliderTransformAnimation(SliderAnimations.CUBEINROTATIONTRANSFORMATION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            sliderView.setAutoCycleDirection(SliderView.LAYOUT_DIRECTION_LOCALE);
        }
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        int slides_duration = readIntegerFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_SLIDER_FLIP_DURATION, 5);
        sliderView.setScrollTimeInSec(slides_duration);
        sliderView.startAutoCycle();
        sliderView.setOnIndicatorClickListener(new DrawController.ClickListener() {
            @Override
            public void onIndicatorClicked(int position) {
                sliderView.setCurrentPagePosition(position);
            }
        });

        TasksAdapter tasksAdapter = new TasksAdapter(getActivity(), homePage.getTasks());
        tasksRecyclerView.setAdapter(tasksAdapter);
        tasksAdapter.setOnItemClickListener(getActivity());


        if(homePage.getFinishedTasks() != null && homePage.getFinishedTasks().size() > 0){
            header_2_txt.setVisibility(View.VISIBLE);
            finishedTaskTypesRecyclerView.setVisibility(View.VISIBLE);
            TasksAdapter finishedTasksAdapter = new TasksAdapter(getActivity(), homePage.getFinishedTasks());
            finishedTaskTypesRecyclerView.setAdapter(finishedTasksAdapter);
            finishedTasksAdapter.setOnItemClickListener(getActivity());
        }
        else {
            header_2_txt.setVisibility(View.GONE);
            finishedTaskTypesRecyclerView.setVisibility(View.GONE);
        }


        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_IS_LUCKY_VALID, String.valueOf(homePage.isLuckyWheelValid()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_IS_BONUS_VALID, String.valueOf(homePage.isDailyBonusValid()));
//        if(homePage.isLuckyWheelValid()){
//            new Handler().postDelayed(
//                    new Runnable() {
//                        @Override
//                        public void run() {
//                            Calendar c = Calendar.getInstance();
//                            c.set(Calendar.HOUR_OF_DAY, 0);
//                            c.set(Calendar.MINUTE, 0);
//                            c.set(Calendar.SECOND, 0);
//                            c.set(Calendar.MILLISECOND, 0);
//                            Date today = c.getTime();
//                            long luckyClosed = new Date().getTime();
//                            boolean isFirstInstall = false;
//                            try {
//                                luckyClosed = Date.parse(readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_LUCKY_CLOSED, "0"));
//                            }
//                            catch (Exception e){
//                                isFirstInstall = true;
//                            }
//                            Date luckyClosedDate = new Date(luckyClosed);
//                            if(luckyClosedDate.before(today) || isFirstInstall){
//                                Intent luckyIntent = new Intent(getActivity(), DailyBonusActivity.class);
//                                startActivityForResult(luckyIntent, LUCKY_WHEEL_REQUEST_CODE);
//                                saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_LUCKY_CLOSED, String.valueOf(today));
//                            }
//                        }
//                    },
//                    2000
//            );
//        }

        if (homePage.getPendingTasks() != null && homePage.getPendingTasks().size() > 0) {
            for(int i = 0; i < homePage.getPendingTasks().size(); i++){
                if(homePage.getPendingTasks().get(i).getTaskType().getName().equals(TASK_TYPE_INSTALL_APP)){
                    boolean isInstalled = isPackageInstalled(homePage.getPendingTasks().get(i).getPackageName(), Objects.requireNonNull(getActivity()).getPackageManager());
                    if(isInstalled){
                        addTaskEnded(homePage.getPendingTasks().get(i).getId());
                    }
                }
            }
        }
        setUserGroupInfo();
    }

    private void setUserGroupInfo(){
        String groupId = readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_ID, "0");
        boolean isUpgradeGroup = false;
        if(!groupId.equals(String.valueOf(customer.getGroupId()))){
            isUpgradeGroup = true;
        }
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_DAILY_BONUS_LEVEL, String.valueOf(customer.getDailyBonusLevel()));

        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_TOTAL_POINTS, String.valueOf(customer.getTotalPoints()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_CURRENT_POINTS, String.valueOf(customer.getCurrentPoints()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_ID, String.valueOf(customer.getGroupId()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_NAME, String.valueOf(customer.getGroup().getDisplayName()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_COLOR, String.valueOf(customer.getGroup().getColor()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_NEXT_GROUP_POINTS, String.valueOf(customer.getNextGroupPoints()));
        BaseActivity.setUserGroupInfo(customer.getGroup().getDisplayName(), String.valueOf(customer.getCurrentPoints()),
                String.valueOf(customer.getTotalPoints()), customer.getGroup().getColor(), customer.getNextGroupPoints());
        if(isUpgradeGroup){
            showUpgradeDialog();
        }
    }

    private void showUpgradeDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.CustomAlertDialog);
        TextView groupName = dialogView.findViewById(R.id.group_name);
        CardView cardView = dialogView.findViewById(R.id.dialog_card);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        groupName.setText(String.format("%s %s", customer.getGroup().getDisplayName(), getActivity().getString(R.string.group)));
        cardView.setCardBackgroundColor(Color.parseColor(customer.getGroup().getColor()));
        alertDialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == LUCKY_WHEEL_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            assert data != null;
            Prize prize = (Prize) Objects.requireNonNull(data.getExtras()).getSerializable("prize");
            final CustomDialog cdd = new CustomDialog(getActivity(), DIALOG_TYPE_PRIZE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Objects.requireNonNull(cdd.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
            cdd.show();
            Glide.with(Objects.requireNonNull(getActivity()))
                    .load(API_IMAGES_URL + (prize != null ? prize.getImageUrl() : null))
                    .centerInside()
                    .into(cdd.prizeImg);
            assert prize != null;
            cdd.prizeName.setText(prize.getDisplayName());
            cdd.ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cdd.dismiss();
                }
            });
        }
    }

    private boolean isPackageInstalled(String packageName, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private void addTaskEnded(int appTaskId) {
        CustomerTask inModel = new CustomerTask(appTaskId);
        String json = gson.toJson(inModel);
        new SendGetJsonApi(getActivity(), TASKS + ADD_END, json,
                new CallBackListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<CustomerTask> appTaskAPIResponse = getResponse(result, CustomerTask.class);
                            if (appTaskAPIResponse != null && appTaskAPIResponse.getResult().equals("success"))
                            {
                                customer = appTaskAPIResponse.getCurrentCustomer();
                                setUserGroupInfo();
                            }
                            else
                            {
                                showRedToast(getApplicationContext(), getString(R.string.went_wrong));
                            }
                        }
                        catch ( Exception e)
                        {
                            showRedToast(getApplicationContext(), getString(R.string.went_wrong));
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ).Execute();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setFCMToken() {
        boolean isFCMSent = Boolean.parseBoolean(readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_IS_FCM_TOKEN_SENT, "False"));
        String fcmToken = readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_FCM_TOKEN, "");

        if(!isFCMSent){
            Customer inModel = new Customer(fcmToken);
            String json = gson.toJson(inModel);
            new SendGetJsonApi(getActivity(), SET_FCM_TOKEN, json,
                    new CallBackListener() {
                        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                        @Override
                        public void onFinish(String result)
                        {
                            saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_IS_FCM_TOKEN_SENT, "True");
                            onSuccess();
                        }
                        @Override
                        public void onProgress(int process) {
                        }
                    }
            ).Execute();
        }
        else {
            onSuccess();
        }
    }
}
