package com.u.mygamesapp.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;
import com.u.mygamesapp.activities.DailyBonusActivity;
import com.u.mygamesapp.activities.LuckyWheelActivity;

import static com.u.mygamesapp.helpers.Constants.LUCKY_WHEEL_REQUEST_CODE;

public class MiniGamesFragment extends Fragment {

    public static String MINI_GAMES_FRAGMENT_TAG  = MiniGamesFragment.class.getSimpleName();

    private ImageView lucky, bonus;
    private Button luckyBtn, bonusBtn;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mini_games, container, false);
    }

    @SuppressLint("DefaultLocale")
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BaseActivity.pointsLayout.setVisibility(View.GONE);

        lucky = view.findViewById(R.id.lucky_tab);
        bonus = view.findViewById(R.id.bonus_tab);
        luckyBtn = view.findViewById(R.id.lucky_button);
        bonusBtn = view.findViewById(R.id.bonus_button);

        Glide.with(getActivity())
                .load(getImage("lucky_wheel"))
                .circleCrop()
                .placeholder(R.drawable.icon)
                .into(lucky);

        Glide.with(getActivity())
                .load(getImage("daily_bonus"))
                .circleCrop()
                .placeholder(R.drawable.icon)
                .into(bonus);

        luckyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent luckyIntent = new Intent(getActivity(), LuckyWheelActivity.class);
                startActivityForResult(luckyIntent, LUCKY_WHEEL_REQUEST_CODE);
            }
        });

        bonusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent luckyIntent = new Intent(getActivity(), DailyBonusActivity.class);
                startActivityForResult(luckyIntent, LUCKY_WHEEL_REQUEST_CODE);
            }
        });
    }

    public int getImage(String imageName) {
        int drawableResourceId = this.getResources().getIdentifier(imageName, "drawable", getActivity().getPackageName());
        return drawableResourceId;
    }
}
