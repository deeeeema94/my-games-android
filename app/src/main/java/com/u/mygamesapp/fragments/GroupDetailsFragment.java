package com.u.mygamesapp.fragments;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.ramijemli.percentagechartview.PercentageChartView;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;

import java.util.Objects;

import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_COLOR;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_NEXT_GROUP_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_TOTAL_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readFromPreferences;

public class GroupDetailsFragment extends Fragment {

    public static String PRIZE_DETAILS_FRAGMENT_TAG  = GroupDetailsFragment.class.getSimpleName();

    private TextView title, next_points;
    private PercentageChartView mChart;
    private RelativeLayout groupLayout;


    public GroupDetailsFragment(){

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_group_details, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BaseActivity.pointsLayout.setVisibility(View.GONE);

        title = view.findViewById(R.id.group_name);
        next_points = view.findViewById(R.id.next_points);
        mChart = view.findViewById(R.id.view_id);
        groupLayout = view.findViewById(R.id.group_layout);

        setData();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("DefaultLocale")
    private void setData() {
        String group = readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_NAME, "");
        String totalPoints = readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_TOTAL_POINTS, "");
        String color = readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_COLOR, "");
        int nextGroupPoints = Integer.parseInt(readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_NEXT_GROUP_POINTS, "0"));

        Objects.requireNonNull(getActivity()).setTitle(group);
        title.setText(group);

        float percentage;
        if(nextGroupPoints != 0){
            percentage = (float) (100*Integer.parseInt(totalPoints))/nextGroupPoints;
        }
        else {
            percentage = 100;
        }
        if(percentage > 100)
            percentage = 100;
        mChart.setProgress(percentage, true);
        mChart.setProgressColor(Color.parseColor(color));

        int difference = nextGroupPoints - Integer.parseInt(totalPoints);
        if(difference > 0){
            next_points.setText(String.format(getString(R.string.next_group_desc), difference));
        }
        else {
            next_points.setText(R.string.max_group);
        }
        next_points.setTextColor(Color.parseColor(color));
    }
}
