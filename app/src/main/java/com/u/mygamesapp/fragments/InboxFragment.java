package com.u.mygamesapp.fragments;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;
import com.u.mygamesapp.adapters.MessagesAdapter;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.models.CustomerMessage;
import com.u.mygamesapp.requestmodels.PaginationRequest;
import com.u.mygamesapp.responsemodels.PagedAPIResponse;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static com.u.mygamesapp.api.APIConstants.CUSTOMER_MESSAGES;
import static com.u.mygamesapp.api.APIConstants.GET_ALL;
import static com.u.mygamesapp.helpers.JsonParser.getPagedResponse;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_MESSAGES_COUNT;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.saveToPreferences;

public class InboxFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static String INBOX_FRAGMENT_TAG  = InboxFragment.class.getSimpleName();
//    private Boolean isStarted = false;
//    private Boolean isVisible = false;

    private RecyclerView messagesRecyclerView;
    private List<CustomerMessage> messages;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Gson gson = new Gson();
    private TextView txt_no_data;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_account_tabs, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.inbox));
        BaseActivity.pointsLayout.setVisibility(View.GONE);

        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getData();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
        );

        txt_no_data = view.findViewById(R.id.txt_no_data);
        messagesRecyclerView = view.findViewById(R.id.tasks_recycler);
        GridLayoutManager mLayoutManager1 = new GridLayoutManager(getActivity(), 1);
        messagesRecyclerView.setLayoutManager(mLayoutManager1);
//        isStarted = true;
//        if(isStarted && isVisible){
            getData();
//        }
    }

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        isVisible = isVisibleToUser;
//        if (isStarted && isVisible) {
//            getData();
//        }
//    }

    @Override
    public void onRefresh() {
        if (swipeRefreshLayout.isRefreshing()) {
            getData();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void getData() {
        PaginationRequest request = new PaginationRequest(1, 200);
        String json = gson.toJson(request);
        new SendGetJsonApi(getActivity(), CUSTOMER_MESSAGES + GET_ALL, json,
                new CallBackListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onFinish(String result)
                    {
//                        isVisible = false;
//                        isStarted = false;
                        try {
                            PagedAPIResponse<CustomerMessage[]> pagedAPIResponse = getPagedResponse(result, CustomerMessage[].class);
                            if (pagedAPIResponse != null && pagedAPIResponse.getMessage().equals("success"))
                            {
                                messages = new LinkedList<>(Arrays.asList(pagedAPIResponse.getContent().getContent()));
                                if(messages.size() > 0){
                                    txt_no_data.setVisibility(View.GONE);
                                    messagesRecyclerView.setVisibility(View.VISIBLE);
                                    onSuccess(messages);
                                }
                                else
                                {
                                    txt_no_data.setVisibility(View.VISIBLE);
                                    messagesRecyclerView.setVisibility(View.GONE);
                                }
                            }
                            else
                            {
                                txt_no_data.setVisibility(View.VISIBLE);
                                messagesRecyclerView.setVisibility(View.GONE);
                            }
                        }
                        catch ( Exception e)
                        {
                            txt_no_data.setVisibility(View.VISIBLE);
                            messagesRecyclerView.setVisibility(View.GONE);
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ).Execute();
    }

    private void onSuccess(List<CustomerMessage> msgs) {

        int count = 0;
        for (int i = 0; i < msgs.size(); i++){
            if(!msgs.get(i).isRead()){
                count++;
            }
        }
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_MESSAGES_COUNT, String.valueOf(count));
        MessagesAdapter messagesAdapter = new MessagesAdapter(getActivity(), msgs);
        messagesRecyclerView.setAdapter(messagesAdapter);
        messagesAdapter.setOnItemClickListener(getActivity());
    }
}
