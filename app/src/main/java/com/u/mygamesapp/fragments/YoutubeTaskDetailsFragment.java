package com.u.mygamesapp.fragments;

import android.Manifest;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.YouTubeScopes;
import com.google.api.services.youtube.model.Subscription;
import com.google.api.services.youtube.model.SubscriptionListResponse;
import com.google.gson.Gson;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.helpers.CustomDialog;
import com.u.mygamesapp.models.AppTask;
import com.u.mygamesapp.models.Customer;
import com.u.mygamesapp.models.CustomerTask;
import com.u.mygamesapp.responsemodels.APIResponse;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;
import static com.u.mygamesapp.api.APIConstants.ADD_END;
import static com.u.mygamesapp.api.APIConstants.ADD_START;
import static com.u.mygamesapp.api.APIConstants.API_IMAGES_URL;
import static com.u.mygamesapp.api.APIConstants.GET_ONE;
import static com.u.mygamesapp.api.APIConstants.TASKS;
import static com.u.mygamesapp.helpers.Constants.DIALOG_TYPE_PRIZE;
import static com.u.mygamesapp.helpers.Constants.REQUEST_ACCOUNT_PICKER;
import static com.u.mygamesapp.helpers.Constants.REQUEST_AUTHORIZATION;
import static com.u.mygamesapp.helpers.Constants.REQUEST_GOOGLE_PLAY_SERVICES;
import static com.u.mygamesapp.helpers.Constants.REQUEST_PERMISSION_GET_ACCOUNTS;
import static com.u.mygamesapp.helpers.Constants.YOUTUBE_SUBSCRIBE_REQUEST_CODE;
import static com.u.mygamesapp.helpers.Constants.showRedToast;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.DEFAULT_FILE_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_CURRENT_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_COLOR;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_ID;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_GROUP_NAME;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_NEXT_GROUP_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesConstants.PREF_TOTAL_POINTS;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.readFromPreferences;
import static com.u.mygamesapp.helpers.sharedpreferences.SharedPreferencesManager.saveToPreferences;

public class YoutubeTaskDetailsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,
 EasyPermissions.PermissionCallbacks{

    public static String YOUTUBE_TASK_DETAILS_FRAGMENT_TAG  = YoutubeTaskDetailsFragment.class.getSimpleName();

    private TextView name, points, overview, started, ends;
    private Button startBtn;
    private ImageView img;
    private AppTask appTask;
    private SwipeRefreshLayout swipeRefreshLayout;
    private int appTaskId;
    private Gson gson = new Gson();
    private boolean IsFromDoneTask = false;

    GoogleAccountCredential mCredential;
    ProgressDialog mProgress;
    private static Customer customer;

    private static final String PREF_ACCOUNT_NAME = "accountName";
    private static final String[] SCOPES = { YouTubeScopes.YOUTUBE_READONLY };
    private ViewGroup viewGroup;
    private View dialogView;
    private static List<Subscription> subscriptions;

    public YoutubeTaskDetailsFragment(int taskId){
        appTaskId = taskId;
    }

    public YoutubeTaskDetailsFragment(int taskId, boolean isFromDoneTask){
        appTaskId = taskId;
        IsFromDoneTask = isFromDoneTask;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_task_details, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(R.string.home);
        BaseActivity.pointsLayout.setVisibility(View.VISIBLE);

        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getData();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
        );

        viewGroup = view.findViewById(android.R.id.content);
        dialogView = LayoutInflater.from(view.getContext()).inflate(R.layout.dialog_custom_upgrade_group, viewGroup, false);

        name = view.findViewById(R.id.name);
        overview = view.findViewById(R.id.overview);
        started = view.findViewById(R.id.strted);
        ends = view.findViewById(R.id.ends);
        img = view.findViewById(R.id.img);
        points = view.findViewById(R.id.points);
        startBtn = view.findViewById(R.id.start_btn);

        getData();
    }

    @Override
    public void onRefresh() {
        if (swipeRefreshLayout.isRefreshing()) {
            getData();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void getData() {
        AppTask inModel = new AppTask(appTaskId);
        String json = gson.toJson(inModel);
        new SendGetJsonApi(getActivity(), TASKS + GET_ONE, json,
                new CallBackListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<AppTask> appTaskAPIResponse = getResponse(result, AppTask.class);
                            if (appTaskAPIResponse != null && appTaskAPIResponse.getResult().equals("success"))
                            {
                                appTask = appTaskAPIResponse.getContent();
                                onSuccess(appTask);
                            }
                            else
                            {
                                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.fragment_container, new NoDataFragment())
                                        .addToBackStack(null)
                                        .commit();
                            }
                        }
                        catch ( Exception e)
                        {
                            Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container, new NoDataFragment())
                                    .addToBackStack(null)
                                    .commit();
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ).Execute();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("DefaultLocale")
    private void onSuccess(final AppTask appTask) {
        Objects.requireNonNull(getActivity()).setTitle(appTask.getDisplayName());
        name.setText(appTask.getDisplayName());
        points.setText(String.format("%d", appTask.getPoints()));
        overview.setText(appTask.getDescription() != null ? appTask.getDescription() : "");

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");

        Date endDate = null, startDate = null;
        try {
            endDate = format.parse(appTask.getEndDate());
            String end = formatter.format(endDate.getTime());
            ends.setText(String.format("%s", end));
            startDate = format.parse(appTask.getStartDate());
            String start = formatter.format(startDate.getTime());
            started.setText(String.format("%s", start));
        } catch (Exception e) {
            e.printStackTrace();
        }

        Glide.with(getActivity())
                .load(API_IMAGES_URL + appTask.getImageUrl())
                .centerInside()
                .into(img);

        if(appTask.isDone()){
            startBtn.setEnabled(false);
            startBtn.setText(R.string.done_task);
        }
        else{
            if(appTask.isReachLimit()){
                startBtn.setEnabled(false);
                startBtn.setText(R.string.task_reached_limit);
            }
            else{
                startBtn.setEnabled(true);
                startBtn.setText(R.string.start);
            }
        }

        if(IsFromDoneTask){
            showConDialog();
        }

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addTaskStarted();
            }
        });

        mProgress = new ProgressDialog(getActivity());
        mProgress.setMessage("Calling YouTube Data API ...");

        // Initialize credentials and service object.
        mCredential = GoogleAccountCredential.usingOAuth2(
                getActivity(), Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());
    }

    private void getResultsFromApi(boolean isFirstStep) {
        if (!isGooglePlayServicesAvailable()) {
            acquireGooglePlayServices();
        } else if (mCredential.getSelectedAccountName() == null) {
            chooseAccount(isFirstStep);
        } else if (! isDeviceOnline()) {
            Toast.makeText(getActivity(), "No network connection available.", Toast.LENGTH_LONG).show();
        } else {
            new MakeRequestTask(mCredential, isFirstStep).execute();
        }
    }

    @AfterPermissionGranted(REQUEST_PERMISSION_GET_ACCOUNTS)
    private void chooseAccount(boolean isFirstStep) {
        if (EasyPermissions.hasPermissions(
                getActivity(), Manifest.permission.GET_ACCOUNTS)) {
            String accountName = getActivity().getPreferences(Context.MODE_PRIVATE)
                    .getString(PREF_ACCOUNT_NAME, null);
            if (accountName != null) {
                mCredential.setSelectedAccountName(accountName);
                getResultsFromApi(isFirstStep);
            } else {
                // Start a dialog from which the user can choose an account
                startActivityForResult(
                        mCredential.newChooseAccountIntent(),
                        REQUEST_ACCOUNT_PICKER);
            }
        } else {
            // Request the GET_ACCOUNTS permission via a user dialog
            EasyPermissions.requestPermissions(
                    getActivity(),
                    "This app needs to access your Google account (via Contacts).",
                    REQUEST_PERMISSION_GET_ACCOUNTS,
                    Manifest.permission.GET_ACCOUNTS);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK) {
                    Toast.makeText(getActivity(), "This app requires Google Play Services. Please install " +
                            "Google Play Services on your device and relaunch this app.", Toast.LENGTH_LONG).show();
                } else {
                    getResultsFromApi(true);
                }
                break;
            case REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        SharedPreferences settings =
                                getActivity().getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(PREF_ACCOUNT_NAME, accountName);
                        editor.apply();
                        mCredential.setSelectedAccountName(accountName);
                        getResultsFromApi(true);
                    }
                }
                break;
            case REQUEST_AUTHORIZATION:
                if (resultCode == RESULT_OK) {
                    getResultsFromApi(true);
                }
                break;
            case YOUTUBE_SUBSCRIBE_REQUEST_CODE:
                getResultsFromApi(false);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(
                requestCode, permissions, grantResults, getActivity());
    }

    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(getActivity());
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    private void acquireGooglePlayServices() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(getActivity());
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
        }
    }

    void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(
                getActivity(),
                connectionStatusCode,
                REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    private class MakeRequestTask extends AsyncTask<Void, Void, List<String>> {
        private com.google.api.services.youtube.YouTube mService = null;
        private Exception mLastError = null;
        private boolean mIsFirstStep;

        MakeRequestTask(GoogleAccountCredential credential, boolean isFirstStep) {
            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mIsFirstStep = isFirstStep;
            mService = new com.google.api.services.youtube.YouTube.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName("YouTube Data API Android Quickstart")
                    .build();
        }

        /**
         * Background task to call YouTube Data API.
         * @param params no parameters needed for this task.
         */
        @Override
        protected List<String> doInBackground(Void... params) {
            try {
                return getDataFromApi();
            } catch (Exception e) {
                mLastError = e;
                cancel(true);
                return null;
            }
        }

        /**
         * Fetch information about the "GoogleDevelopers" YouTube channel.
         * @return List of Strings containing information about the channel.
         * @throws IOException
         */
        private List<String> getDataFromApi() throws IOException {
            // Get a list of up to 10 files.
            List<String> channelInfo = new ArrayList<String>();
            YouTube youtubeService = mService;

            YouTube.Subscriptions.List request = youtubeService.subscriptions()
                    .list("subscriberSnippet");
            SubscriptionListResponse response = request.setForChannelId(appTask.getVedioId())
                    .setMine(true)
                    .execute();

            subscriptions = response.getItems();
            if(mIsFirstStep){
                final String url = appTask.getLink(); // getPackageName() from Context or Activity object
                startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse(url)), YOUTUBE_SUBSCRIBE_REQUEST_CODE);
            }
            return channelInfo;
        }

        @Override
        protected void onPreExecute() {
            mProgress.show();
        }

        @Override
        protected void onPostExecute(List<String> output) {
            mProgress.hide();
            if(!mIsFirstStep){
                if(subscriptions != null && subscriptions.size() > 0){
                    addTaskEnded();
                }
            }
            if (output == null || output.size() == 0) {
                Toast.makeText(getActivity(), "No results returned.", Toast.LENGTH_LONG).show();
            } else {
                output.add(0, "Data retrieved using the YouTube Data API:");
                Toast.makeText(getActivity(), TextUtils.join("\n", output), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onCancelled() {
            mProgress.hide();
            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            REQUEST_AUTHORIZATION);
                } else {
                    Toast.makeText(getActivity(), "The following error occurred:\n"
                            + mLastError.getMessage(), Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getActivity(), "Request cancelled.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void showConDialog() {
        final CustomDialog cdd = new CustomDialog(getActivity(), DIALOG_TYPE_PRIZE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(cdd.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        cdd.show();
        cdd.setCancelable(false);
        Glide.with(getActivity())
                .load(getImage("star"))
                .centerInside()
                .into(cdd.prizeImg);
        cdd.prizeName.setText(String.format("%d %s", appTask.getPoints(), getString(R.string.points)));
        cdd.ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cdd.dismiss();
                setUserGroupInfo();
            }
        });
    }

    public int getImage(String imageName) {
        int drawableResourceId = this.getResources().getIdentifier(imageName, "drawable", getActivity().getPackageName());
        return drawableResourceId;
    }

    private void addTaskStarted() {
        CustomerTask inModel = new CustomerTask(appTaskId);
        String json = gson.toJson(inModel);
        new SendGetJsonApi(getActivity(), TASKS + ADD_START, json,
                new CallBackListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<CustomerTask> appTaskAPIResponse = getResponse(result, CustomerTask.class);
                            if (appTaskAPIResponse != null && appTaskAPIResponse.getResult().equals("success"))
                            {
                                getResultsFromApi(true);
                            }
                            else
                            {
                                showRedToast(getApplicationContext(), getString(R.string.went_wrong));
                            }
                        }
                        catch ( Exception e)
                        {
                            showRedToast(getApplicationContext(), getString(R.string.went_wrong));
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ).Execute();
    }

    private void addTaskEnded() {
        CustomerTask inModel = new CustomerTask(appTaskId);
        String json = gson.toJson(inModel);
        new SendGetJsonApi(getActivity(), TASKS + ADD_END, json,
                new CallBackListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<CustomerTask> appTaskAPIResponse = getResponse(result, CustomerTask.class);
                            if (appTaskAPIResponse != null && appTaskAPIResponse.getResult().equals("success"))
                            {
                                customer = appTaskAPIResponse.getCurrentCustomer();
                                startBtn.setEnabled(false);
                                showConDialog();
                            }
                            else
                            {
                                showRedToast(getApplicationContext(), getString(R.string.went_wrong));
                            }
                        }
                        catch ( Exception e)
                        {
                            showRedToast(getApplicationContext(), getString(R.string.went_wrong));
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ).Execute();
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    private void setUserGroupInfo(){
        String groupId = readFromPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_ID, "0");
        boolean isUpgradeGroup = false;
        if(!groupId.equals(String.valueOf(customer.getGroupId()))){
            isUpgradeGroup = true;
        }
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_TOTAL_POINTS, String.valueOf(customer.getTotalPoints()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_CURRENT_POINTS, String.valueOf(customer.getCurrentPoints()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_ID, String.valueOf(customer.getGroupId()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_NAME, String.valueOf(customer.getGroup().getDisplayName()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_GROUP_COLOR, String.valueOf(customer.getGroup().getColor()));
        saveToPreferences(getActivity(), DEFAULT_FILE_NAME, PREF_NEXT_GROUP_POINTS, String.valueOf(customer.getNextGroupPoints()));
        BaseActivity.setUserGroupInfo(customer.getGroup().getDisplayName(), String.valueOf(customer.getCurrentPoints()),
                String.valueOf(customer.getTotalPoints()), customer.getGroup().getColor(), customer.getNextGroupPoints());
        if(isUpgradeGroup){
            showUpgradeDialog();
        }
    }

    private void showUpgradeDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.CustomAlertDialog);
        TextView groupName = dialogView.findViewById(R.id.group_name);
        CardView cardView = dialogView.findViewById(R.id.dialog_card);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        groupName.setText(String.format("%s %s", getActivity().getString(R.string.group), customer.getGroup().getDisplayName()));
        cardView.setCardBackgroundColor(Color.parseColor(customer.getGroup().getColor()));
        alertDialog.show();
    }
}
