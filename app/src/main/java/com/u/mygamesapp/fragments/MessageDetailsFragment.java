package com.u.mygamesapp.fragments;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.u.mygamesapp.R;
import com.u.mygamesapp.activities.BaseActivity;
import com.u.mygamesapp.api.CallBackListener;
import com.u.mygamesapp.api.SendGetJsonApi;
import com.u.mygamesapp.models.CustomerMessage;
import com.u.mygamesapp.responsemodels.APIResponse;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import static com.u.mygamesapp.api.APIConstants.CUSTOMER_MESSAGES;
import static com.u.mygamesapp.api.APIConstants.GET_ONE;
import static com.u.mygamesapp.helpers.JsonParser.getResponse;

public class MessageDetailsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static String MESSAGE_DETAILS_FRAGMENT_TAG  = MessageDetailsFragment.class.getSimpleName();

    private TextView title, date, script;
    private CustomerMessage message;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Gson gson = new Gson();

    public MessageDetailsFragment(CustomerMessage msg){
        message = msg;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_message_details, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.message_details));
        BaseActivity.pointsLayout.setVisibility(View.GONE);

        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getData();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
        );

        title = view.findViewById(R.id.msg_title);
        date = view.findViewById(R.id.msg_date);
        script = view.findViewById(R.id.msg_script);

        getData();
    }

    @Override
    public void onRefresh() {
        if (swipeRefreshLayout.isRefreshing()) {
            getData();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void getData() {
        CustomerMessage inModel = new CustomerMessage(message.getId());
        String json = gson.toJson(inModel);
        new SendGetJsonApi(getActivity(), CUSTOMER_MESSAGES + GET_ONE, json,
                new CallBackListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onFinish(String result)
                    {
                        try {
                            APIResponse<CustomerMessage> messageAPIResponse = getResponse(result, CustomerMessage.class);
                            if (messageAPIResponse != null && messageAPIResponse.getResult().equals("success"))
                            {
                                message = messageAPIResponse.getContent();
                                onSuccess(message);
                            }
                            else
                            {
                                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.fragment_container, new NoDataFragment())
                                        .addToBackStack(null)
                                        .commit();
                            }
                        }
                        catch ( Exception e)
                        {
                            Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container, new NoDataFragment())
                                    .addToBackStack(null)
                                    .commit();
                        }
                    }
                    @Override
                    public void onProgress(int process) {
                    }
                }
        ).Execute();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("DefaultLocale")
    private void onSuccess(final CustomerMessage msg) {
        title.setText(msg.getMessage().getTitle());
        script.setText(msg.getMessage().getScript());
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");
        Date sendDate = null;
        try {
            sendDate = format.parse(msg.getSendDate());
            String send = formatter.format(sendDate.getTime());
            date.setText(String.format("%s", send));
        } catch (Exception e) {
            date.setText(msg.getSendDate());
            e.printStackTrace();
        }
    }
}
