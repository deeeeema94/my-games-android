package com.u.mygamesapp.responsemodels;

import com.google.gson.annotations.SerializedName;

public class PagedContent<T> {

    @SerializedName("pagination")
    private Pagination pagination;

    @SerializedName("content")
    private T content;

    public PagedContent() {
    }

    public PagedContent(Pagination pagination, T content) {
        this.pagination = pagination;
        this.content = content;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }
}
