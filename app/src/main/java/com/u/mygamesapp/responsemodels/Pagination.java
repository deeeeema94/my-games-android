package com.u.mygamesapp.responsemodels;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Pagination implements Serializable {

    @SerializedName("totalCount")
    private int TotalCount;

    @SerializedName("limit")
    private int Limit;

    @SerializedName("totalPages")
    private int TotalPages;

    @SerializedName("currentPage")
    private int CurrentPage;


    public Pagination() {
    }

    public Pagination(int totalCount, int limit, int totalPages, int currentPage) {
        TotalCount = totalCount;
        Limit = limit;
        TotalPages = totalPages;
        CurrentPage = currentPage;
    }

    public int getTotalCount() {
        return TotalCount;
    }

    public void setTotalCount(int totalCount) {
        TotalCount = totalCount;
    }

    public int getLimit() {
        return Limit;
    }

    public void setLimit(int limit) {
        Limit = limit;
    }

    public int getTotalPages() {
        return TotalPages;
    }

    public void setTotalPages(int totalPages) {
        TotalPages = totalPages;
    }

    public int getCurrentPage() {
        return CurrentPage;
    }

    public void setCurrentPage(int currentPage) {
        CurrentPage = currentPage;
    }
}

