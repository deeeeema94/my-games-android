package com.u.mygamesapp.responsemodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.u.mygamesapp.models.Customer;

public class APIResponse<T> {

    @SerializedName("message")
    @Expose
    private String result;

    @SerializedName("content")
    @Expose
    private T content;

    @SerializedName("code")
    @Expose
    private int errorCode;

    @SerializedName("currentCustomer")
    @Expose
    private Customer currentCustomer;

    public APIResponse() {
    }

    public APIResponse(String result, T content, int errorCode) {
        this.result = result;
        this.content = content;
        this.errorCode = errorCode;
    }

    public APIResponse(String result, T content, int errorCode, Customer currentCustomer) {
        this.result = result;
        this.content = content;
        this.errorCode = errorCode;
        this.currentCustomer = currentCustomer;
    }

    public String getResult() {
        return result;
    }

    public T getContent() {
        return content;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void setContent(T content) {
        this.content = content;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public Customer getCurrentCustomer() {
        return currentCustomer;
    }

    public void setCurrentCustomer(Customer currentCustomer) {
        this.currentCustomer = currentCustomer;
    }
}
