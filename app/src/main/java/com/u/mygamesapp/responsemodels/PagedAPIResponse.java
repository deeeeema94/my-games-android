package com.u.mygamesapp.responsemodels;

import com.google.gson.annotations.SerializedName;
import com.u.mygamesapp.models.Customer;

import java.io.Serializable;

public class PagedAPIResponse<T> implements Serializable {

    @SerializedName("message")
    private String message;

    @SerializedName("content")
    private PagedContent<T> content;

    @SerializedName("code")
    private int code;

    @SerializedName("currentCustomer")
    private Customer currentCustomer;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PagedContent<T> getContent() {
        return content;
    }

    public void setContent(PagedContent<T> content) {
        this.content = content;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Customer getCurrentCustomer() {
        return currentCustomer;
    }

    public void setCurrentCustomer(Customer currentCustomer) {
        this.currentCustomer = currentCustomer;
    }
}
